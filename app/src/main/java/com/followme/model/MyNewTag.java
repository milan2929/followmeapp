package com.followme.model;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by pc on 25/05/17.
 */

public class MyNewTag implements ClusterItem {


    public final Bitmap profilePhoto;
    private final LatLng mPosition;

    public MyNewTag(LatLng position, Bitmap pictureResource) {
        profilePhoto = pictureResource;
        mPosition = position;
    }


    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
