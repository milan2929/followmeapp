package com.followme.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.followme.R;
import com.followme.realm.Contacts;
import com.followme.realm.Tags;
import com.followme.responsemodel.SampleTagModel;
import com.followme.utils.Urls;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;

/**
 * Created by pc on 05/05/17.
 */

public class MessageListAdapter extends BaseAdapter {

    private String TAG = MessageListAdapter.class.getName();

    //    ArrayList<SampleTagModel> data = new ArrayList<>();
    private ArrayList<Tags> data = new ArrayList<>();
    // ImageLoader imageLoader;
    private Context mContext;
    private LayoutInflater infalter;
//    View.OnClickListener mClickListener;
//    int REQUEST_FOR_ACTIVITY_CODE = 101;
    private SparseBooleanArray mSelectedItemsIds;
    private int image_size = 50;
    Realm realm;
    @SuppressWarnings("unused")
    public MessageListAdapter(Context c, ArrayList<Tags> data, Realm realm) {

        infalter = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = c;
        this.data = data;
        this.realm = realm;

        mSelectedItemsIds = new SparseBooleanArray();
        image_size = (int) c.getResources().getDimension(R.dimen.iv_user_image_size);
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Tags getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("DefaultLocale")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {

            convertView = infalter.inflate(R.layout.item_tag, null);

            holder = new ViewHolder();

            holder.ivUserImage = (ImageView) convertView.findViewById(R.id.iv_user_image);
            holder.tvUserName = (TextView) convertView.findViewById(R.id.tv_username);
            holder.tvMessage = (TextView) convertView.findViewById(R.id.tv_message);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date_time);
            holder.tvUnReadCount = (TextView) convertView.findViewById(R.id.tv_unread_count);
            holder.relContent = (RelativeLayout) convertView.findViewById(R.id.rel_content);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {

            Picasso.with(mContext)
                    .load(Urls.BASE_IMAGE_URL +data.get(position).getSender_detail().getProfile_picture())
                    .resize(image_size, image_size)
                    .placeholder(R.drawable.ic_user_default)
                    .centerCrop()
                    .into(holder.ivUserImage);

            Contacts contacts = realm.where(Contacts.class).equalTo("contact_number",data.get(position).getSender_detail().getContact_number()).findFirst();
            if (contacts!=null){
                holder.tvUserName.setText(contacts.getName());
            }else{
                holder.tvUserName.setText(data.get(position).getSender_detail().getContact_number());
            }

            holder.tvMessage.setText(data.get(position).getMessage());


            if (data.get(position).getSender_detail().getMessage_count() == 0) {
                holder.tvUnReadCount.setVisibility(View.GONE);
            } else if (data.get(position).getSender_detail().getMessage_count() > 0) {
                holder.tvUnReadCount.setVisibility(View.VISIBLE);
                holder.tvUnReadCount.setText(String.format("%d", data.get(position).getSender_detail().getMessage_count()));
            }

            String finalDate;
            String currentDate = getCurrentDate();
            String responsedate = parseStringToDate(data.get(position).getCreated_date());
            String yesterDayDate = getYesterDayDate();
            String twodaysago = getTwoDayAgo();
            String threedaysago = getThreeDayAgo();
//            Log.e(TAG, "YesterdayDate==>" + yesterDayDate);
//            Log.e(TAG, "TWO DAYS AGO DATE==>" + twodaysago);
//            Log.e(TAG, "THREE DAYS AGO DATE==>" + threedaysago);


            if (currentDate.equals(responsedate)) {
                //finalDate = parseDate(data.get(position).createdDate);
                finalDate = parseDate(data.get(position).getCreated_date());
            } else if (yesterDayDate.equals(responsedate)) {
                finalDate = "Yesterday";

            } else if (threedaysago.equals(responsedate)) {
                finalDate = "3 days ago";
            } else if (twodaysago.equals(responsedate)) {
                finalDate = "2 days ago";
            } else {
                finalDate = responsedate;
            }


            Log.e(TAG, "FinalDate==>" + finalDate);

            Log.e(TAG, "Response Date===>" + responsedate);
            holder.tvDate.setText(finalDate);


            /** Change background color of the selected items in list view  **/
            convertView.setBackgroundColor(mSelectedItemsIds.get(position) ? 0x99E2E2E2 : Color.TRANSPARENT);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }


    public class ViewHolder {

        ImageView ivUserImage;
        TextView tvUserName;
        TextView tvMessage;
        TextView tvDate;
        //        FrameLayout frameUnread;
        TextView tvUnReadCount;
        RelativeLayout relContent;

    }


    public void remove(SampleTagModel object) {
        data.remove(object);
        notifyDataSetChanged();
    }


    /***
     * Methods required for do selections, remove selections, etc.
     */

    //Toggle selection methods
    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }


    //Remove selected selections
    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }


    //Put or delete selected position into SparseBooleanArray
    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);

        notifyDataSetChanged();
    }

    //Get total selected count
    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    //Return all selected ids
    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }

    public void addAll(ArrayList<Tags> tagsArrayList) {
        this.data = tagsArrayList;
    }


    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        Log.d(TAG, "Current time => " + c.getTime());

//        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
//        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
        String formattedDate = df.format(c.getTime());
        Log.e(TAG, "CurrentDate==>" + formattedDate);

        return formattedDate;
    }


    private String parseStringToDate(String time) {

        Log.e(TAG, " Method time-" + time);

//        2017-05-03 07:42:03
//        yyyy-MM-dd hh:mm:ss
//        String inputPattern = "MMM dd, yyyy hh:mm:ss a";
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
//        String outputPattern = "MM/dd/yy";
        String outputPattern = "dd/MM/yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;

        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    private String getYesterDayDate() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
//        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
    }

    private String getThreeDayAgo() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
//        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
        Calendar cal = Calendar.getInstance();
        //cal.add(Calendar.DATE, -3);
        cal.add(Calendar.DAY_OF_YEAR, -3);
        return dateFormat.format(cal.getTime());

    }

    private String getTwoDayAgo() {

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
//        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
        Calendar cal = Calendar.getInstance();
        //cal.add(Calendar.DATE, -3);
        cal.add(Calendar.DAY_OF_YEAR, -2);
        return dateFormat.format(cal.getTime());

    }


    private String parseDate(String time) {

//        2017-05-03 07:42:03
//        String inputPattern = "MMM dd, yyyy hh:mm:ss a";
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        //String outputPattern = "hh:mm:ss a";
//        String outputPattern = "hh:mm a";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}
