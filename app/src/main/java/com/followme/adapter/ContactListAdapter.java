package com.followme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.followme.R;
import com.followme.realm.Contacts;
import com.followme.utils.Urls;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by pc on 09/05/17.
 */

public class ContactListAdapter extends BaseAdapter {

    private String TAG = ContactListAdapter.class.getName();

    //    ArrayList<SampleContactModelClass> data = new ArrayList<>();
    private ArrayList<Contacts> data = new ArrayList<>();
    // ImageLoader imageLoader;
    private Context mContext;
    private LayoutInflater infalter;
//    View.OnClickListener mClickListener;
//    int REQUEST_FOR_ACTIVITY_CODE = 101;
    int image_size = 50;

//    ArrayList<SampleContactModelClass> selectedContactList = new ArrayList<>();

    @SuppressWarnings("unused")
    public ContactListAdapter(Context c, ArrayList<Contacts> data) {

        infalter = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = c;
        this.data = data;
        image_size = (int) c.getResources().getDimension(R.dimen.iv_contact_image_size);

    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {

            convertView = infalter.inflate(R.layout.item_contact, null);

            holder = new ViewHolder();

            holder.ivUserImage = (ImageView) convertView.findViewById(R.id.iv_contact_image);
            holder.tvUserName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.relContent = (RelativeLayout) convertView.findViewById(R.id.rel_content);
            holder.tvCity = (TextView) convertView.findViewById(R.id.tv_city);
            holder.ivSelectContact = (ImageView) convertView.findViewById(R.id.iv_select_conatct);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        try {


            if (data.get(position).getIs_app_contact().equals("1")) {

                holder.tvUserName.setText(data.get(position).getName());

                if (data.get(position).isSelected()) {
                    holder.ivSelectContact.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_select_contact));
                } else {
                    holder.ivSelectContact.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_disselect_contact));
                }


                if (!data.get(position).getProfile_picture().equals("")) {
                    Picasso.with(mContext)
                            .load(Urls.BASE_IMAGE_URL + data.get(position).getProfile_picture())
                            .placeholder(R.drawable.ic_user_default)
                            .resize(image_size, image_size)
                            .centerCrop()
                            .into(holder.ivUserImage);

//                    Glide.with(mContext)
//                            .load(Urls.BASE_IMAGE_URL + data.get(position).getImage())
//                            .into(holder.ivUserImage);
                }



                holder.relContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {




                        if (data.get(position).isSelected()) {
                            data.get(position).setSelected(false);
                            holder.ivSelectContact.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_disselect_contact));
                        } else {
                            data.get(position).setSelected(true);
                            holder.ivSelectContact.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_select_contact));
                        }
                    }
                });

            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return convertView;
    }

    public class ViewHolder {
        ImageView ivUserImage;
        TextView tvUserName;
        TextView tvCity;
        ImageView ivSelectContact;
        RelativeLayout relContent;

    }

    public ArrayList<Contacts> getSelectedConactList() {
        return this.data;
    }
}
