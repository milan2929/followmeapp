package com.followme.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;

import com.followme.fragments.MyWorldFragment;
import com.followme.fragments.TagCameraFragment;
import com.followme.fragments.TagsFragment;

/**
 * Created by pc on 05/05/17.
 */

public class HomePagerAdapter extends FragmentStatePagerAdapter {

    private int mNumOfTabs;
    private SparseArray<Fragment> fragmentSparseArray =new SparseArray<>();

    public HomePagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    public Fragment getFragment(int position) {
        return fragmentSparseArray.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragmentSparseArray.put(position, new TagCameraFragment());
                break;
             case 1:
                 fragmentSparseArray.put(position, new TagsFragment());
                 break;
            case 2:
                fragmentSparseArray.put(position, new MyWorldFragment());
                break;
        }
        return fragmentSparseArray.get(position);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
