package com.followme.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.TextView;

import com.followme.R;
import com.followme.utils.TouchImageView;
import com.followme.utils.Urls;
import com.squareup.picasso.Picasso;

/**
 * Created by pc on 06/05/17.
 */

public class CustomPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;

    float y1, y2;
    public static boolean flaginWebpager = false;

    String imageUrl;
    String message;

//    int[] mResources = {
//            R.drawable.sample_profile_img,
//            R.drawable.turtle,
//            R.drawable.teacher,
//            R.drawable.john,
//            R.drawable.yeats,
//            R.drawable.stefan
//    };

    int[] mResources = {
            R.drawable.sample,
    };

    public CustomPagerAdapter(Context context, String imageUrl, String message) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imageUrl = imageUrl;
        this.message = message;
    }


    @Override
    public int getCount() {
        return mResources.length;
    }

//    @Override
//    public boolean isViewFromObject(View view, Object object) {
//        return false;
//    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
//        return view == ((LinearLayout) object);
        return (view == object);

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
////        final TouchImageView imgflag = new TouchImageView(container.getContext());
////        final TextView tvDescription = new TextView(container.getContext());
//
//
//        container.addView(imgflag, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        container.addView(tvDescription, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        try {
//
////
////            if (getProductApiResponse != null) {
////
////                Debug.e(TAG, "==getProductApiResponseObj not ===  NULL");
////
////
////                if (getProductApiResponse.getData().getImagesArrayList() != null && getProductApiResponse.getData().getImagesArrayList().size() > 0) {
////
////                    Debug.e(TAG, "==Image URL=====>>>" + getProductApiResponse.getData().getImagesArrayList().get(position).getImage());
////
////                    Picasso.with(ViewPagerFullImage.this)
////                            .load(getProductApiResponse.getData().getImagesArrayList().get(position).getImage())
////                            .into(imgflag);
//
//            imgflag.setImageResource(mResources[position]);
//            tvDescription.setText("Don't let anyone tell you that you're not strong enough.");
//            tvDescription.setTextColor(mContext.getResources().getColor(R.color.white));
//
//
////                }
//
////            }
//
//
//        } catch (Exception e) {
//        }


        // Declare Variables
        TextView tvDescription;
        final TouchImageView imgflag;

        mLayoutInflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = mLayoutInflater.inflate(R.layout.layout_full_image_view, container,
                false);


        tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
        imgflag = (TouchImageView) itemView.findViewById(R.id.iv_image);


//        imgflag.setImageResource(mResources[position]);
//        tvDescription.setText("Don't let anyone tell you that you're not strong enough.");
        Picasso.with(mContext)
                .load(Urls.BASE_TAG_IMAGE_URL + imageUrl)
                .into(imgflag);

//        if(imageUrl.startsWith("http")){
//            Glide.with(mContext).load(imageUrl).into(imgflag);
//        }else {
//            Bitmap bitmap=createBitmap(imageUrl);
//            imgflag.setImageBitmap(bitmap);
//        }

//        Glide.with(mContext).load(imageUrl).into(imgflag);
        tvDescription.setText(message);


        imgflag.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                PointF curr = new PointF(event.getX(), event.getY());
                final int MIN_DISTANCE = 100;
                switch (event.getAction()) {


                    case MotionEvent.ACTION_DOWN:
                        y1 = event.getY();


                        break;


                    case MotionEvent.ACTION_UP:
                        y2 = event.getY();
                        float deltaX = y2 - y1;
                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            int i = 0;
                            if (deltaX < 0)
                                i = 1;
                            if (deltaX > 0)
                                i = 2;
                            if (imgflag.getCurrentZoom() == 1.0) {
                                //Toast.makeText(context, "Button UP:"+getCurrentZoom(), Toast.LENGTH_SHORT).show();
                                TranslateAnimation tAnimation;
                                if (i == 1)
                                    tAnimation = new TranslateAnimation(0, 0, 0, -1550);
                                else
                                    tAnimation = new TranslateAnimation(0, 0, 0, 1550);
                                tAnimation.setDuration(100);
                                tAnimation.setRepeatCount(0);
                                tAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
                                tAnimation.setFillAfter(true);
                                tAnimation.setAnimationListener(new Animation.AnimationListener() {

                                    @Override
                                    public void onAnimationStart(Animation animation) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animation animation) {
                                        // TODO Auto-generated method stub

                                    }

                                    @Override
                                    public void onAnimationEnd(Animation animation) {
                                        // TODO Auto-generated method stub
                                        // imgflag.setVisibility(View.GONE);
                                        flaginWebpager = true;
//                                        finish();
                                    }
                                });

                                imgflag.startAnimation(tAnimation);

                                  /*  Activity act=(Activity)context;
                                    act.finish();
                                    if(i==1)
                                    {


                                     //   imgflag.animate().translationYBy(10000).translationY(0).setDuration(1000).start();
                                        act.overridePendingTransition(0, R.anim.slide_top_down);
                                    }
                                    else
                                        act.overridePendingTransition(0,R.anim.slide_out_down);*/
                            }
                        }
                        break;


                }

                return false;
            }
        });


//            collection.addView(view, 0);


        // Add viewpager_item.xml to ViewPager
        ((ViewPager) container).addView(itemView);
        return itemView;

//        return imgflag;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        container.removeView((LinearLayout) object);
        container.removeView((View) object);
    }


    private Bitmap createBitmap(String imagePath) {

        Bitmap bitmap = null;

        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }


        bitmap = BitmapFactory.decodeFile(imagePath);
        return bitmap;
    }

}
