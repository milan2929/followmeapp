package com.followme.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pc on 24/05/17.
 */

public class AddTagsRequestModel {

    @JsonProperty("secret_key")
    private String secret_key;

    @JsonProperty("access_key")
    private String access_key;

    @JsonProperty("sender_id")
    private String sender_id;

    @JsonProperty("friend_ids")
    private String friend_ids;

    @JsonProperty("latitude")
    private String latitude;

    @JsonProperty("longitude")
    private String longitude;

    @JsonProperty("message")
    private String message;

    @JsonProperty("is_testdata")
    private String is_testdata;

    @JsonProperty("media_data")
    private String media_data;


    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getFriend_ids() {
        return friend_ids;
    }

    public void setFriend_ids(String friend_ids) {
        this.friend_ids = friend_ids;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIs_testdata() {
        return is_testdata;
    }

    public void setIs_testdata(String is_testdata) {
        this.is_testdata = is_testdata;
    }

    public String getMedia_data() {
        return media_data;
    }

    public void setMedia_data(String media_data) {
        this.media_data = media_data;
    }
}
