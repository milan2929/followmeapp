package com.followme.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.followme.realm.Contacts;

import java.util.ArrayList;

/**
 * Created by pc on 20/05/17.
 */

public class GetContactsDetailRequestModel {

    @JsonProperty("contacts")
    private ArrayList<Contacts> contacts =new ArrayList<>();

    @JsonProperty("user_id")
    private int user_id;

    @JsonProperty("secret_key")
    private String secret_key;

    @JsonProperty("access_key")
    private String access_key;

    public ArrayList<Contacts> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contacts> contacts) {
        this.contacts = contacts;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }


}
