package com.followme.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pc on 11/05/17.
 */

public class SendConfirmationCodeRequestModel {

    @JsonProperty("access_key")
    private String access_key;

    @JsonProperty("secret_key")
    private String secret_key;

    @JsonProperty("device_token")
    private String device_token;

    @JsonProperty("device_type")
    private String device_type;

    @JsonProperty("is_testdata")
    private String is_testdata;

    @JsonProperty("country_code")
    private String country_code;

    @JsonProperty("contact_number")
    private String contact_number;

    @JsonProperty("sms_mode")
    private String sms_mode;

    public String getAccessKey() {
        return access_key;
    }

    public void setAccessKey(String accessKey) {
        this.access_key = accessKey;
    }

    public String getSecretKey() {
        return secret_key;
    }

    public void setSecretKey(String secretKey) {
        this.secret_key = secretKey;
    }

    public String getDeviceToken() {
        return device_token;
    }

    public void setDeviceToken(String deviceToken) {
        this.device_token = deviceToken;
    }

    public String getDeviceType() {
        return device_type;
    }

    public void setDeviceType(String deviceType) {
        this.device_type = deviceType;
    }

    public String getIsTestdata() {
        return is_testdata;
    }

    public void setIsTestdata(String isTestdata) {
        this.is_testdata = isTestdata;
    }

    public String getCountryCode() {
        return country_code;
    }

    public void setCountryCode(String countryCode) {
        this.country_code = countryCode;
    }

    public String getContactNumber() {
        return contact_number;
    }

    public void setContactNumber(String contactNumber) {
        this.contact_number = contactNumber;
    }

    public String getSmsMode() {
        return sms_mode;
    }

    public void setSmsMode(String smsMode) {
        this.sms_mode = smsMode;
    }
}
