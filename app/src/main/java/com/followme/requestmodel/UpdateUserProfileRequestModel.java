package com.followme.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pc on 15/05/17.
 */

public class UpdateUserProfileRequestModel {

    @JsonProperty("secret_key")
    private String secret_key;

    @JsonProperty("access_key")
    private String access_key;

    @JsonProperty("user_id")
    private String user_id;

    @JsonProperty("is_testdata")
    private String is_testdata;

    @JsonProperty("contact_number")
    private String contact_number;

    @JsonProperty("profile_picture")
    private String profile_picture;


    public String getSecretKey() {
        return secret_key;
    }

    public void setSecretKey(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getAccessKey() {
        return access_key;
    }

    public void setAccessKey(String access_key) {
        this.access_key = access_key;
    }

    public String getUserId() {
        return user_id;
    }

    public void setUserId(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_testdata() {
        return is_testdata;
    }

    public void setIs_testdata(String is_testdata) {
        this.is_testdata = is_testdata;
    }

    public String getContactNumber() {
        return contact_number;
    }

    public void setContactNumber(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getProfilePicture() {
        return profile_picture;
    }

    public void setProfilePicture(String profile_picture) {
        this.profile_picture = profile_picture;
    }
}
