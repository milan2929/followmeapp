package com.followme.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pc on 27/05/17.
 */

public class GetMessageListRequestModel {

    @JsonProperty("secret_key")
    private String secret_key;

    @JsonProperty("access_key")
    private String access_key;

    @JsonProperty("user_id")
    private String user_id;

    @JsonProperty("is_testdata")
    private String is_testdata;


    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_testdata() {
        return is_testdata;
    }

    public void setIs_testdata(String is_testdata) {
        this.is_testdata = is_testdata;
    }
}
