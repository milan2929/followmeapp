package com.followme.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pc on 11/05/17.
 */

public class RefreshTokenRequestModel {

    @JsonProperty("access_key")
    private String access_key;


    public String getAccessKey() {
        return access_key;
    }

    public void setAccessKey(String access_key) {
        this.access_key = access_key;
    }
}
