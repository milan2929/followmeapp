package com.followme.requestmodel;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by pc on 29/05/17.
 */

public class DeleteTagsRequestModel {

    @JsonProperty("secret_key")
    private String secret_key;

    @JsonProperty("access_key")
    private String access_key;

    @JsonProperty("tag_ids")
    private String tag_ids;

    @JsonProperty("is_testdata")
    private String is_testdata;


    public String getSecret_key() {
        return secret_key;
    }

    public void setSecret_key(String secret_key) {
        this.secret_key = secret_key;
    }

    public String getAccess_key() {
        return access_key;
    }

    public void setAccess_key(String access_key) {
        this.access_key = access_key;
    }

    public String getTag_ids() {
        return tag_ids;
    }

    public void setTag_ids(String tag_ids) {
        this.tag_ids = tag_ids;
    }

    public String getIs_testdata() {
        return is_testdata;
    }

    public void setIs_testdata(String is_testdata) {
        this.is_testdata = is_testdata;
    }
}
