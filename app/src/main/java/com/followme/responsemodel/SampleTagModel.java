package com.followme.responsemodel;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

/**
 * Created by pc on 05/05/17.
 */

public class SampleTagModel implements Serializable{

    private Drawable userImage;
    private String userName;
    private String message;
    private String dateTime;
    private String unReadCount;
    private boolean unReadCountFlag;


    public Drawable getUserImage() {
        return userImage;
    }

    public void setUserImage(Drawable userImage) {
        this.userImage = userImage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getUnReadCount() {
        return unReadCount;
    }

    public void setUnReadCount(String unReadCount) {
        this.unReadCount = unReadCount;
    }

    public boolean isUnReadCountFlag() {
        return unReadCountFlag;
    }

    public void setUnReadCountFlag(boolean unReadCountFlag) {
        this.unReadCountFlag = unReadCountFlag;
    }
}
