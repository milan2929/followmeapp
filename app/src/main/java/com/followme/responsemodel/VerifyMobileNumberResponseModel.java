package com.followme.responsemodel;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by pc on 12/05/17.
 */

public class VerifyMobileNumberResponseModel implements Serializable {

    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
