package com.followme.responsemodel;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pc on 11/05/17.
 */

public class SendConfirmationCodeResponseModel implements Serializable {


    @JsonProperty("userToken")
    private String userToken;
    @JsonProperty("is_newuser")
    private String is_newuser;
    @JsonProperty("User")
    private List<User> User;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getIs_newuser() {
        return is_newuser;
    }

    public void setIs_newuser(String is_newuser) {
        this.is_newuser = is_newuser;
    }

    public List<User> getUser() {
        return User;
    }

    public void setUser(List<User> User) {
        this.User = User;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class User  implements Serializable {
        @JsonProperty("id")
        private String id;
        @JsonProperty("firstname")
        private String firstname;
        @JsonProperty("lastname")
        private String lastname;
        @JsonProperty("username")
        private String username;
        @JsonProperty("email")
        private String email;
        @JsonProperty("is_verified")
        private String is_verified;
        @JsonProperty("is_push_enabled")
        private String is_push_enabled;
        @JsonProperty("verification_code")
        private String verification_code;
        @JsonProperty("profile_picture")
        private String profile_picture;
        @JsonProperty("device_token")
        private String device_token;
        @JsonProperty("device_type")
        private String device_type;
        @JsonProperty("contact_number")
        private String contact_number;
        @JsonProperty("country_code")
        private String country_code;
        @JsonProperty("created_date")
        private String created_date;
        @JsonProperty("modified_date")
        private String modified_date;
        @JsonProperty("guid")
        private String guid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getIs_verified() {
            return is_verified;
        }

        public void setIs_verified(String is_verified) {
            this.is_verified = is_verified;
        }

        public String getIs_push_enabled() {
            return is_push_enabled;
        }

        public void setIs_push_enabled(String is_push_enabled) {
            this.is_push_enabled = is_push_enabled;
        }

        public String getVerification_code() {
            return verification_code;
        }

        public void setVerification_code(String verification_code) {
            this.verification_code = verification_code;
        }

        public String getProfile_picture() {
            return profile_picture;
        }

        public void setProfile_picture(String profile_picture) {
            this.profile_picture = profile_picture;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public String getDevice_type() {
            return device_type;
        }

        public void setDevice_type(String device_type) {
            this.device_type = device_type;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getModified_date() {
            return modified_date;
        }

        public void setModified_date(String modified_date) {
            this.modified_date = modified_date;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }
    }
}
