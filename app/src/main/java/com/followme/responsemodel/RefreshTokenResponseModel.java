package com.followme.responsemodel;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pc on 11/05/17.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefreshTokenResponseModel implements Serializable {

    @JsonProperty("tempToken")
    private String tempToken;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;
    @JsonProperty("adminConfig")
    private List<AdminConfig> adminConfig;

    public String getTempToken() {
        return tempToken;
    }

    public void setTempToken(String tempToken) {
        this.tempToken = tempToken;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AdminConfig> getAdminConfig() {
        return adminConfig;
    }

    public void setAdminConfig(List<AdminConfig> adminConfig) {
        this.adminConfig = adminConfig;
    }

    public static class AdminConfig {
        @JsonProperty("globalPassword")
        private String globalPassword;

        public String getGlobalPassword() {
            return globalPassword;
        }

        public void setGlobalPassword(String globalPassword) {
            this.globalPassword = globalPassword;
        }

        private String userAgent;
        private String tempToken;

        @JsonProperty("userAgent")
        public String getUserAgent() {
            return userAgent;
        }

        public void setUserAgent(String userAgent) {
            this.userAgent = userAgent;
        }

        @JsonProperty("tempToken")
        public String getTempToken() {
            return tempToken;
        }

        public void setTempToken(String tempToken) {
            this.tempToken = tempToken;
        }
    }
}
