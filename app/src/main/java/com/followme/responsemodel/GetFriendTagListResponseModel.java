package com.followme.responsemodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.followme.realm.Tags;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by pc on 30/05/17.
 */

public class GetFriendTagListResponseModel implements Serializable {


    @JsonProperty("Tags")
    private ArrayList<com.followme.realm.Tags> Tags;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;

    public ArrayList<Tags> getTags() {
        return Tags;
    }

    public void setTags(ArrayList<Tags> Tags) {
        this.Tags = Tags;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
