package com.followme.responsemodel;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pc on 15/05/17.
 */

public class UpdateUserProfileResponseModel implements Serializable {


    @JsonProperty("User")
    private List<User> User;
    @JsonProperty("status")
    private String status;
    @JsonProperty("message")
    private String message;

    public List<User> getUser() {
        return User;
    }

    public void setUser(List<User> User) {
        this.User = User;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class User {
        @JsonProperty("id")
        private int id;
        @JsonProperty("username")
        private String username;
        @JsonProperty("firstname")
        private String firstname;
        @JsonProperty("lastname")
        private String lastname;
        @JsonProperty("email")
        private String email;
        @JsonProperty("password")
        private String password;
        @JsonProperty("profile_picture")
        private String profile_picture;
        @JsonProperty("country_code")
        private String country_code;
        @JsonProperty("contact_number")
        private String contact_number;
        @JsonProperty("verification_code")
        private String verification_code;
        @JsonProperty("is_verified")
        private int is_verified;
        @JsonProperty("device_token")
        private String device_token;
        @JsonProperty("device_type")
        private int device_type;
        @JsonProperty("is_delete")
        private int is_delete;
        @JsonProperty("is_testdata")
        private int is_testdata;
        @JsonProperty("created_date")
        private String created_date;
        @JsonProperty("modified_date")
        private String modified_date;
        @JsonProperty("is_push_enabled")
        private int is_push_enabled;
        @JsonProperty("guid")
        private String guid;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getProfile_picture() {
            return profile_picture;
        }

        public void setProfile_picture(String profile_picture) {
            this.profile_picture = profile_picture;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

        public String getVerification_code() {
            return verification_code;
        }

        public void setVerification_code(String verification_code) {
            this.verification_code = verification_code;
        }

        public int getIs_verified() {
            return is_verified;
        }

        public void setIs_verified(int is_verified) {
            this.is_verified = is_verified;
        }

        public String getDevice_token() {
            return device_token;
        }

        public void setDevice_token(String device_token) {
            this.device_token = device_token;
        }

        public int getDevice_type() {
            return device_type;
        }

        public void setDevice_type(int device_type) {
            this.device_type = device_type;
        }

        public int getIs_delete() {
            return is_delete;
        }

        public void setIs_delete(int is_delete) {
            this.is_delete = is_delete;
        }

        public int getIs_testdata() {
            return is_testdata;
        }

        public void setIs_testdata(int is_testdata) {
            this.is_testdata = is_testdata;
        }

        public String getCreated_date() {
            return created_date;
        }

        public void setCreated_date(String created_date) {
            this.created_date = created_date;
        }

        public String getModified_date() {
            return modified_date;
        }

        public void setModified_date(String modified_date) {
            this.modified_date = modified_date;
        }

        public int getIs_push_enabled() {
            return is_push_enabled;
        }

        public void setIs_push_enabled(int is_push_enabled) {
            this.is_push_enabled = is_push_enabled;
        }

        public String getGuid() {
            return guid;
        }

        public void setGuid(String guid) {
            this.guid = guid;
        }
    }
}
