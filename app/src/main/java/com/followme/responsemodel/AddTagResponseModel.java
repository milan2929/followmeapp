package com.followme.responsemodel;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.followme.realm.Tags;

import java.io.Serializable;

/**
 * Created by pc on 24/05/17.
 */

public class AddTagResponseModel implements Serializable {


    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("Tag")
    private Tags tag;


    public Tags getTag() {
        return tag;
    }

    public void setTags(Tags tag) {
        tag = tag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//
//
//    public class Tag implements Serializable,ClusterItem{
//
//
//        @JsonProperty("tag_id")
//        private int tag_id;
//
//        @JsonProperty("sender_id")
//        private String sender_id;
//
//        @JsonProperty("friend_ids")
//        private String friend_ids;
//
//        @JsonProperty("message")
//        private String message;
//
//        @JsonProperty("recipients_detail")
//        private String recipients_detail;
//
//        @JsonProperty("latitude")
//        private String latitude;
//
//        @JsonProperty("longitude")
//        private String longitude;
//
//        @JsonProperty("read_count")
//        private int read_count;
//
//        @JsonProperty("created_date")
//        private String created_date;
//
//        @JsonProperty("modified_date")
//        private String modified_date;
//
//        @JsonProperty("friend_detail")
//        private ArrayList<FriendDetail> friend_detail;
//
//        @JsonProperty("media")
//        private Media media;
//
//        private LatLng mPosition;
//
//
//        public int getTag_id() {
//            return tag_id;
//        }
//
//        public void setTag_id(int tag_id) {
//            this.tag_id = tag_id;
//        }
//
//        public String getSender_id() {
//            return sender_id;
//        }
//
//        public void setSender_id(String sender_id) {
//            this.sender_id = sender_id;
//        }
//
//        public String getFriend_ids() {
//            return friend_ids;
//        }
//
//        public void setFriend_ids(String friend_ids) {
//            this.friend_ids = friend_ids;
//        }
//
//        public String getMessage() {
//            return message;
//        }
//
//        public void setMessage(String message) {
//            this.message = message;
//        }
//
//        public String getRecipients_detail() {
//            return recipients_detail;
//        }
//
//        public void setRecipients_detail(String recipients_detail) {
//            this.recipients_detail = recipients_detail;
//        }
//
//        public String getLatitude() {
//            return latitude;
//        }
//
//        public void setLatitude(String latitude) {
//            this.latitude = latitude;
//        }
//
//        public String getLongitude() {
//            return longitude;
//        }
//
//        public void setLongitude(String longitude) {
//            this.longitude = longitude;
//        }
//
//        public int getRead_count() {
//            return read_count;
//        }
//
//        public void setRead_count(int read_count) {
//            this.read_count = read_count;
//        }
//
//        public String getCreated_date() {
//            return created_date;
//        }
//
//        public void setCreated_date(String created_date) {
//            this.created_date = created_date;
//        }
//
//        public String getModified_date() {
//            return modified_date;
//        }
//
//        public void setModified_date(String modified_date) {
//            this.modified_date = modified_date;
//        }
//
//        public ArrayList<FriendDetail> getFriend_detail() {
//            return friend_detail;
//        }
//
//        public void setFriend_detail(ArrayList<FriendDetail> friend_detail) {
//            this.friend_detail = friend_detail;
//        }
//
//        public Media getMedia() {
//            return media;
//        }
//
//        public void setMedia(Media media) {
//            this.media = media;
//        }
//
//        public LatLng getmPosition() {
//            return mPosition;
//        }
//
//        public void setmPosition(LatLng mPosition) {
//            this.mPosition = mPosition;
//        }
//
//        @Override
//        public LatLng getPosition() {
//            return null;
//        }
//
//        @Override
//        public String getTitle() {
//            return null;
//        }
//
//        @Override
//        public String getSnippet() {
//            return null;
//        }
//    }
//
//    public class FriendDetail implements Serializable{
//
//        @JsonProperty("user_id")
//        private int user_id;
//
//        @JsonProperty("country_code")
//        private String country_code;
//
//        @JsonProperty("contact_number")
//        private String contact_number;
//
//        @JsonProperty("profile_picture")
//        private String profile_picture;
//
//
//        public int getUser_id() {
//            return user_id;
//        }
//
//        public void setUser_id(int user_id) {
//            this.user_id = user_id;
//        }
//
//        public String getCountry_code() {
//            return country_code;
//        }
//
//        public void setCountry_code(String country_code) {
//            this.country_code = country_code;
//        }
//
//        public String getContact_number() {
//            return contact_number;
//        }
//
//        public void setContact_number(String contact_number) {
//            this.contact_number = contact_number;
//        }
//
//        public String getProfile_picture() {
//            return profile_picture;
//        }
//
//        public void setProfile_picture(String profile_picture) {
//            this.profile_picture = profile_picture;
//        }
//    }
//
//
//    public class Media implements Serializable{
//
//        @JsonProperty("media_id")
//        private int media_id;
//
//        @JsonProperty("media_type")
//        private String media_type;
//
//        @JsonProperty("media_url")
//        private String media_url;
//
//
//        public int getMedia_id() {
//            return media_id;
//        }
//
//        public void setMedia_id(int media_id) {
//            this.media_id = media_id;
//        }
//
//        public String getMedia_type() {
//            return media_type;
//        }
//
//        public void setMedia_type(String media_type) {
//            this.media_type = media_type;
//        }
//
//        public String getMedia_url() {
//            return media_url;
//        }
//
//        public void setMedia_url(String media_url) {
//            this.media_url = media_url;
//        }
//    }
}
