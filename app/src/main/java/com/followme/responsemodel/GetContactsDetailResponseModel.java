package com.followme.responsemodel;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.followme.realm.Contacts;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by pc on 20/05/17.
 */

public class GetContactsDetailResponseModel implements Serializable {


   @JsonProperty("Contacts")
    private ArrayList<com.followme.realm.Contacts> Contacts;
   @JsonProperty("status")
    private String status;
   @JsonProperty("message")
    private String message;

    public ArrayList<Contacts> getContacts() {
        return Contacts;
    }

    public void setContacts(ArrayList<Contacts> Contacts) {
        this.Contacts = Contacts;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

 }
