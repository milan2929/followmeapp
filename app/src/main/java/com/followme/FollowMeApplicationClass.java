package com.followme;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.multidex.MultiDexApplication;

import com.google.android.gms.ads.MobileAds;

import io.realm.Realm;

/**
 * Created by pc on 04/05/17.
 */

public class FollowMeApplicationClass extends MultiDexApplication {

    public static FollowMeApplicationClass instance = null;

    public static FollowMeApplicationClass getInstance() {
        if (null == instance) {
            instance = new FollowMeApplicationClass();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(getApplicationContext());
        MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.admob_app_id));
    }

    public static Bitmap getBitmapFromPath(String photoPath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(photoPath, options);
    }
}
