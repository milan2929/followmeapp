package com.followme.realm;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by pc on 20/05/17.
 */

@RealmClass
public class Contacts extends RealmObject implements Serializable {

    @JsonProperty("country_coder")
    private String country_coder;

    @JsonProperty("contact_number")
    @PrimaryKey
    private String contact_number;

    @JsonProperty("name")
    private String name;

    private boolean isSelected = false;

    private String image;

    private String user_id;

    private String is_app_contact;

    private int app_version;

    public int getApp_version() {
        return app_version;
    }

    private int record_id;

    private String profile_picture;

    public String getCountry_coder() {
        return country_coder;
    }

    public void setCountry_coder(String country_coder) {
        this.country_coder = country_coder;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIs_app_contact() {
        return is_app_contact;
    }

    public void setIs_app_contact(String is_app_contact) {
        this.is_app_contact = is_app_contact;
    }

    public void setApp_version(int app_version) {
        this.app_version = app_version;
    }

    public int getRecord_id() {
        return record_id;
    }

    public void setRecord_id(int record_id) {
        this.record_id = record_id;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }
}
