package com.followme.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.R;
import com.followme.adapter.HomePagerAdapter;
import com.followme.fragments.TagCameraFragment;
import com.followme.fragments.TagsFragment;
import com.followme.helper.ReadContactsApi;
import com.followme.realm.Contacts;
import com.followme.requestmodel.GetContactsDetailRequestModel;
import com.followme.services.GPSTracker;
import com.followme.utils.Constants;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.followme.ws.WebserviceWrapper;
import com.followme.ws.WsResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.internal.Util;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String TAG = HomeActivity.class.getName();

    private Toolbar toolbar;

    private DrawerLayout drawer;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private ProgressDialog progressDialog;


    public static Fragment selectedFragment;
    final static int MY_REQUEST_CODE = 3333;
    final static int REQ_VIEW_FULL_IMAGE = 4444;


    private ViewPager viewPager;
    private TabLayout tabLayout;
    private HomePagerAdapter adapter;

    public static Fragment fragment;
    public Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        progressDialog = new ProgressDialog(this);
        realm = getRealm();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int CAMERA = checkSelfPermission(Manifest.permission.CAMERA);
            int READ_PHONE_STATE = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            int ACCESS_COARSE_LOCATION = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            int ACCESS_FINE_LOCATION = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            int READ_EXTERNAL_STORAGE = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            int WRITE_CONTACTS = checkSelfPermission(Manifest.permission.WRITE_CONTACTS);
            int READ_CONTACTS = checkSelfPermission(Manifest.permission.READ_CONTACTS);

            List<String> listPermissionsNeeded = new ArrayList<>();

            if (CAMERA != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }

            if (READ_PHONE_STATE != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }

            if (ACCESS_COARSE_LOCATION != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }

            if (ACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (WRITE_CONTACTS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
            }

            if (READ_CONTACTS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                Log.i("Permission size", listPermissionsNeeded.size() + " ");
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_REQUEST_CODE);
            } else {

                new ReadContactsApi(HomeActivity.this, completeSyncing);
                if (!Utils.getBooleanFromUserDefaults(HomeActivity.this, Constants.isContactSynced)) {
                    progressDialog.setMessage("Syncing your contact..");
                    progressDialog.show();
                }
            }
        } else {
            new ReadContactsApi(HomeActivity.this, completeSyncing);
            if (!Utils.getBooleanFromUserDefaults(HomeActivity.this, Constants.isContactSynced)) {
                progressDialog.setMessage("Syncing your contact..");
                progressDialog.show();
            }
        }

        init();
        initTab();

        GPSTracker gps = new GPSTracker(HomeActivity.this);
        if (gps.canGetLocation()) {
            Utils.saveToUserDefaults(HomeActivity.this, Constants.LATITUDE, String.valueOf(gps.getLatitude()));
            Utils.saveToUserDefaults(HomeActivity.this, Constants.LONGITUDE, String.valueOf(gps.getLongitude()));
        }

    }


    ReadContactsApi.CompleteSyncing completeSyncing = new ReadContactsApi.CompleteSyncing() {
        @Override
        public void onComplete() {
            Utils.saveSettingsToUserDefaults(HomeActivity.this, Constants.isContactSynced, true);
            getContactsDetail();
            progressDialog.dismiss();

        }
    };


    private void getContactsDetail() {

        GetContactsDetailRequestModel getContactsDetailRequestModel = new GetContactsDetailRequestModel();
        getContactsDetailRequestModel.setAccess_key(Utils.getFromUserDefaults(HomeActivity.this, Constants.ACCESS_KEY));
        getContactsDetailRequestModel.setSecret_key(Utils.getFromUserDefaults(HomeActivity.this, Constants.SECRET_KEY));
        getContactsDetailRequestModel.setUser_id(Integer.parseInt(Utils.getFromUserDefaults(HomeActivity.this, Constants.USER_ID)));
        getContactsDetailRequestModel.getContacts().addAll(realm.where(Contacts.class).findAll());

        try {
            JSONObject object = new JSONObject(getMapper().writeValueAsString(getContactsDetailRequestModel));
            WebserviceWrapper webserviceWrapper = new WebserviceWrapper(this, object, new WsResponseListener() {
                @Override
                public void onDelieverResponse(String serviceType, Exception err, Object data) {

                    try {
                        if (data != null) {
                            JSONObject object1 = new JSONObject(data.toString());
                            if (object1.getInt("status") == 1) {
                                final JSONArray array = object1.getJSONArray("Contacts");
                                if (array.length() > 0) {
                                    realm.executeTransactionAsync(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.createOrUpdateAllFromJson(Contacts.class, array);
                                            Log.e(TAG, "execute: "+realm.where(Contacts.class).equalTo("is_app_contact", "1").count());
                                        }
                                    });

                                }

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            webserviceWrapper.execute(Urls.GET_CONTACTS_DETAIL);
        } catch (JSONException | JsonProcessingException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null)
            realm.close();
    }

    //    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//
//            if (selectedFragment != null) {
//                if (selectedFragment instanceof HomeFragment) {
//                    // code here.
//                    Fragment fragment = ((HomeFragment) selectedFragment).getFragment();
//                    if (fragment instanceof TagCameraFragment) {
//                        ((HomeFragment) selectedFragment).setTab(1);
//                    } else if (fragment instanceof MyWorldFragment) {
//                        ((HomeFragment) selectedFragment).setTab(1);
//                    } else {
//                        super.onBackPressed();
//                    }
//                } else if (selectedFragment instanceof ProfileFragment) {
//                    displaySelectedScreen(R.id.nav_home);
//                } else if (selectedFragment instanceof SettingsFragment) {
//                    displaySelectedScreen(R.id.nav_home);
//                } else {
//                    super.onBackPressed();
//                }
//            } else {
//                super.onBackPressed();
//            }
//
//        }
//    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (viewPager.getCurrentItem() == 0) {
                viewPager.setCurrentItem(1);
            } else if (viewPager.getCurrentItem() == 2) {
                viewPager.setCurrentItem(1);
            } else {
                super.onBackPressed();
            }
        }
    }


//
//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        int id = item.getItemId();
//
//
//        displaySelectedScreen(item.getItemId());
//
//        return false;
//
//    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_profile: {
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivityForResult(intent, 700);

                break;
            }
            case R.id.nav_settings: {
                Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
                startActivity(intent);
                break;
            }
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void initTab() {


        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.ic_camera)));
        tabLayout.addTab(tabLayout.newTab().setText("Tags"));
        tabLayout.addTab(tabLayout.newTab().setText("MyWorld"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(2);
        adapter = new HomePagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                fragment = (Fragment) adapter.instantiateItem(viewPager, viewPager.getCurrentItem());
//                fragment = adapter.getItem(tab.getPosition());
//                if (tab.getPosition()==0 && adapter.getFragment(0)!=null){
//                    ((TagCameraFragment)adapter.getFragment(0)).startCamera();
//                }

                if (tab.getPosition()==0&& adapter.getFragment(0)!=null){
                    ((TagCameraFragment)adapter.getFragment(0)).startSenssor();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                if (tab.getPosition()==0 && adapter.getFragment(0)!=null){
//                    ((TagCameraFragment)adapter.getFragment(0)).stopCamera();
//                }

                if (tab.getPosition()==0&& adapter.getFragment(0)!=null){
                    ((TagCameraFragment)adapter.getFragment(0)).removeSenssor();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        viewPager.setCurrentItem(1);
        fragment = (Fragment) adapter.instantiateItem(viewPager, 1);


    }


    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.showOverflowMenu();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Follow Me");


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        if (id == R.id.action_view) {
//            Intent intent = new Intent(HomeActivity.this, ViewPagerFullImage.class);
//            if (viewPager.getCurrentItem() == 2) {
//                Fragment fragment = getFragment();
//                if (fragment != null) {
//                    if (((MyWorldFragment) fragment).myTag != null) {
//                        intent.putExtra("imageUrl", ((MyWorldFragment) fragment).myTag.getMedia().getMedia_url());
//                        intent.putExtra("message", ((MyWorldFragment) fragment).myTag.getMessage());
//                    }
//                }
//            }
//            startActivityForResult(intent, REQ_VIEW_FULL_IMAGE);
//        } else if (id == R.id.action_share) {
//            Toast.makeText(HomeActivity.this, "Share", Toast.LENGTH_SHORT).show();
//        } else if (id == R.id.action_delete) {
//
//        } else if (id == R.id.action_refresh) {
//            Toast.makeText(HomeActivity.this, "Refresh", Toast.LENGTH_SHORT).show();
//        }

        return super.onOptionsItemSelected(item);
    }




    public void setTab(int position) {
        if (viewPager != null) {
            viewPager.setCurrentItem(position);
        }
    }

    //Return current fragment on basis of Position
    public Fragment getFragment() {
//        return adapter.getItem(pos);
        return this.fragment;
    }


//    public Fragment getFragment(int pos) {
//
//        return selectedFragment;
//    }

//    @Override
//    protected void onResume() {
//        super.onResume();
//        Log.e(TAG, "NO RESUME CALL");
//
//
//
//        if (selectedFragment == null) {
//            displaySelectedScreen(R.id.nav_home);
//        } else if (selectedFragment instanceof HomeFragment) {
//            displaySelectedScreen(R.id.nav_home);
//        }
//
//    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "NO RESUME CALL");
//        viewPager.setCurrentItem(1);

        if (viewPager.getCurrentItem() == 0) {
            viewPager.setCurrentItem(1);
        } else if (viewPager.getCurrentItem() == 1) {
            Fragment fragment = getFragment();
            if (fragment != null) {
                if (((TagsFragment) fragment).isNewTagIniatiate) {
                    ((TagsFragment) fragment).isNewTagIniatiate = false;
                    viewPager.setCurrentItem(2);
                }
            }

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case MY_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {


                            requestPermissions(new String[]{
                                    Manifest.permission.READ_PHONE_STATE,
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_CONTACTS,
                                    Manifest.permission.READ_CONTACTS}, MY_REQUEST_CODE);

                        }
                    }

                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_VIEW_FULL_IMAGE) {
                viewPager.setCurrentItem(2);
            }
            if (requestCode == 700){
                Intent intent = new Intent(this, SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                this.finish();
            }
        }
    }


}
