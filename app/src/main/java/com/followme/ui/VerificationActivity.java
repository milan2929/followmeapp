package com.followme.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.R;
import com.followme.helper.Security;
import com.followme.requestmodel.SendConfirmationCodeRequestModel;
import com.followme.requestmodel.VerifyMobileNumberRequestModel;
import com.followme.responsemodel.SendConfirmationCodeResponseModel;
import com.followme.responsemodel.VerifyMobileNumberResponseModel;
import com.followme.utils.Constants;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.Phonenumber;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class VerificationActivity extends BaseActivity {

    private String TAG = VerificationActivity.class.getName();

    private EditText etVerificationCode;
    private ImageView ivVerify;
    private TextView tvResendCode;
    private ProgressWheel progressWheel;

    final static int MY_REQUEST_CODE = 1111;

    private SendConfirmationCodeResponseModel sendConfirmationCodeResponseModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);


        sendConfirmationCodeResponseModel = (SendConfirmationCodeResponseModel) getIntent().getSerializableExtra("data");
        if (sendConfirmationCodeResponseModel != null) {
            Log.e(TAG, "USER TOKEN-" + sendConfirmationCodeResponseModel.getUserToken());
            Log.e(TAG, "VERIFICATION CODE-" + sendConfirmationCodeResponseModel.getUser().get(0).getVerification_code());
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.READ_PHONE_STATE,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_CONTACTS,
                                Manifest.permission.READ_CONTACTS},
                        MY_REQUEST_CODE);


            } else {

            }

        }


        init();

        ivVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                ivVerify.startAnimation(animation);

                String verificationCode = etVerificationCode.getText().toString().trim();

                if (validate(verificationCode)) {

                    etVerificationCode.setError(null);
                    progressWheel.setVisibility(View.VISIBLE);
                    verifyMobileNumber();

//                    Intent intent = new Intent(VerificationActivity.this, HomeActivity.class);
//                    startActivity(intent);
//                    VerificationActivity.this.finish();
                }


            }
        });

    }

    private void init() {
        etVerificationCode = (EditText) findViewById(R.id.et_verification_code);
        ivVerify = (ImageView) findViewById(R.id.iv_verify);
        tvResendCode = (TextView) findViewById(R.id.tv_resend);
        progressWheel = (ProgressWheel) findViewById(R.id.progress);
        tvResendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressWheel.setVisibility(View.VISIBLE);
                sendConfirmationCode();

            }
        });

//        if (sendConfirmationCodeResponseModel != null) {
//            showVerificationCodeAlertDialog(VerificationActivity.this, "Your Follow Me verification code is " + sendConfirmationCodeResponseModel.getUser().get(0).getVerification_code());
//        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case MY_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {

//                            requestPermissions(new String[]{Manifest.permission.CAMERA},
//                                    MY_REQUEST_CODE);

                            requestPermissions(new String[]{Manifest.permission.CAMERA,
                                            Manifest.permission.READ_PHONE_STATE,
                                            Manifest.permission.ACCESS_COARSE_LOCATION,
                                            Manifest.permission.ACCESS_FINE_LOCATION,
                                            Manifest.permission.READ_EXTERNAL_STORAGE,
                                            Manifest.permission.WRITE_CONTACTS,
                                            Manifest.permission.READ_CONTACTS},
                                    MY_REQUEST_CODE);


                        }
                    }

                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean validate(String verificationCode) {
        boolean result = true;

        if (TextUtils.isEmpty(verificationCode)) {
            result = false;
            etVerificationCode.setError("Please insert verification code");
            etVerificationCode.setFocusableInTouchMode(true);
            etVerificationCode.setFocusable(true);
            etVerificationCode.requestFocus();
            etVerificationCode.setSelection(verificationCode.length());

        }

        return result;
    }

    private void verifyMobileNumber() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");

//        String accessKey = AESHelper.encrypt(ConstantMethod.getPreference(getActivity(),UserDefault.kGUID),
//                ConstantMethod.getPreference(getActivity(), UserDefault.kGlobalPass));

        Log.e(TAG, "GUID-" + sendConfirmationCodeResponseModel.getUser().get(0).getGuid());
        Log.e(TAG, "GLOBAL PASSWORD-" + Utils.getFromUserDefaults(VerificationActivity.this, Constants.GLOBAL_PASSWORD));

//        String accessKey = AESHelper.encrypt(sendConfirmationCodeResponseModel.getUser().get(0).getGuid(), Utils.getFromUserDefaults(VerificationActivity.this, Constants.GLOBAL_PASSWORD));

        VerifyMobileNumberRequestModel verifyMobileNumberRequestModel = new VerifyMobileNumberRequestModel();
        verifyMobileNumberRequestModel.setSecret_key(Utils.getFromUserDefaults(VerificationActivity.this, Constants.SECRET_KEY));
//        verifyMobileNumberRequestModel.setAccess_key(sendConfirmationCodeResponseModel.getUserToken());
        verifyMobileNumberRequestModel.setAccess_key(Utils.getFromUserDefaults(VerificationActivity.this, Constants.ACCESS_KEY));
        verifyMobileNumberRequestModel.setVerification_code(etVerificationCode.getText().toString());
        verifyMobileNumberRequestModel.setUser_id(sendConfirmationCodeResponseModel.getUser().get(0).getId());
        verifyMobileNumberRequestModel.setIs_testdata("1");


        StringEntity entity = null;
        try {
            entity = new StringEntity(getMapper().writer().writeValueAsString(verifyMobileNumberRequestModel));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "VERIFY MOBILE NUMBER URL-" + Urls.VERIFY_MOBILE_NUMBER);
        client.post(VerificationActivity.this, Urls.VERIFY_MOBILE_NUMBER, entity, "application/json", new GetVerifyMobileNumberResponseHandler());
    }

    private class GetVerifyMobileNumberResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            progressWheel.setVisibility(View.GONE);

            if (responseBody != null) {

                Log.e(TAG, "VERIFY MOBILE NUMBER RESPONSE-" + new String(responseBody));
                VerifyMobileNumberResponseModel verifyMobileNumberResponseModel = null;
                try {
                    verifyMobileNumberResponseModel = getMapper().readValue(responseBody, VerifyMobileNumberResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (verifyMobileNumberResponseModel != null && verifyMobileNumberResponseModel.getStatus().equals("1")) {
                    Utils.saveBooleanToUserDefaults(VerificationActivity.this, Constants.IS_NUMBER_VERIFIED, true);

                    showAlertDialog(VerificationActivity.this, verifyMobileNumberResponseModel.getMessage());
                }else if (verifyMobileNumberResponseModel!=null){
                    Toast.makeText(VerificationActivity.this, verifyMobileNumberResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
//            Log.e(TAG, "FAILURE RESPONSE-" + new String(responseBody));
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }


    private void showAlertDialog(Context context, String message) {
        final Dialog alertDialogs = new Dialog(context);
        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogs.setContentView(R.layout.alert_no_connection);

        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView txtaltmsg = (TextView) alertDialogs.findViewById(R.id.txtaltmsg);
        TextView txtaltok = (TextView) alertDialogs.findViewById(R.id.tv_ok);


        txtaltmsg.setText(message);


        alertDialogs.setCancelable(false);

        txtaltok.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                Intent intent = new Intent(VerificationActivity.this, HomeActivity.class);
                startActivity(intent);
                VerificationActivity.this.finish();

                alertDialogs.dismiss();
            }

        });

        alertDialogs.show();
    }


    private void sendConfirmationCode() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");

        SendConfirmationCodeRequestModel sendConfirmationCodeRequestModel = new SendConfirmationCodeRequestModel();

        sendConfirmationCodeRequestModel.setAccessKey(Constants.ACCESS_KEY_VALUE);
        sendConfirmationCodeRequestModel.setSecretKey(Utils.getFromUserDefaults(VerificationActivity.this, Constants.SECRET_KEY));
        sendConfirmationCodeRequestModel.setDeviceToken(Utils.getDeviceToken(VerificationActivity.this));
        sendConfirmationCodeRequestModel.setDeviceType("2");
        sendConfirmationCodeRequestModel.setIsTestdata("1");
        sendConfirmationCodeRequestModel.setCountryCode(sendConfirmationCodeResponseModel.getUser().get(0).getCountry_code());
        sendConfirmationCodeRequestModel.setContactNumber(sendConfirmationCodeResponseModel.getUser().get(0).getContact_number());
        sendConfirmationCodeRequestModel.setSmsMode("Nexmo");

        StringEntity entity = null;
        try {
            entity = new StringEntity(new JSONObject(getMapper().writeValueAsString(sendConfirmationCodeRequestModel)).toString(), "UTF-8");
            entity.setContentType("application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "SEND CONFIRMATION CODE URL-" + Urls.SEND_CONFIRMATION_CODE);
        client.post(VerificationActivity.this, Urls.SEND_CONFIRMATION_CODE, entity, "application/json", new VerificationActivity.GetSendConfirmationCodeResponseHandler());


    }

    private class GetSendConfirmationCodeResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            progressWheel.setVisibility(View.GONE);

            if (responseBody != null) {

                Log.e(TAG, "SEND CONFIRMATION CODE RESPONSE-" + new String(responseBody));
                SendConfirmationCodeResponseModel sendConfirmationCodeResponseModel = null;
                try {
                    sendConfirmationCodeResponseModel = getMapper().readValue(responseBody, SendConfirmationCodeResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sendConfirmationCodeResponseModel != null && sendConfirmationCodeResponseModel.getStatus().equals("1")) {

                    Utils.saveToUserDefaults(VerificationActivity.this, Constants.USER_ID, sendConfirmationCodeResponseModel.getUser().get(0).getId());
                    Utils.saveToUserDefaults(VerificationActivity.this, Constants.CONTACT_NUMBER, sendConfirmationCodeResponseModel.getUser().get(0).getContact_number());

                    String SECRET_KEY_VALUE = sendConfirmationCodeResponseModel.getUserToken();
                    String ACCESS_KEY_VALUE = Security.encrypt(sendConfirmationCodeResponseModel.getUser().get(0).getGuid(), Utils.getFromUserDefaults(VerificationActivity.this, Constants.GLOBAL_PASSWORD));

                    Utils.saveToUserDefaults(VerificationActivity.this, Constants.SECRET_KEY, SECRET_KEY_VALUE);
                    Utils.saveToUserDefaults(VerificationActivity.this, Constants.ACCESS_KEY, ACCESS_KEY_VALUE);

                    Toast.makeText(VerificationActivity.this, sendConfirmationCodeResponseModel.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            progressWheel.setVisibility(View.GONE);

            Log.e(TAG, "STATUS CODE-" + statusCode);
//            Log.e(TAG, "FAILURE RESPONSE-" + new String(responseBody));
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();

        }
    }
}
