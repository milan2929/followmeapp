package com.followme.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.followme.R;
import com.followme.adapter.ContactListAdapter;
import com.followme.realm.Contacts;
import com.followme.utils.Constants;
import com.followme.utils.Utils;

import java.util.ArrayList;

import io.realm.Realm;

public class SelectContactActivity extends BaseActivity {

    private String TAG = SelectContactActivity.class.getName();

    private ImageView ivClose;
    private ImageView ivDone;
    private ListView lvContacts;
    private LinearLayout linearLayoutNoContact;
    private ContactListAdapter mAdapter;

    private Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_contact);

        realm = getRealm();
        ArrayList<Contacts> contactsArrayList = (ArrayList<Contacts>) getIntent().getSerializableExtra("contactList");
        init();

        if (contactsArrayList.size() == 0) {
            contactsArrayList.addAll(realm.copyFromRealm(realm.where(Contacts.class).equalTo("is_app_contact", "1")
                    .notEqualTo("contact_number", Utils.getFromUserDefaults(SelectContactActivity.this, Constants.CONTACT_NUMBER)).findAll()));
        }

        if (contactsArrayList.size() > 0) {
            lvContacts.setVisibility(View.VISIBLE);
            linearLayoutNoContact.setVisibility(View.GONE);
            ivDone.setVisibility(View.VISIBLE);
            mAdapter = new ContactListAdapter(SelectContactActivity.this, contactsArrayList);
            lvContacts.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        } else {
            lvContacts.setVisibility(View.GONE);
            linearLayoutNoContact.setVisibility(View.VISIBLE);
            ivDone.setVisibility(View.GONE);
        }


        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                ivClose.startAnimation(animation);


                SelectContactActivity.this.finish();
            }
        });

        ivDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                ivDone.startAnimation(animation);


                Intent intent = new Intent();
                intent.putExtra("selectedContactList", mAdapter.getSelectedConactList());
                setResult(RESULT_OK, intent);
                SelectContactActivity.this.finish();

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null)
            realm.close();
    }

    private void init() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivClose = (ImageView) findViewById(R.id.iv_close);
        ivDone = (ImageView) findViewById(R.id.iv_done);

        lvContacts = (ListView) findViewById(R.id.lv_contact);

        linearLayoutNoContact = (LinearLayout) findViewById(R.id.linear_no_contats);


    }


}
