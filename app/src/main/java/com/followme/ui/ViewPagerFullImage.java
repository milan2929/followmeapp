package com.followme.ui;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.followme.R;
import com.followme.adapter.CustomPagerAdapter;

public class ViewPagerFullImage extends BaseActivity {

    private String TAG = ViewPagerFullImage.class.getName();

    private ViewPager tagImages;
    private ImageView imgClose;

    CustomPagerAdapter customPagerAdapter;

    String imageUrl;
    String message;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager_full_image);

        imageUrl = getIntent().getStringExtra("imageUrl");
        message = getIntent().getStringExtra("message");

        init();

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                imgClose.startAnimation(animation);

                setResult(RESULT_OK);
                finish();
            }
        });
    }


    private void init() {
        tagImages = (ViewPager) findViewById(R.id.pagerTagImages);
        imgClose = (ImageView) findViewById(R.id.img_close);

        customPagerAdapter = new CustomPagerAdapter(ViewPagerFullImage.this,imageUrl,message);
        tagImages.setAdapter(customPagerAdapter);
    }


}
