package com.followme.ui;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.followme.R;
import com.followme.helper.Security;
import com.followme.requestmodel.SendConfirmationCodeRequestModel;
import com.followme.responsemodel.RefreshTokenResponseModel;
import com.followme.responsemodel.SendConfirmationCodeResponseModel;
import com.followme.utils.Constants;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.realm.Realm;

public class SignUpActivity extends BaseActivity {

    private String TAG = SignUpActivity.class.getName();

    private ImageView ivFlag;
    private EditText etCountry;
    private ImageView ivDropDown;
    private TextView tvCountryCode;
    private EditText etNumber;
    private String number = "";
    private ImageView ivSignUp;
    //    private LinearLayout llCountry;
    private String countryIso = "";
    private String unformattedPhoneNumber;

    private String formattedPhoneNumber;
    private boolean isInAfterTextChanged;
    private AsYouTypeFormatter aytf;


    private CountryPicker picker;
    //    private int textlength = 0;
    final static int MY_REQUEST_CODE = 3333;
    private ProgressWheel progressWheel;
    private PhoneNumberUtil phoneUtil;
    private int textlength;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Realm.init(this);
        setContentView(R.layout.activity_main);

        phoneUtil = PhoneNumberUtil.getInstance();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int CAMERA = checkSelfPermission(Manifest.permission.CAMERA);
            int READ_PHONE_STATE = checkSelfPermission(Manifest.permission.READ_PHONE_STATE);
            int ACCESS_COARSE_LOCATION = checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
            int ACCESS_FINE_LOCATION = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            int READ_EXTERNAL_STORAGE = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
            int WRITE_CONTACTS = checkSelfPermission(Manifest.permission.WRITE_CONTACTS);
            int READ_CONTACTS = checkSelfPermission(Manifest.permission.READ_CONTACTS);

            List<String> listPermissionsNeeded = new ArrayList<>();

            if (CAMERA != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.CAMERA);
            }

            if (READ_PHONE_STATE != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            }

            if (ACCESS_COARSE_LOCATION != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }

            if (ACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }

            if (READ_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            if (WRITE_CONTACTS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.WRITE_CONTACTS);
            }

            if (READ_CONTACTS != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
            }

            if (!listPermissionsNeeded.isEmpty()) {
                Log.i("Permission size", listPermissionsNeeded.size() + " ");
                requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_REQUEST_CODE);
            } else {

            }

        }


//        Utils.saveBooleanToUserDefaults(SignUpActivity.this, Constants.IS_SETTINGS_FIRST_TIME, true);

//        if (Utils.getBooleanFromUserDefaults(SignUpActivity.this, Constants.IS_NUMBER_VERIFIED) != null) {
        if (Utils.getBooleanFromUserDefaults(SignUpActivity.this, Constants.IS_NUMBER_VERIFIED)) {
            Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
            startActivity(intent);
            SignUpActivity.this.finish();
        }
//        }

//        if (Utils.isNetworkAvailable(SignUpActivity.this)) {
//            getToken();
//        }

        init();

        ivDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryPickerDialog();
            }
        });


        etCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCountryPickerDialog();
            }
        });

        ivSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                ivSignUp.startAnimation(animation);


                String country = etCountry.getText().toString().trim();
                String number = etNumber.getText().toString().trim();

                if (validate(country, number)) {
                    clearError();


//
                    if (Utils.isNetworkAvailable(SignUpActivity.this)) {
                        progressWheel.setVisibility(View.VISIBLE);
                        getToken();
                    } else {
                        Utils.showNoInternetAlertDialog(SignUpActivity.this);
                    }


//                    sendConfirmationCode();

//                    Intent intent = new Intent(SignUpActivity.this, VerificationActivity.class);
//                    startActivity(intent);
//                    SignUpActivity.this.finish();
                }
            }
        });

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (aytf != null) {
                    if (!isInAfterTextChanged) {
                        isInAfterTextChanged = true;

                        if (s.length() > 0) {
                            try {
                                unformattedPhoneNumber = s.toString().replaceAll("[^\\d.]", "");
                                for (int i = 0; i < unformattedPhoneNumber.length(); i++) {
                                    formattedPhoneNumber = aytf.inputDigit(unformattedPhoneNumber.charAt(i));
                                }
                                etNumber.setText(formattedPhoneNumber);
                                etNumber.setSelection(etNumber.getText().length());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            aytf.clear();
                        }

                        formattedPhoneNumber = null;
                        isInAfterTextChanged = false;
                    }
                } else {
                    textlength = etNumber.getText().length();
                    String text = s.toString();
                    if (text.endsWith(" "))
                        return;

                    if (textlength == 4 || textlength == 8) {
                        etNumber.setText(new StringBuilder(text).insert(text.length() - 1, " ").toString());
                        etNumber.setSelection(etNumber.getText().length());
                    }

                }


            }
        });


    }

    private void init() {
        ivFlag = (ImageView) findViewById(R.id.iv_flag);
        etCountry = (EditText) findViewById(R.id.et_country);
        ivDropDown = (ImageView) findViewById(R.id.iv_drop_down);
        tvCountryCode = (TextView) findViewById(R.id.tv_country_code);
        etNumber = (EditText) findViewById(R.id.et_number);
        ivSignUp = (ImageView) findViewById(R.id.iv_sign_up);
//        llCountry = (LinearLayout) findViewById(R.id.rel_country);
        progressWheel = (ProgressWheel) findViewById(R.id.progress);
    }

    private void showCountryPickerDialog() {
        picker = CountryPicker.newInstance("Select Country");
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String s, String s1, String s2, int i) {
                Log.d(TAG, "NAME-" + s);
                Log.d(TAG, "CODE-" + s1);
                Log.d(TAG, "DIAL CODE-" + s2);
                Log.d(TAG, "FLAG DRAWABLE RESOURSE ID-" + i);

                countryIso = s1;
                etCountry.setText(s);
                tvCountryCode.setText(s2);
                ivFlag.setImageResource(i);

                etNumber.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                aytf = phoneUtil.getAsYouTypeFormatter(countryIso);

                try {
                    unformattedPhoneNumber = etNumber.getText().toString().replaceAll("[^\\d.]", "");
                    etNumber.setText(unformattedPhoneNumber);
                 } catch (Exception e) {
                    e.printStackTrace();
                }

                picker.dismiss();
            }
        });
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }

    private boolean validate(String country, String number) {
        boolean result = true;

        if (TextUtils.isEmpty(country)) {
            result = false;
            etCountry.setError("Please select country");
            etCountry.setFocusableInTouchMode(true);
            etCountry.setFocusable(true);
            etCountry.requestFocus();
            etCountry.setSelection(country.length());

        } else if (TextUtils.isEmpty(number)) {
            result = false;
            etNumber.setError("Please provide mobile number");
            etNumber.setFocusableInTouchMode(true);
            etNumber.setFocusable(true);
            etNumber.requestFocus();
            etNumber.setSelection(number.length());
        }


        return result;
    }


    private void clearError() {
        etCountry.setError(null);
        etNumber.setError(null);
    }

    /*  For removing + sign from countryCode
     */
    private String removeFirstCharacter(String countryCode) {
        return countryCode.substring(1);
    }


    private void sendConfirmationCode() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");


        SendConfirmationCodeRequestModel sendConfirmationCodeRequestModel = new SendConfirmationCodeRequestModel();

        sendConfirmationCodeRequestModel.setAccessKey(Constants.ACCESS_KEY_VALUE);
        sendConfirmationCodeRequestModel.setSecretKey(Utils.getFromUserDefaults(SignUpActivity.this, Constants.SECRET_KEY));
        sendConfirmationCodeRequestModel.setDeviceToken(Utils.getDeviceToken(SignUpActivity.this));
        sendConfirmationCodeRequestModel.setDeviceType("2");
        sendConfirmationCodeRequestModel.setIsTestdata("1");
        sendConfirmationCodeRequestModel.setCountryCode(removeFirstCharacter(tvCountryCode.getText().toString().trim()));
//        sendConfirmationCodeRequestModel.setContactNumber(etNumber.getText().toString().trim().replaceAll("[^a-zA-Z0-9]", ""));
        try {
            Phonenumber.PhoneNumber swissNumberProto = phoneUtil.parse(etNumber.getText().toString(), countryIso);
            Log.e(TAG, "onClick: " + swissNumberProto.getNationalNumber());
            sendConfirmationCodeRequestModel.setContactNumber(String.valueOf(swissNumberProto.getNationalNumber()));
        } catch (NumberParseException e) {
            e.printStackTrace();
        }
//        sendConfirmationCodeRequestModel.setSmsMode("Other");
        sendConfirmationCodeRequestModel.setSmsMode("Nexmo");

        StringEntity entity = null;
        try {
            entity = new StringEntity(new JSONObject(getMapper().writeValueAsString(sendConfirmationCodeRequestModel)).toString(), "UTF-8");
            entity.setContentType("application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e(TAG, "SEND CONFIRMATION CODE URL-" + Urls.SEND_CONFIRMATION_CODE);
        client.post(SignUpActivity.this, Urls.SEND_CONFIRMATION_CODE, entity, "application/json", new GetSendConfirmationCodeResponseHandler());


    }

    private class GetSendConfirmationCodeResponseHandler extends AsyncHttpResponseHandler {


        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

//            progressWheel.setVisibility(View.GONE);
//            Log.e(TAG, "SEND CONFIRMATION CODE RESPONSE-" + new String(responseBody));
//            SendConfirmationCodeResponseModel sendConfirmationCodeResponseModel = new Gson().fromJson(new String(responseBody), SendConfirmationCodeResponseModel.class);
//            if (sendConfirmationCodeResponseModel.getStatus().equals("1")) {
//
//                Utils.saveToUserDefaults(SignUpActivity.this, Constants.USER_ID, sendConfirmationCodeResponseModel.getUser().get(0).getId());
//
//                Intent intent = new Intent(SignUpActivity.this, VerificationActivity.class);
//                intent.putExtra("data", sendConfirmationCodeResponseModel);
//                startActivity(intent);
//                SignUpActivity.this.finish();
//
//            }


            progressWheel.setVisibility(View.GONE);

            if (responseBody != null) {


                Log.e(TAG, "SEND CONFIRMATION CODE RESPONSE-" + new String(responseBody));
                SendConfirmationCodeResponseModel sendConfirmationCodeResponseModel = null;
                try {
                    sendConfirmationCodeResponseModel = getMapper().readValue(responseBody, SendConfirmationCodeResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sendConfirmationCodeResponseModel != null && sendConfirmationCodeResponseModel.getStatus().equals("1")) {

                    Utils.saveToUserDefaults(SignUpActivity.this, Constants.USER_ID, sendConfirmationCodeResponseModel.getUser().get(0).getId());
                    Utils.saveToUserDefaults(SignUpActivity.this, Constants.CONTACT_NUMBER, sendConfirmationCodeResponseModel.getUser().get(0).getContact_number());

                    String SECRET_KEY_VALUE = sendConfirmationCodeResponseModel.getUserToken();
                    String ACCESS_KEY_VALUE = Security.encrypt(sendConfirmationCodeResponseModel.getUser().get(0).getGuid(), Utils.getFromUserDefaults(SignUpActivity.this, Constants.GLOBAL_PASSWORD));

                    Utils.saveToUserDefaults(SignUpActivity.this, Constants.SECRET_KEY, SECRET_KEY_VALUE);
                    Utils.saveToUserDefaults(SignUpActivity.this, Constants.ACCESS_KEY, ACCESS_KEY_VALUE);

                    Intent intent = new Intent(SignUpActivity.this, VerificationActivity.class);
                    intent.putExtra("data", sendConfirmationCodeResponseModel);
                    startActivity(intent);
                    SignUpActivity.this.finish();

                }
            }

        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            progressWheel.setVisibility(View.GONE);

            Log.e(TAG, "STATUS CODE-" + statusCode);
//            Log.e(TAG, "FAILURE RESPONSE-" + new String(responseBody));
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();

        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case MY_REQUEST_CODE:
                if (permissions.length == grantResults.length) {
                    //new ReadContactsApi(SignUpActivity.this);
                }

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE)
                                != PackageManager.PERMISSION_GRANTED) {


//                            requestPermissions(new String[]{
//                                    Manifest.permission.READ_PHONE_STATE}, MY_REQUEST_CODE);


                            requestPermissions(new String[]{
                                    Manifest.permission.READ_PHONE_STATE,
                                    Manifest.permission.CAMERA,
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_CONTACTS,
                                    Manifest.permission.READ_CONTACTS}, MY_REQUEST_CODE);


                        }
                    }
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void getToken() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");


        try {
//            RefreshTokenRequestModel refreshTokenRequestModel = new RefreshTokenRequestModel();
//            refreshTokenRequestModel.setAccessKey(Constants.ACCESS_KEY_VALUE);

            JSONObject object = new JSONObject();
            object.put("access_key", "nousername");
            StringEntity entity = new StringEntity(object.toString());
            client.post(SignUpActivity.this, Urls.REFRESH_TOKEN, entity, "application/json", new GetRefreshTokenResponseHandler());

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e(TAG, "REFRESH TOKEN URL-" + Urls.REFRESH_TOKEN);

    }

    private class GetRefreshTokenResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            progressWheel.setVisibility(View.GONE);

            if (responseBody != null) {

                Log.d(TAG, "STATUS CODE-" + statusCode);

                RefreshTokenResponseModel refreshTokenResponseModel = null;
                try {
                    refreshTokenResponseModel = new ObjectMapper().readValue(new String(responseBody), RefreshTokenResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (refreshTokenResponseModel != null && refreshTokenResponseModel.getStatus().equals("1")) {
                    Utils.saveToUserDefaults(SignUpActivity.this, Constants.SECRET_KEY, refreshTokenResponseModel.getTempToken());
                    Utils.saveToUserDefaults(SignUpActivity.this, Constants.GLOBAL_PASSWORD, refreshTokenResponseModel.getAdminConfig().get(0).getGlobalPassword());
                    progressWheel.setVisibility(View.VISIBLE);
                    sendConfirmationCode();
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
//            Log.e(TAG, "FAILURE RESPONSE-" + new String(responseBody));
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }

    }


}
