package com.followme.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.followme.utils.Constants;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import io.realm.Realm;

/**
 * Created by pc on 04/05/17.
 */

public class BaseActivity extends AppCompatActivity {

    private final static Lock lock = new ReentrantLock();
    private static ObjectMapper mapper = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Realm getRealm(){
        return Realm.getInstance(Constants.config);
    }


    @Nullable
    static public synchronized ObjectMapper getMapper() {
        if (mapper != null)
            return mapper;

        try {
            lock.lock();
            mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z", Locale.US);
            mapper.setDateFormat(sourceFormat);
            lock.unlock();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mapper;
    }

}
