package com.followme.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.FollowMeApplicationClass;
import com.followme.R;
import com.followme.model.MyNewTag;
import com.followme.realm.Contacts;
import com.followme.requestmodel.AddTagsRequestModel;
import com.followme.requestmodel.GetContactsDetailRequestModel;
import com.followme.responsemodel.AddTagResponseModel;
import com.followme.responsemodel.UpdateUserProfileResponseModel;
import com.followme.services.GPSTracker;
import com.followme.utils.Constants;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.followme.ws.FileUploadInput;
import com.followme.ws.WebserviceWrapper;
import com.followme.ws.WsResponseListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.realm.Realm;

//import com.followmeandroidapp.model.Person;

public class NewTagActivity extends BaseActivity implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<MyNewTag>, ClusterManager.OnClusterInfoWindowClickListener<MyNewTag>, ClusterManager.OnClusterItemClickListener<MyNewTag>, ClusterManager.OnClusterItemInfoWindowClickListener<MyNewTag>, ImageChooserListener {

    private String TAG = NewTagActivity.class.getName();
    private Bitmap bitmap = null;

    private Toolbar toolbar;
    private ImageView ivBack;
    private ImageView ivLoaction;
    private TextView tvNewTag;
    private TextView tvCamera;
    private TextView tvGallery;
    private ImageView ivAddPeople;
    private ImageView ivSticker;
    private EditText etMessage;
    private ImageView ivSend;

    private ProgressWheel progressWheel;

    private RelativeLayout relSelectedContact;
    private TextView tvSelectedContact;

//    private DBHalperClass dbHalperClass;

    private GoogleMap mMap;
    //    private ClusterManager<Person> mClusterManager;
    private ClusterManager<MyNewTag> mClusterManager;
    private Random mRandom = new Random(1984);

    private int REQ_SELECT_CONTACT = 2222;
    private boolean canSend = false;

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    File file;

    //    private ArrayList<Contacts> listContact = new ArrayList<>();
    ArrayList<Contacts> tempList = new ArrayList<>();

    private ImageChooserManager imageChooserManager;
    private String filePath;
    private int chooserType;

    //    private int appContactCounter = 0;
    final static int MY_REQUEST_CODE = 3333;

    private String friendIds = "";
    private Realm realm;


    // GPSTracker class
    GPSTracker gps;

    private double latitude;
    private double longitude;

    boolean isTimerCancelled = false;
    Timer timer = new Timer();

//    private String base64Image;
//    String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_tag);
        realm = getRealm();
//        bitmap = (Bitmap) getIntent().getParcelableExtra("bitmapImage");
//        startGPSTrackerService();

        filePath = getIntent().getStringExtra("bitmapImagePath");
        Log.e(TAG, "IMAGE-" + filePath);
        if (filePath != null) {
//            Bitmap bitmap = createBitmap(imagePath);
            bitmap = createBitmap(filePath);
//            base64Image = encodeImage(bitmap);
            freeMemory();
        } else {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.ic_image_black_48dp);
        }


        init();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                ivBack.startAnimation(animation);

                onBackPressed();
            }
        });

        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                tvCamera.startAnimation(animation);


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE);

                    } else {
                        Intent intent = new Intent(NewTagActivity.this, CameraActivity.class);
                        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    }

                } else {
                    Intent intent = new Intent(NewTagActivity.this, CameraActivity.class);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                }


            }
        });

        tvGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                tvGallery.startAnimation(animation);


                chooseImage();
            }
        });

        ivAddPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tempList != null) {
//                    if (tempList.size() > 0) {
                    Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                    ivAddPeople.startAnimation(animation);

                    Intent intent = new Intent(NewTagActivity.this, SelectContactActivity.class);
                    intent.putExtra("contactList", tempList);
                    startActivityForResult(intent, REQ_SELECT_CONTACT);
//                    }
//                    else {
//                        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
//                        ivAddPeople.startAnimation(animation);
//
//                        Intent intent = new Intent(NewTagActivity.this, SelectContactActivity.class);
//                        intent.putExtra("contactList", tempList);
//                        startActivityForResult(intent, REQ_SELECT_CONTACT);
//                    }
                }


            }
        });

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (canSend) {

                    Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                    ivSend.startAnimation(animation);


                    if (filePath != null) {
                        if (!filePath.equals("")) {
                            progressWheel.setVisibility(View.VISIBLE);
                            addTag();
                        } else {
                            // Show Alert

                            showAlertDialog(NewTagActivity.this, "Please select image.");
                        }

                    } else {
                        //Show Alert
                        showAlertDialog(NewTagActivity.this, "Please select image.");
                    }

                }
            }
        });


        if (realm.where(Contacts.class).count() == 0) {
            getContactsDetail();
        }

    }

    private void init() {


        // create class object
//        gps = new GPSTracker(NewTagActivity.this);

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivLoaction = (ImageView) findViewById(R.id.iv_location);
        tvNewTag = (TextView) findViewById(R.id.tv_new_tag);
        tvCamera = (TextView) findViewById(R.id.tv_camera);
        tvGallery = (TextView) findViewById(R.id.tv_gallery);
        ivAddPeople = (ImageView) findViewById(R.id.iv_add_people);
        ivSticker = (ImageView) findViewById(R.id.iv_stickers);
        etMessage = (EditText) findViewById(R.id.et_message);
        ivSend = (ImageView) findViewById(R.id.iv_send);

        progressWheel = (ProgressWheel) findViewById(R.id.progress);

        relSelectedContact = (RelativeLayout) findViewById(R.id.rel_selected_contact);
        tvSelectedContact = (TextView) findViewById(R.id.tv_selected_contact);
        relSelectedContact.setVisibility(View.GONE);

//        dbHalperClass = new DBHalperClass(NewTagActivity.this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mMap != null) {
            return;
        }
        mMap = googleMap;
//        startDemo();
        startGPSTrackerService();

    }


    protected GoogleMap getMap() {
        return mMap;
    }

    protected void startDemo() {
//        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.503186, -0.126446), 9.5f));
        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 9.5f));


        mClusterManager = new ClusterManager<MyNewTag>(this, getMap());

        mClusterManager.setRenderer(new PersonRenderer());
        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);
        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        addItems();
        mClusterManager.cluster();
    }

    private void addItems() {
        // http://www.flickr.com/photos/sdasmarchives/5036248203/
        mClusterManager.addItem(new MyNewTag(position(), bitmap));


    }

    private LatLng position() {
        return new LatLng(latitude, longitude);
    }

//    private double random(double min, double max) {
//        return mRandom.nextDouble() * (max - min) + min;
//    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (chosenImage != null) {

                    Log.e("onImageChosen",
                            "Camera --" + chosenImage.getFilePathOriginal());

                    file = new File(chosenImage.getFilePathOriginal());

                    if (file.exists()) {
//

                        Bitmap myBitmap = FollowMeApplicationClass.getBitmapFromPath(file.getAbsolutePath());
//
//                        ivPreviewSmall.setImageBitmap(myBitmap);
                        sendBitmapToSavePage(myBitmap, chosenImage.getFilePathOriginal());
                    }

                }
            }
        });
    }

    @Override
    public void onError(String s) {

    }

    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }

    private class PersonRenderer extends DefaultClusterRenderer<MyNewTag> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        public PersonRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            mImageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

        @Override
        protected void onBeforeClusterItemRendered(MyNewTag person, MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.
//            mImageView.setImageResource(person.profilePhoto);
            mImageView.setImageBitmap(person.profilePhoto);
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MyNewTag> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
//            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
//            int width = mDimension;
//            int height = mDimension;

//            for (Person p : cluster.getItems()) {
//                // Draw 4 at most.
//                if (profilePhotos.size() == 4) break;
//                Drawable drawable = getResources().getDrawable(p.profilePhoto);
//                drawable.setBounds(0, 0, width, height);
//                profilePhotos.add(drawable);
//            }
//            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
//            multiDrawable.setBounds(0, 0, width, height);
//
//            mClusterImageView.setImageDrawable(multiDrawable);
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<MyNewTag> cluster) {


        // Show a toast with some info when the cluster is clicked.
//        String firstName = cluster.getItems().iterator().next().name;
//        Toast.makeText(this, cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
//        LatLngBounds.Builder builder = LatLngBounds.builder();
//        for (ClusterItem item : cluster.getItems()) {
//            builder.include(item.getPosition());
//        }
//        // Get the LatLngBounds
//        final LatLngBounds bounds = builder.build();
//
//        // Animate camera to the bounds
//        try {
//            getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MyNewTag> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(MyNewTag item) {
        // Does nothing, but you could go into the user's profile page, for example.

//        showImagePopup(NewTagActivity.this, item.profilePhoto);

        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(MyNewTag item) {
        // Does nothing, but you could go into the user's profile page, for example.

//        showImagePopup(NewTagActivity.this, item.profilePhoto);
    }

    ArrayList<Contacts> sampleContactModelClassArrayList = new ArrayList<>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQ_SELECT_CONTACT) {

//                ArrayList<Contacts> sampleContactModelClassArrayList = new ArrayList<>();
                ArrayList<String> selectedContact = new ArrayList<>();
                ArrayList<String> selectedFriendID = new ArrayList<>();


                if (data != null) {
                    sampleContactModelClassArrayList.clear();
                    sampleContactModelClassArrayList = (ArrayList<Contacts>) data.getSerializableExtra("selectedContactList");
                }

                Log.e(TAG, "SELECTED CONTACT SIZE-" + sampleContactModelClassArrayList.size());

                for (int i = 0; i < sampleContactModelClassArrayList.size(); i++) {

                    if (sampleContactModelClassArrayList.get(i).isSelected()) {
                        Log.e(TAG, "SELECTED CONTACT NAME-" + sampleContactModelClassArrayList.get(i).getName());
                        selectedContact.add(sampleContactModelClassArrayList.get(i).getName());
                        selectedFriendID.add(sampleContactModelClassArrayList.get(i).getUser_id());
//                        tempList.get(i).setSelected(true);

                    }
                }

                friendIds = convertListToCommaSepratedString(selectedFriendID);
                Log.e(TAG, "SELECTED FRIEND IDS-" + friendIds);

                String strSelectedContact = convertListToCommaSepratedString(selectedContact);
                Log.e(TAG, "SELECTED CONTACT-" + strSelectedContact);


                tempList.clear();
                tempList.addAll(sampleContactModelClassArrayList);


                relSelectedContact.setVisibility(View.VISIBLE);
                tvSelectedContact.setText(strSelectedContact);
                ivSend.setImageResource(R.drawable.ic_send_blue);
                canSend = true;
            } else if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (data != null) {
//                    Bitmap bitmap = data.getParcelableExtra("bitmapImage");
//                    ivProfileImage.setImageBitmap(bitmap);

                    filePath = data.getStringExtra("bitmapImagePath");
                    bitmap = createBitmap(filePath);
//                    base64Image = encodeImage(bitmap);
                    freeMemory();

                    startDemo();

//                    progressWheel.setVisibility(View.VISIBLE);
//                    updateUserProfile();

                }
            } else if (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE) {
//                if (imageChooserManager == null) {
//                    imageChooserManager = new ImageChooserManager(this,
//                            ChooserType.REQUEST_PICK_PICTURE, "myfolder", true);
//                }

                if (imageChooserManager == null) {
                    imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE);
                }
                imageChooserManager.submit(requestCode, data);
            }
        }
    }

    private void showSentDialog(Context context) {
        try {
            final Dialog alertDialogs = new Dialog(context);
            alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialogs.setContentView(R.layout.layout_sent);

            alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            alertDialogs.setCancelable(true);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    finish();
                    alertDialogs.dismiss();

                }
            }, 2000);


            alertDialogs.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    private void showImagePopup(Context context, int imageResource, String tagText) {
    private void showImagePopup(Context context, Bitmap imageResource) {

        final Dialog alertDialogs = new Dialog(context);
        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogs.setContentView(R.layout.layout_poopup);


        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView imageView = (ImageView) alertDialogs.findViewById(R.id.iv_img);
        TextView tvTag = (TextView) alertDialogs.findViewById(R.id.tv_tag);

//        tvTag.setText(tagText);
//        imageView.setImageResource(imageResource);
        imageView.setImageBitmap(imageResource);


        alertDialogs.setCancelable(true);


        alertDialogs.show();
    }


    private String convertListToCommaSepratedString(ArrayList<String> list) {
        String str = "";
        StringBuilder commaSepValueBuilder = new StringBuilder();

        //Looping through the list
        for (int i = 0; i < list.size(); i++) {
            //append the value into the builder
            commaSepValueBuilder.append(list.get(i));

            //if the value is not the last element of the list
            //then append the comma(,) as well
            if (i != list.size() - 1) {
                commaSepValueBuilder.append(",");
            }
        }
        Log.e(TAG, commaSepValueBuilder.toString());
        str = commaSepValueBuilder.toString();
        return str;
    }


//    private Bitmap createBitmap(String imagePath) {
//
//        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
//        return bitmap;
//    }
//
//    private String encodeImage(Bitmap bm) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] b = baos.toByteArray();
//        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
//
//        return encImage;
//    }


    private void chooseImage() {

        Log.e("Choose Image", "Camera");

        chooserType = ChooserType.REQUEST_PICK_PICTURE;
//        imageChooserManager = new ImageChooserManager(this,
//                ChooserType.REQUEST_PICK_PICTURE, "myfolder", true);
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE);
        imageChooserManager.setImageChooserListener(this);
        try {
//            pd = ProgressDialog.show(CameraActivity.this, "",
//                    "Capturing image...");
            filePath = imageChooserManager.choose();

            Log.e("Choose Image", "Camera --" + filePath);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void sendBitmapToSavePage(final Bitmap bm, final String path) {

        final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.activity_image_save);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        ImageButton tvCancel = (ImageButton) dialog.findViewById(R.id.tvCancel);
        ImageView ivCapturedImage = (ImageView) dialog.findViewById(R.id.ivCapturedImage);

//        ImageView ivUpload = (ImageView) dialog.findViewById(R.id.ivUpload);

//        FrameLayout flSaveImage = (FrameLayout) dialog.findViewById(R.id.fl_save);
        final ImageView ivSave = (ImageView) dialog.findViewById(R.id.shutterButton);


        ivCapturedImage.setImageBitmap(bm);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(CameraActivity.this, "====>Save Clicked", Toast.LENGTH_LONG).show();
//                Log.e(TAG, "Save Image");
//                ivSave.setBackgroundColor(ContextCompat.getColor(NewTagActivity.this, R.color.colorPrimary));
//                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_bounce);
//                ivSave.startAnimation(animation);
//                animation.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//
//
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//
//                    }
//                });


                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(path);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);
                    bm.compress(Bitmap.CompressFormat.JPEG, 95, bos);
                    bos.flush();
                    fos.getFD().sync();
                    bos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                File originalFile = new File(path);
                Log.e(TAG, "Original File Path===>" + originalFile.getPath());
                Log.e(TAG, "Original File Name===>" + originalFile.getName());


//                Intent intent = new Intent();
//                Bitmap resized = ThumbnailUtils.extractThumbnail(bm, 350, 350);
//                intent.putExtra("bitmapImage", resized);

//                intent.putExtra("bitmapImage", encodeImage(bm));
//                intent.putExtra("bitmapImagePath", originalFile.getPath());
//
                filePath = originalFile.getPath();
                bitmap = createBitmap(originalFile.getPath());
//                base64Image = encodeImage(bitmap);
                freeMemory();
                startDemo();

//                setResult(RESULT_OK, intent);
                dialog.dismiss();

            }
        });


        dialog.show();
    }


//    private class ReadContacts extends AsyncTask {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            progressWheel.setVisibility(View.VISIBLE);
//        }
//
//        @Override
//        protected Object doInBackground(Object[] params) {
//
//            getContacts();
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(Object o) {
//            super.onPostExecute(o);
////            progressWheel.setVisibility(View.GONE);
//
//            if (listContact != null) {
//                if (listContact.size() > 0) {
//                    getContactsDetail();
//                }
//            }
//
//        }
//    }


    private void getContacts() {

//        listContact.clear();

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (Integer.parseInt(cur.getString(
                        cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            new String[]{id}, null);

                    ArrayList<String> tempNumberList = new ArrayList<>();
                    tempNumberList.clear();
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
////                        Toast.makeText(SelectContactActivity.this, "Name: " + name + ", Phone No: " + phoneNo.replaceAll("[^a-zA-Z0-9]", ""), Toast.LENGTH_SHORT).show();
//                        Log.e(TAG, "Name: " + name + ", Phone No: " + phoneNo.replaceAll("[^a-zA-Z0-9]", ""));
//                        Log.d(TAG, "CODE-" + getCountryCode(phoneNo.replaceAll("[^a-zA-Z0-9]", "")));
//                        Log.d(TAG, "NUMBER-" + getPhoneNumber(phoneNo.replaceAll("[^a-zA-Z0-9]", "")));
////                        Log.d(TAG, "Name: " + name + ", Phone No: " + phoneNo.replaceAll("[\\s()]+", ""));


                        if (isValidPhoneNumber(phoneNo.replaceAll("[^a-zA-Z0-9]", ""))) {


                            Log.e(TAG, "Name: " + name + ", Phone No: " + phoneNo.replaceAll("[^a-zA-Z0-9]", ""));
                            Log.d(TAG, "CODE-" + getCountryCode(phoneNo.replaceAll("[^a-zA-Z0-9]", "")));
                            Log.d(TAG, "NUMBER-" + getPhoneNumber(phoneNo.replaceAll("[^a-zA-Z0-9]", "")));


                            Contacts contacts = new Contacts();
                            contacts.setContact_number(getPhoneNumber(phoneNo.replaceAll("[^a-zA-Z0-9]", "")));
                            contacts.setCountry_coder(getCountryCode(phoneNo.replaceAll("[^a-zA-Z0-9]", "")));
                            contacts.setName(name);
                            contacts.setImage("");
                            contacts.setSelected(false);
                            contacts.setUser_id("");
                            contacts.setIs_app_contact("");

                            if (!Utils.getFromUserDefaults(NewTagActivity.this, Constants.CONTACT_NUMBER).equals(contacts.getContact_number())) {
                                if (!tempNumberList.contains(contacts.getContact_number())) {
//                                    listContact.add(contacts);
                                    tempNumberList.add(contacts.getContact_number());
                                }
                            }


                        }
                    }
                    pCur.close();
                }
            }
        }
    }


    private void getContactsDetail() {
        GetContactsDetailRequestModel getContactsDetailRequestModel = new GetContactsDetailRequestModel();
        getContactsDetailRequestModel.setAccess_key(Utils.getFromUserDefaults(NewTagActivity.this, Constants.ACCESS_KEY));
        getContactsDetailRequestModel.setSecret_key(Utils.getFromUserDefaults(NewTagActivity.this, Constants.SECRET_KEY));
        getContactsDetailRequestModel.setUser_id(Integer.parseInt(Utils.getFromUserDefaults(NewTagActivity.this, Constants.USER_ID)));
        getContactsDetailRequestModel.getContacts().addAll(realm.where(Contacts.class).findAll());

        try {
            JSONObject object = new JSONObject(getMapper().writeValueAsString(getContactsDetailRequestModel));
            WebserviceWrapper webserviceWrapper = new WebserviceWrapper(this, object, new WsResponseListener() {
                @Override
                public void onDelieverResponse(String serviceType, Exception err, Object data) {

                    try {
                        if (data != null) {
                            JSONObject object1 = new JSONObject(data.toString());
                            if (object1.getInt("status") == 1) {

                                final JSONArray array = object1.getJSONArray("Contacts");
                                if (array.length() > 0) {
                                    realm.executeTransactionAsync(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.createOrUpdateAllFromJson(Contacts.class, array);
                                        }
                                    }, new Realm.Transaction.OnSuccess() {
                                        @Override
                                        public void onSuccess() {
                                            tempList.addAll(realm.copyFromRealm(realm.where(Contacts.class).equalTo("is_app_contact", "1")
                                                    .notEqualTo("contact_number", Utils.getFromUserDefaults(NewTagActivity.this, Constants.CONTACT_NUMBER)).findAll()));
                                        }
                                    });

                                }

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            });
            webserviceWrapper.execute(Urls.GET_CONTACTS_DETAIL);
        } catch (JSONException | JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private String getPhoneNumber(String number) {

        if (number.length() == 10) {
            return number;
        } else if (number.length() > 10) {
            return number.substring(number.length() - 10);
        } else {
            // whatever is appropriate in this case
//            throw new IllegalArgumentException("word has less than 3 characters!");
            return "";

        }
    }

    private boolean isValidPhoneNumber(String number) {


        if (number.length() == 10) {
            return true;
        } else if (number.length() > 10) {
            return true;
        } else {
            // whatever is appropriate in this case
            return false;
        }

    }

    private String getCountryCode(String number) {

        if (number != null) {
            if (!number.equals("")) {
                if (number.length() > 10) {
                    return number.substring(0, number.length() - 10);
                }
            }
        }

        return "";
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    private void checkForLatLong(double latitude, double longitude) {
        if (latitude != 0 && longitude != 0) {
            timer.cancel();
            isTimerCancelled = true;
            startDemo();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case MY_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(NewTagActivity.this, CameraActivity.class);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {


                            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE);

                        }

                    }

                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void addTag() {

        FileUploadInput fileUploadInput = new FileUploadInput();
        fileUploadInput.paramList.add(fileUploadInput.new Item("sender_id", Utils.getFromUserDefaults(NewTagActivity.this, Constants.USER_ID)));
        fileUploadInput.paramList.add(fileUploadInput.new Item("is_testdata", "1"));
        fileUploadInput.paramList.add(fileUploadInput.new Item("contact_number", Utils.getFromUserDefaults(NewTagActivity.this, Constants.CONTACT_NUMBER)));
        fileUploadInput.fileList.add(fileUploadInput.new Item("media_data", filePath));
        fileUploadInput.paramList.add(fileUploadInput.new Item("friend_ids", friendIds));
        fileUploadInput.paramList.add(fileUploadInput.new Item("latitude", String.valueOf(latitude)));
        fileUploadInput.paramList.add(fileUploadInput.new Item("longitude", String.valueOf(longitude)));
        fileUploadInput.paramList.add(fileUploadInput.new Item("message", etMessage.getText().toString()));

        WebserviceWrapper webserviceWrapper = new WebserviceWrapper(this, null, new WsResponseListener() {
            @Override
            public void onDelieverResponse(String serviceType, Exception err, Object data) {
                progressWheel.setVisibility(View.GONE);
                if (data != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(data.toString());
                        if (jsonObject.getString("status").equals("1")) {
                            Log.e(TAG, "ADD TAG RESPONSE-" + data.toString());

                            try {
                                final AddTagResponseModel addTagResponseModel = getMapper().readValue(data.toString(), AddTagResponseModel.class);
                                if (addTagResponseModel.getStatus().equals("1") && addTagResponseModel.getTag() != null) {

                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            realm.copyToRealmOrUpdate(addTagResponseModel.getTag());
                                        }
                                    });

                                    showSentDialog(NewTagActivity.this);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        webserviceWrapper.executeUploadMediaFile(Urls.ADD_TAG, fileUploadInput);
        progressWheel.setVisibility(View.VISIBLE);
    }


//    private void addTag() {
//
//
//        AsyncHttpClient client = new AsyncHttpClient();
//        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
//        client.addHeader("User-Agent", "Android");
//
//
//        AddTagsRequestModel addTagsRequestModel = new AddTagsRequestModel();
//
//        addTagsRequestModel.setSecret_key(Utils.getFromUserDefaults(NewTagActivity.this, Constants.SECRET_KEY));
//        addTagsRequestModel.setAccess_key(Utils.getFromUserDefaults(NewTagActivity.this, Constants.ACCESS_KEY));
//        addTagsRequestModel.setSender_id(Utils.getFromUserDefaults(NewTagActivity.this, Constants.USER_ID));
////        addTagsRequestModel.setFriend_ids(getFriendsId());
//        addTagsRequestModel.setFriend_ids(friendIds);
//        addTagsRequestModel.setLatitude(String.valueOf(latitude));
//        addTagsRequestModel.setLongitude(String.valueOf(longitude));
////        if(etMessage.getText().toString().equals("")){
////
////        }else {
////
////        }
//        addTagsRequestModel.setMessage(etMessage.getText().toString());
//        addTagsRequestModel.setIs_testdata("1");
//        addTagsRequestModel.setMedia_data(base64Image);
//
//
//        StringEntity entity = null;
//        try {
//            entity = new StringEntity(getMapper().writer().writeValueAsString(addTagsRequestModel));
//        } catch (UnsupportedEncodingException | JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        Log.e(TAG, "ADD TAG URL-" + Urls.ADD_TAG);
//        client.post(NewTagActivity.this, Urls.ADD_TAG, entity, "application/json", new AddTagResponseHandler());
//
//
//    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
        }
    }

//    private class AddTagResponseHandler extends AsyncHttpResponseHandler {
//
//        @Override
//        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//
//            progressWheel.setVisibility(View.GONE);
//            if (responseBody != null) {
////                Log.e(TAG, "ADD TAG RESPONSE-" + new String(responseBody));
//
//                try {
//                    final AddTagResponseModel addTagResponseModel = getMapper().readValue(responseBody, AddTagResponseModel.class);
//                    if (addTagResponseModel.getStatus().equals("1")) {
//
//                        realm.executeTransaction(new Realm.Transaction() {
//                            @Override
//                            public void execute(Realm realm) {
//                                realm.copyToRealmOrUpdate(addTagResponseModel.getTag());
//                            }
//                        });
//
//                        showSentDialog(NewTagActivity.this);
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//        }
//
//        @Override
//        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
//            progressWheel.setVisibility(View.GONE);
//            Log.e(TAG, "STATUS CODE-" + statusCode);
//            Log.e(TAG, "ERROR-" + error.getMessage());
//            error.printStackTrace();
//        }
//    }


    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }


    private Bitmap createBitmap(String imagePath) {

//        Bitmap bitmap = null;

        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }


        bitmap = BitmapFactory.decodeFile(imagePath);
        return bitmap;
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();

        return Base64.encodeToString(b, Base64.DEFAULT);
    }

    private void startGPSTrackerService() {


        try {
            gps = new GPSTracker(NewTagActivity.this);
            if (gps.canGetLocation()) {
                gps.getLocation();
                latitude = gps.getLatitude();
                longitude = gps.getLongitude();
                if (latitude == 0.0 || longitude == 0.0){
                    latitude = Double.parseDouble(Utils.getFromUserDefaults(this, Constants.LATITUDE));
                    longitude = Double.parseDouble(Utils.getFromUserDefaults(this, Constants.LONGITUDE));
                }
            } else {
                gps.showSettingsAlert();
                latitude = Double.parseDouble(Utils.getFromUserDefaults(this, Constants.LATITUDE));
                longitude = Double.parseDouble(Utils.getFromUserDefaults(this, Constants.LONGITUDE));
            }
            Log.e(TAG, "ON RESUME LATITUDE-" + latitude);
            Log.e(TAG, "ON RESUME LAOGITUDE-" + longitude);

            if (!isTimerCancelled) {

                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Log.e(TAG, "RUN ON RESUME LATITUDE-" + latitude);
                                Log.e(TAG, "RUN ON RESUME LAOGITUDE-" + longitude);

                                checkForLatLong(latitude, longitude);


                            }
                        });


                    }
                }, 0, 1000);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


//    private void saveTagOffline(AddTagResponseModel.Tag tag) {
////        ArrayList<MyTag> myTagsList=new ArrayList<>();
//        MyTag myTag = new MyTag();
//        Gson gson = new Gson();
//
//        String jsonString = Utils.getFromUserDefaults(NewTagActivity.this, Constants.MY_TAG);
//
//        if (jsonString == null) {
//            ArrayList<MyTag> myTagsList = new ArrayList<>();
//
//            MyTag.UserDetails userDetails = new MyTag.UserDetails();
//            userDetails.setUserId(Utils.getFromUserDefaults(NewTagActivity.this, Constants.USER_ID));
//            userDetails.setUserContactNumber(Utils.getFromUserDefaults(NewTagActivity.this, Constants.CONTACT_NUMBER));
//
//            ArrayList<MyTag.FriendDetails> friendDetailsArrayList = new ArrayList<>();
//            myTag.setUserDetails(userDetails);
//
//
//            for (int i = 0; i < tag.getFriend_detail().size(); i++) {
//                MyTag.FriendDetails friendDetails = new MyTag.FriendDetails();
//
//
//                friendDetails.setFriendsID(String.valueOf(tag.getFriend_detail().get(i).getUser_id()));
//                friendDetails.setFriendContactNumber(tag.getFriend_detail().get(i).getContact_number());
//                friendDetails.setFrindName("");
//
//                friendDetailsArrayList.add(friendDetails);
//
//            }
//
//            myTag.setFriendDetails(friendDetailsArrayList);
//
//            myTag.setLatitude(Double.parseDouble(tag.getLatitude()));
//            myTag.setLongitude(Double.parseDouble(tag.getLongitude()));
//            myTag.setmPosition(new LatLng(Double.parseDouble(tag.getLatitude()), Double.parseDouble(tag.getLongitude())));
//            myTag.setMessage(tag.getMessage());
//            myTag.setMedia(imagePath);
//            myTag.setImageUrl(tag.getMedia().getMedia_url());
//            myTag.setTagId(tag.getTag_id());
//
//            myTagsList.add(myTag);
//
//
//            String json = gson.toJson(myTagsList);
//            Log.d(TAG, "MY TAG-" + json);
//            Utils.saveToUserDefaults(NewTagActivity.this, Constants.MY_TAG, json);
//
//
//        } else {
//
//            if (!jsonString.equals("")) {
//
//                Log.e(TAG, "MY TAG FROM PREF-" + jsonString);
//                Type type = new TypeToken<ArrayList<MyTag>>() {
//                }.getType();
//                ArrayList<MyTag> arrayList = gson.fromJson(jsonString, type);
//
//                MyTag.UserDetails userDetails = new MyTag.UserDetails();
//                userDetails.setUserId(Utils.getFromUserDefaults(NewTagActivity.this, Constants.USER_ID));
//                userDetails.setUserContactNumber(Utils.getFromUserDefaults(NewTagActivity.this, Constants.CONTACT_NUMBER));
//
//                ArrayList<MyTag.FriendDetails> friendDetailsArrayList = new ArrayList<>();
//                myTag.setUserDetails(userDetails);
//
////                for (int i = 0; i < sampleContactModelClassArrayList.size(); i++) {
////                    MyTag.FriendDetails friendDetails = new MyTag.FriendDetails();
////
////                    if (sampleContactModelClassArrayList.get(i).isSelected()) {
////                        friendDetails.setFriendsID(sampleContactModelClassArrayList.get(i).getUserId());
////                        friendDetails.setFriendContactNumber(sampleContactModelClassArrayList.get(i).getContact_number());
////                        friendDetails.setFrindName(sampleContactModelClassArrayList.get(i).getName());
////
////                        friendDetailsArrayList.add(friendDetails);
////                    }
////
////
////                }
//
//                for (int i = 0; i < tag.getFriend_detail().size(); i++) {
//                    MyTag.FriendDetails friendDetails = new MyTag.FriendDetails();
//
//
//                    friendDetails.setFriendsID(String.valueOf(tag.getFriend_detail().get(i).getUser_id()));
//                    friendDetails.setFriendContactNumber(tag.getFriend_detail().get(i).getContact_number());
//                    friendDetails.setFrindName("");
//
//                    friendDetailsArrayList.add(friendDetails);
//
//
//                }
//
//                myTag.setFriendDetails(friendDetailsArrayList);
//
//                myTag.setLatitude(Double.parseDouble(tag.getLatitude()));
//                myTag.setLongitude(Double.parseDouble(tag.getLongitude()));
//                myTag.setmPosition(new LatLng(Double.parseDouble(tag.getLatitude()), Double.parseDouble(tag.getLongitude())));
//                myTag.setMessage(tag.getMessage());
//                myTag.setMedia(imagePath);
//                myTag.setImageUrl(tag.getMedia().getMedia_url());
//                myTag.setTagId(tag.getTag_id());
//
//
////        myTagsList.add(myTag);
//                arrayList.add(myTag);
//
//
////        String json = gson.toJson(myTagsList);
//                String json = gson.toJson(arrayList);
//                Log.d(TAG, "MY TAG-" + json);
//                Utils.saveToUserDefaults(NewTagActivity.this, Constants.MY_TAG, json);
//            } else {
//                ArrayList<MyTag> myTagsList = new ArrayList<>();
//
//                MyTag.UserDetails userDetails = new MyTag.UserDetails();
//                userDetails.setUserId(Utils.getFromUserDefaults(NewTagActivity.this, Constants.USER_ID));
//                userDetails.setUserContactNumber(Utils.getFromUserDefaults(NewTagActivity.this, Constants.CONTACT_NUMBER));
//
//                ArrayList<MyTag.FriendDetails> friendDetailsArrayList = new ArrayList<>();
//                myTag.setUserDetails(userDetails);
//
////                for (int i = 0; i < sampleContactModelClassArrayList.size(); i++) {
////                    MyTag.FriendDetails friendDetails = new MyTag.FriendDetails();
////
////                    if (sampleContactModelClassArrayList.get(i).isSelected()) {
////                        friendDetails.setFriendsID(sampleContactModelClassArrayList.get(i).getUserId());
////                        friendDetails.setFriendContactNumber(sampleContactModelClassArrayList.get(i).getContact_number());
////                        friendDetails.setFrindName(sampleContactModelClassArrayList.get(i).getName());
////
////                        friendDetailsArrayList.add(friendDetails);
////                    }
////
////                }
//
//
//                for (int i = 0; i < tag.getFriend_detail().size(); i++) {
//                    MyTag.FriendDetails friendDetails = new MyTag.FriendDetails();
//
//
//                    friendDetails.setFriendsID(String.valueOf(tag.getFriend_detail().get(i).getUser_id()));
//                    friendDetails.setFriendContactNumber(tag.getFriend_detail().get(i).getContact_number());
//                    friendDetails.setFrindName("");
//
//                    friendDetailsArrayList.add(friendDetails);
//
//
//                }
//
//
//                myTag.setFriendDetails(friendDetailsArrayList);
//
//                myTag.setLatitude(Double.parseDouble(tag.getLatitude()));
//                myTag.setLongitude(Double.parseDouble(tag.getLongitude()));
//                myTag.setmPosition(new LatLng(Double.parseDouble(tag.getLatitude()), Double.parseDouble(tag.getLongitude())));
//                myTag.setMessage(tag.getMessage());
//                myTag.setMedia(imagePath);
//                myTag.setImageUrl(tag.getMedia().getMedia_url());
//                myTag.setTagId(tag.getTag_id());
//
//
//                myTagsList.add(myTag);
////            arrayList.add(myTag);
//
//
//                String json = gson.toJson(myTagsList);
////            String json = gson.toJson(arrayList);
//                Log.d(TAG, "MY TAG-" + json);
//                Utils.saveToUserDefaults(NewTagActivity.this, Constants.MY_TAG, json);
//            }
//        }
//
//
//    }


    private void showAlertDialog(Context context, String message) {
        final Dialog alertDialogs = new Dialog(context);
        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogs.setContentView(R.layout.alert_no_connection);

        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView txtaltmsg = (TextView) alertDialogs.findViewById(R.id.txtaltmsg);
        TextView txtaltok = (TextView) alertDialogs.findViewById(R.id.tv_ok);


        txtaltmsg.setText(message);


        alertDialogs.setCancelable(false);

        txtaltok.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

//                etVerificationCode.setText(sendConfirmationCodeResponseModel.getUser().get(0).getVerification_code());
                alertDialogs.dismiss();

            }

        });

        alertDialogs.show();
    }


//    private void saveTagToLocalDatabase(Tags tag) {
//
//        MyTag myTag = new MyTag();
//        Gson gson = new Gson();
//
//
//        MyTag.UserDetails userDetails = new MyTag.UserDetails();
//        userDetails.setUserId(Utils.getFromUserDefaults(NewTagActivity.this, Constants.USER_ID));
//        userDetails.setUserContactNumber(Utils.getFromUserDefaults(NewTagActivity.this, Constants.CONTACT_NUMBER));
//
//        ArrayList<MyTag.FriendDetails> friendDetailsArrayList = new ArrayList<>();
//        myTag.setUserDetails(userDetails);
//
//
//        for (int i = 0; i < tag.getFriend_detail().size(); i++) {
//            MyTag.FriendDetails friendDetails = new MyTag.FriendDetails();
//
//
//            friendDetails.setFriendsID(String.valueOf(tag.getFriend_detail().get(i).getUser_id()));
//            friendDetails.setFriendContactNumber(tag.getFriend_detail().get(i).getContact_number());
//            friendDetails.setFrindName("");
//
//            friendDetailsArrayList.add(friendDetails);
//
//        }
//
//        myTag.setFriendDetails(friendDetailsArrayList);
//
//        myTag.setLatitude(Double.parseDouble(tag.getLatitude()));
//        myTag.setLongitude(Double.parseDouble(tag.getLongitude()));
//        myTag.setmPosition(new LatLng(Double.parseDouble(tag.getLatitude()), Double.parseDouble(tag.getLongitude())));
//        myTag.setMessage(tag.getMessage());
//        myTag.setMedia(imagePath);
//        myTag.setImageUrl(tag.getMedia().getMedia_url());
//        myTag.setTagId(tag.getTag_id());
//
//
//
//        String userDetail = gson.toJson(myTag.getUserDetails());
//        Log.e(TAG, "USER_DETAILS-" + userDetail);
//        String frindsDetails = gson.toJson(myTag.getFriendDetails());
//        Log.e(TAG, "FRIENDS DETAILS-" + frindsDetails);
//        Log.e(TAG, "LATITUDE-" + myTag.getLatitude());
//        Log.e(TAG, "LONGITUDE-" + myTag.getLongitude());
//        Log.e(TAG, "POSITION-" + myTag.getmPosition());
//        Log.e(TAG, "LOCAL IMAGE-" + myTag.getMedia());
//        Log.e(TAG, "SERVER IMAGE-" + Urls.BASE_TAG_IMAGE_URL + myTag.getImageUrl());
//        Log.e(TAG, "TAG ID-" + myTag.getTagId());
//        Log.e(TAG, "MESSAGE-" + myTag.getMessage());
//        Log.e(TAG, "IS FILE EXIST-" + myTag.isFileExist());
//
//        boolean isTagInserted = dbHalperClass.insertAddTag(userDetail, frindsDetails, String.valueOf(myTag.getLatitude()), String.valueOf(myTag.getLongitude()), myTag.getMessage(),
//                myTag.getMedia(), String.valueOf(myTag.getmPosition()), Urls.BASE_TAG_IMAGE_URL + myTag.getImageUrl(), myTag.getTagId(), 1);
//
//        if (isTagInserted) {
//            Log.e(TAG, "TAG INSERTED SUCCESSFULLY.");
//        } else {
//            Log.e(TAG, "ERROR IN INSERTING TAG.");
//        }
//
//    }

}
