package com.followme.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.R;
import com.followme.adapter.SlidingImage_Adapter;
import com.followme.helper.MultiImageDialogue;
import com.followme.realm.Contacts;
import com.followme.realm.Tags;
import com.followme.requestmodel.GetFriendTagListRequestModel;
import com.followme.utils.Constants;
import com.followme.utils.MultiDrawable;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.realm.Realm;

//import com.followmeandroidapp.model.Person;
//import com.followmeandroidapp.model.SampleTag;

public class TagDetailsActivity extends BaseActivity implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<Tags>, ClusterManager.OnClusterInfoWindowClickListener<Tags>, ClusterManager.OnClusterItemClickListener<Tags>, ClusterManager.OnClusterItemInfoWindowClickListener<Tags> {

    private String TAG = TagDetailsActivity.class.getName();

    private ImageView ivBack;
    //    private ImageView ivShow;
//    private ImageView ivDelete;
    private ProgressWheel progressWheel;

    private GoogleMap mMap;
    //    private ClusterManager<Person> mClusterManager;
    private ClusterManager<Tags> mClusterManager;
    private Random mRandom = new Random(1984);

    //    private String userName;
    private Tags tags = null;

    ArrayList<Tags> friendTagList = new ArrayList<>();
    private LruCache<String, Bitmap> mMemoryCache;

    Tags friendTag = null;

    private Realm realm;
    AdView adView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_details);

        realm = getRealm();
//        userName = getIntent().getStringExtra("username");
        tags = realm.where(Tags.class).equalTo("tag_id", getIntent().getIntExtra("data", 0)).findFirst();
        friendTagList.addAll(realm.copyFromRealm(realm.where(Tags.class)
                .equalTo("sender_id", tags.getSender_id()).isNotNull("media").findAll()));
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, @NonNull Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };

        init();
        getFriendTagList();

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.alpha);
                ivBack.startAnimation(animation);

                onBackPressed();
            }
        });

        if (friendTagList.size() > 0) {
            new getImages().execute();
        }
    }

    private class getImages extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            for (Tags tags : friendTagList) {
                try {
                    URL url = new URL(Urls.BASE_TAG_IMAGE_URL + tags.getMedia().getVideo_thumb_image());
                    addBitmapToMemoryCache(Urls.BASE_TAG_IMAGE_URL + tags.getMedia().getVideo_thumb_image(), BitmapFactory.decodeStream(url.openConnection().getInputStream()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (friendTagList.size() > 0 && getMap()!=null)
            startDemo();
        }
    }


    private void addBitmapToMemoryCache(@NonNull String key, @NonNull Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    private void removeBitmapFromMemCache(@NonNull String key) {
        mMemoryCache.remove(key);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
        }
    }

    private void init() {


        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);

        adView = (AdView) findViewById(R.id.adView);
        AdRequest.Builder build = new AdRequest.Builder();
        build.addTestDevice("45784DE82D84CC18577404F63C990355");
        adView.loadAd(build.build());


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ivBack = (ImageView) findViewById(R.id.iv_back);
        ImageView ivUserImage = (ImageView) findViewById(R.id.iv_user_image);
        TextView tvUserName = (TextView) findViewById(R.id.tv_user_name);

//        ivShow = (ImageView) findViewById(R.id.iv_show);
//        ivDelete = (ImageView) findViewById(R.id.iv_delete);
        progressWheel = (ProgressWheel) findViewById(R.id.progress);


        Contacts contacts = realm.where(Contacts.class).equalTo("contact_number", tags.getSender_detail().getContact_number()).findFirst();
        if (contacts != null) {
            tvUserName.setText(contacts.getName());
        } else {
            tvUserName.setText(tags.getSender_detail().getContact_number());
        }


        int image_size = (int) getResources().getDimension(R.dimen.iv_contact_image_size);
        Picasso.with(TagDetailsActivity.this)
                .load(Urls.BASE_IMAGE_URL + tags.getSender_detail().getProfile_picture())
                .resize(image_size, image_size)
                .centerCrop()
                .into(ivUserImage);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mMap != null) {
            return;
        }
        mMap = googleMap;

        if (friendTagList.size() > 0) {
            startDemo();
        }

    }


    protected GoogleMap getMap() {
        return mMap;
    }
//
//    @Override
//    public boolean onClusterClick(Cluster<Person> cluster) {
//        return false;
//    }

    private ImageView mClusterImageView;

    private class PersonRenderer extends DefaultClusterRenderer<Tags> {
        private final IconGenerator mIconGenerator = new IconGenerator(getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getApplicationContext());
        private final ImageView mImageView;
        private final int mDimension;
        private FrameLayout frame_marker;
        private List<Drawable> profilePhotos = new LinkedList<>();

        PersonRenderer() {
            super(getApplicationContext(), getMap(), mClusterManager);
            View multiProfile = getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);
            frame_marker = (FrameLayout) multiProfile.findViewById(R.id.frame_marker);
            mImageView = new ImageView(getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }


        @Override
        protected void onBeforeClusterItemRendered(final Tags person, final MarkerOptions markerOptions) {

            mImageView.setBackgroundResource(person.getIs_read() == 0 ? R.drawable.marker_border : 0);

//            MarkerTarget markerTarget =new MarkerTarget(markerOptions);
//            mImageView.setTag(markerTarget);
//            Glide.with(TagDetailsActivity.this)
//                    .load(Urls.BASE_TAG_IMAGE_URL + person.getMedia().getVideo_thumb_image())
//                    .asBitmap().override(50,50).centerCrop()
//                    .into(mImageView);


            Bitmap bitmap = getBitmapFromMemCache(Urls.BASE_TAG_IMAGE_URL + person.getMedia().getVideo_thumb_image());
            if (bitmap != null) {
                mImageView.setImageBitmap(bitmap);
            }
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");
//            markerTargets.add(markerTarget);
//            try {
//                 Picasso.with(TagDetailsActivity.this)
//                        .load(Urls.BASE_TAG_IMAGE_URL + person.getMedia().getVideo_thumb_image())
//                        .resize(mDimension, mDimension)
//                        .into(markerTarget);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

        }


//        class MarkerTarget extends SimpleTarget<Bitmap> {
//
//            MarkerOptions markerOptions;
//
//            MarkerTarget(MarkerOptions markerOptions) {
//                this.markerOptions = markerOptions;
//            }
//
//
//            @Override
//            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                mImageView.setImageBitmap(resource);
//                Bitmap icon = mIconGenerator.makeIcon();
//                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");
//            }
//        }


//        class MarkerTarget1 extends SimpleTarget<Bitmap> {
//
//            MarkerOptions markerOptions;
//
//            int size;
//            MarkerTarget1(MarkerOptions markerOptions,int size) {
//                this.markerOptions = markerOptions;
//                this.size = size;
//            }
//
//            @Override
//            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
//                mImageView.setImageBitmap(resource);
//                Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(size));
//                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
//            }
//        }

        @Override
        protected void onBeforeClusterRendered(Cluster<Tags> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;


            frame_marker.setBackgroundResource(0);
            for (Tags p : cluster.getItems()) {
                if (p.getIs_read() == 0) {
                    frame_marker.setBackgroundResource(R.drawable.alert_border);
                }
            }

            for (Tags p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;

                Bitmap bitmap = getBitmapFromMemCache(Urls.BASE_TAG_IMAGE_URL + p.getMedia().getVideo_thumb_image());
                if (bitmap != null) {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                    bitmapDrawable.setBounds(0, 0, width, height);
                    profilePhotos.add(bitmapDrawable);
                }


            }
            if (profilePhotos.size()>0) {
                MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
                multiDrawable.setBounds(0, 0,  width, height);

                mClusterImageView.setImageDrawable(multiDrawable);
            }
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    @Override
    public boolean onClusterClick(Cluster<Tags> cluster) {

//        ivDelete.setVisibility(View.VISIBLE);
//        ivShow.setVisibility(View.VISIBLE);

//        ivDelete.setVisibility(View.GONE);
//        ivShow.setVisibility(View.GONE);

        // Show a toast with some info when the cluster is clicked.
//        String firstName = cluster.getItems().iterator().next().getMessage();
//        Toast.makeText(this, cluster.getSize() + " (including " + firstName + ")", Toast.LENGTH_SHORT).show();

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.

//        ArrayList<String> images = new ArrayList<>();
//        ArrayList<String> tagMessage = new ArrayList<>();

        ArrayList<Tags> tagses = new ArrayList<>();
//        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            tagses.add((Tags) item);
//            builder.include(item.getPosition());
        }
//        // Get the LatLngBounds
//        final LatLngBounds bounds = builder.build();
        showMultipleImagePopup(TagDetailsActivity.this, tagses);

        // Animate camera to the bounds
//        try {
//            getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Tags> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(Tags item) {
        // Does nothing, but you could go into the user's profile page, for example.


//        ivDelete.setVisibility(View.VISIBLE);
//        ivShow.setVisibility(View.VISIBLE);

        friendTag = item;

//        showImagePopup(TagDetailsActivity.this, item.getMedia().getMedia_url(), item.getMessage());

        ArrayList<Tags> tagses = new ArrayList<>();
        tagses.add(item);

        showMultipleImagePopup(TagDetailsActivity.this, tagses);

        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(Tags item) {
        // Does nothing, but you could go into the user's profile page, for example.
        friendTag = item;

        ArrayList<Tags> tagses = new ArrayList<>();
        tagses.add(item);

        showMultipleImagePopup(TagDetailsActivity.this, tagses);
//        showImagePopup(TagDetailsActivity.this, item.getMedia().getMedia_url(), item.getMessage());
    }


    private void showMultipleImagePopup(Context context, final ArrayList<Tags> tagses) {


        MultiImageDialogue multiImageDialogue =new MultiImageDialogue(context , tagses, realm);
        multiImageDialogue.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                friendTagList.clear();
                friendTagList.addAll(realm.copyFromRealm(realm.where(Tags.class)
                        .equalTo("sender_id", tags.getSender_id()).isNotNull("media").findAll()));
                if (getMap() != null) startDemo();
            }
        });
        multiImageDialogue.show();

//        final Dialog alertDialogs = new Dialog(context);
//        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        alertDialogs.setContentView(R.layout.layout_multiple_image_popup);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(alertDialogs.getWindow().getAttributes());
//        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//        lp.width = displayMetrics.widthPixels;
//        lp.height = displayMetrics.heightPixels;
//        alertDialogs.getWindow().setAttributes(lp);
//
//
//        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//        ViewPager tagImages = (ViewPager) alertDialogs.findViewById(R.id.pagerTagImages);
//        ImageView imgClose = (ImageView) alertDialogs.findViewById(R.id.img_close);
//        ImageView imgShare = (ImageView) alertDialogs.findViewById(R.id.img_share);
//        ImageView imgDelete = (ImageView) alertDialogs.findViewById(R.id.img_delete);
//        imgShare.setVisibility(View.GONE);
//        imgDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showAlertDialogs(TagDetailsActivity.this, "", "Are you sure you want to delete?");
//            }
//        });
//
//        tagImages.setAdapter(new SlidingImage_Adapter(TagDetailsActivity.this, tagses, realm));
//
//        imgClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialogs.dismiss();
//
//
//            }
//        });
//
//        alertDialogs.setCancelable(true);
//        alertDialogs.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                friendTagList.clear();
//                friendTagList.addAll(realm.copyFromRealm(realm.where(Tags.class)
//                        .equalTo("sender_id", tags.getSender_id()).isNotNull("media").findAll()));
//                if (getMap() != null) startDemo();
//            }
//        });
//        alertDialogs.show();
    }


    //    @Override
    protected void startDemo() {
        if (friendTagList.size() > 0) {
            LatLngBounds.Builder builder = LatLngBounds.builder();
            boolean isUnreadAvailable= false;
            for (Tags tags :friendTagList) {
                if (tags.getIs_read() == 0){
                    isUnreadAvailable= true;
                    builder.include(tags.getPosition());
                }
            }
            if (isUnreadAvailable) {
                final LatLngBounds bounds = builder.build();
                try {
                    getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 7.5f));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else
                getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(friendTagList.get(0).getLatitude(), friendTagList.get(0).getLongitude()), 9.5f));
        }

        mClusterManager = new ClusterManager<Tags>(this, getMap());
        mClusterManager.setRenderer(new PersonRenderer());

        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);
        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);

        addItems();
        mClusterManager.cluster();
        Log.e("TagDetailsActivity", "startDemo: ");
    }

    private void addItems() {
        // http://www.flickr.com/photos/sdasmarchives/5036248203/
//        mClusterManager.addItem(new SampleTag(position(), "Walter", R.drawable.walter));
//        mClusterManager.addItem(new SampleTag(position(), tags.getSender_detail().getContact_number(), tags.getSender_detail().getProfile_picture(), tags.getMessage()));

        // http://www.flickr.com/photos/usnationalarchives/4726917149/
//        mClusterManager.addItem(new Person(position(), "Gran", R.drawable.gran));
//
//        // http://www.flickr.com/photos/nypl/3111525394/
//        mClusterManager.addItem(new Person(position(), "Ruth", R.drawable.ruth));
//
//        // http://www.flickr.com/photos/smithsonian/2887433330/
//        mClusterManager.addItem(new Person(position(), "Stefan", R.drawable.stefan));
//
//        // http://www.flickr.com/photos/library_of_congress/2179915182/
//        mClusterManager.addItem(new Person(position(), "Mechanic", R.drawable.mechanic));
//
//        // http://www.flickr.com/photos/nationalmediamuseum/7893552556/
//        mClusterManager.addItem(new Person(position(), "Yeats", R.drawable.yeats));
//
//        // http://www.flickr.com/photos/sdasmarchives/5036231225/
//        mClusterManager.addItem(new Person(position(), "John", R.drawable.john));
//
//        // http://www.flickr.com/photos/anmm_thecommons/7694202096/
//        mClusterManager.addItem(new Person(position(), "Trevor the Turtle", R.drawable.turtle));
//
//        // http://www.flickr.com/photos/usnationalarchives/4726892651/
//        mClusterManager.addItem(new Person(position(), "Teach", R.drawable.teacher));

        mClusterManager.addItems(friendTagList);
    }


    public void showAlertDialogs(final Context context, String title, String msg) {

        final Dialog alertDialogs = new Dialog(context);
        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogs.setContentView(R.layout.alert_delete);


        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        TextView txtaltmsg = (TextView) alertDialogs.findViewById(R.id.txtaltmsg);
        TextView txtaltyes = (TextView) alertDialogs.findViewById(R.id.tv_yes);
        TextView txtaltno = (TextView) alertDialogs.findViewById(R.id.tv_no);


        txtaltmsg.setText(msg);
//        txtaltmsg.setTextSize(14);
//        txtaltyes.setTextSize(14);
//        txtaltno.setTextSize(14);


        alertDialogs.setCancelable(false);

        txtaltyes.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {


                alertDialogs.dismiss();
            }

        });

        txtaltno.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {


                alertDialogs.dismiss();
            }

        });

        alertDialogs.show();
    }

    //    private void showImagePopup(Context context, int imageResource, String tagText) {
//    private void showImagePopup(Context context, String imageResource, String tagText) {
//
//        final Dialog alertDialogs = new Dialog(context);
//        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        alertDialogs.setContentView(R.layout.layout_poopup);
//
//
//        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//        ImageView imageView = (ImageView) alertDialogs.findViewById(R.id.iv_img);
//        TextView tvTag = (TextView) alertDialogs.findViewById(R.id.tv_tag);
//
//        tvTag.setText(tagText);
//
////        imageView.setImageResource(imageResource);
//
////        Glide.with(TagDetailsActivity.this).load(Urls.BASE_TAG_IMAGE_URL + imageResource).into(imageView);
//        Picasso.with(TagDetailsActivity.this)
//                .load(Urls.BASE_TAG_IMAGE_URL + imageResource)
//                .into(imageView);
//
//        alertDialogs.setCancelable(true);
//        alertDialogs.show();
//    }


    private void getFriendTagList() {


        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");

        GetFriendTagListRequestModel getFriendTagListRequestModel = new GetFriendTagListRequestModel();
        getFriendTagListRequestModel.setSecret_key(Utils.getFromUserDefaults(TagDetailsActivity.this, Constants.SECRET_KEY));
        getFriendTagListRequestModel.setAccess_key(Utils.getFromUserDefaults(TagDetailsActivity.this, Constants.ACCESS_KEY));
        getFriendTagListRequestModel.setUser_id(Utils.getFromUserDefaults(TagDetailsActivity.this, Constants.USER_ID));
        getFriendTagListRequestModel.setFriend_id(tags.getSender_id());
        getFriendTagListRequestModel.setIs_testdata("1");

        StringEntity entity = null;
        try {
            entity = new StringEntity(getMapper().writer().writeValueAsString(getFriendTagListRequestModel));
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "GET FRIEND TAG LIST URL-" + Urls.GET_FRIEND_TAG_LIST);
        client.post(TagDetailsActivity.this, Urls.GET_FRIEND_TAG_LIST, entity, null, new GetFriendTagListResponseHandler());


    }

    private class GetFriendTagListResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            progressWheel.setVisibility(View.GONE);
            if (responseBody != null) {
                try {
                    JSONObject object = new JSONObject(new String(responseBody));
                    if (object.getInt("status") == 1) {
                        final JSONArray array = object.getJSONArray("Tags");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject sender_detail = new JSONObject();
                            sender_detail.put("contact_number", tags.getSender_detail().getContact_number());
                            array.getJSONObject(i).put("sender_detail", sender_detail);
                        }
                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.createOrUpdateAllFromJson(Tags.class, array);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {

                                friendTagList.clear();
                                friendTagList.addAll(realm.copyFromRealm(realm.where(Tags.class)
                                        .equalTo("sender_id", tags.getSender_id()).isNotNull("media").findAll()));
                                if (getMap() != null) startDemo();

                                if (friendTagList.size() > 0) {
                                    new getImages().execute();
                                }


                            }
                        });


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }


//    private class GetCluster {
//
//        final List<Drawable> profilePhotos;
//        private final List<Tags> tagses;
//        int height;
//        int width;
//        MarkerOptions markerOptions;
//        IconGenerator iconGenerator;
//        private int index = 0;
//
//        GetCluster(Cluster<Tags> cluster, MarkerOptions markerOptions, int height, int width, IconGenerator iconGenerator) {
//            profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
//            this.height = height;
//            this.width = width;
//            tagses = (List<Tags>) cluster.getItems();
//            this.markerOptions = markerOptions;
//            this.iconGenerator = iconGenerator;
//        }
//
////        @Override
////        protected Object doInBackground(Object[] params) {
////
////            for (Tags p : cluster.getItems()) {
////                // Draw 4 at most.
////                if (profilePhotos.size() == 4) break;
////                if (!isFinishing()) {
//////                        Drawable drawable = getResources().getDrawable(p.profilePhoto);
////
////                    final String url = Urls.BASE_TAG_IMAGE_URL + p.getMedia().getMedia_url();
////
////                    Bitmap bitmap = getBitmapFromURL(url);
////                    Drawable drawable = new BitmapDrawable(getResources(), bitmap);
////                    drawable.setBounds(0, 0, width, height);
////                    profilePhotos.add(drawable);
////
////
////                }
////
////            }
////
////
////            return null;
////        }
//
//        void onPostExecute() {
//
//
//            MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
//            multiDrawable.setBounds(0, 0, width, height);
//
//            mClusterImageView.setImageDrawable(multiDrawable);
//
//            Bitmap icon = iconGenerator.makeIcon(String.valueOf(tagses.size()));
//            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
////             getMap().addMarker(markerOptions);
//            Log.e("GetCluster", "onPostExecute: Marker loded " + profilePhotos.size());
//
//        }
//
//        class MarkerTarget implements Target {
//
//
//            @Override
//            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//                BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
//                bitmapDrawable.setBounds(0, 0, width, height);
//                profilePhotos.add(bitmapDrawable);
//
//                index++;
//                execute();
//                Log.e("PersonRenderer", "onBitmapLoaded: ");
//            }
//
//            @Override
//            public void onBitmapFailed(Drawable errorDrawable) {
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                Log.e("PersonRenderer", "onPrepareLoad: ");
//            }
//        }
//
//
//        private void execute() {
//            try {
//                if (index < tagses.size()) {
//                    MarkerTarget markerTarget = new MarkerTarget();
//                    mClusterImageView.setTag(markerTarget);
//                    Picasso.with(TagDetailsActivity.this)
//                            .load(Urls.BASE_TAG_IMAGE_URL + tagses.get(index).getMedia().getVideo_thumb_image())
//                            .resize(width, height)
//                            .centerCrop()
//                            .into(markerTarget);
//                } else {
//                    onPostExecute();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//    }


}
