package com.followme.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.R;
import com.followme.listeners.AlertButtonsClickListener;
import com.followme.requestmodel.GetUserProfileRequestModel;
import com.followme.requestmodel.UpdateUserProfileRequestModel;
import com.followme.responsemodel.GetUserProfileResponseModel;
import com.followme.responsemodel.UpdateUserProfileResponseModel;
import com.followme.utils.Constants;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.followme.ws.FileUploadInput;
import com.followme.ws.WebserviceWrapper;
import com.followme.ws.WsResponseListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.Format;
import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class ProfileActivity extends BaseActivity {

    private String TAG = ProfileActivity.class.getName();
    private Toolbar toolbar;

    private ImageView ivProfileImage;
    private ImageView ivAddProfileImage;
    private TextView tvMobileNumber;
    private LinearLayout llDeleteAccount;

    private ProgressWheel progressWheel;
    private ProgressBar progressBar;

//    private ImageChooserManager imageChooserManager;
//    private String filePath;
//    private int chooserType;
//    File file;
//    String path;
//    String savedImageDirectory = Environment.getExternalStorageDirectory()
//            + File.separator + "FollowMe";
//    Format dateInFilename = new SimpleDateFormat("yyyyMMdd_HHmmss");

    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private String selectedFilePath;

    final static int MY_REQUEST_CODE = 3333;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
        setProfile();
        progressWheel.setVisibility(View.VISIBLE);
        getUserProfile();

        llDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.getConfirmDialog(ProfileActivity.this, "Delete Account", "Are you sure you want to delete account?", "Yes", "Cancel", false, new AlertButtonsClickListener() {
                    @Override
                    public void PositiveButtonsClick(DialogInterface dialog, int id) {

                        deleteUser();

                    }

                    @Override
                    public void NegativeButtonsClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


            }
        });

        ivAddProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.CAMERA)
                            != PackageManager.PERMISSION_GRANTED) {

                        requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE);

                    } else {
                        Intent intent = new Intent(ProfileActivity.this, CameraActivity.class);
                        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

                    }

                } else {
                    Intent intent = new Intent(ProfileActivity.this, CameraActivity.class);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);

                }

            }
        });
    }

    private void setProfile(){
        if (Utils.getFromUserDefaults(ProfileActivity.this, Constants.USER_PROFILE_IMAGE) != null) {
            if (!Utils.getFromUserDefaults(ProfileActivity.this, Constants.USER_PROFILE_IMAGE).equals("")) {
                int size = (int) getResources().getDimension(R.dimen.profile_image_size);
                Picasso.with(ProfileActivity.this)
                        .load(Utils.getFromUserDefaults(ProfileActivity.this, Constants.USER_PROFILE_IMAGE))
                        .resize(size,size)
                        .centerCrop()
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                ivProfileImage.setImageBitmap(bitmap);
                            }

                            @Override
                            public void onBitmapFailed(Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });
            }
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitleBar();

        ivProfileImage = (ImageView) findViewById(R.id.profile_image);
        ivAddProfileImage = (ImageView) findViewById(R.id.iv_add_profile_image);
        tvMobileNumber = (TextView) findViewById(R.id.tv_mobile_number);
        llDeleteAccount = (LinearLayout) findViewById(R.id.liner_delete_account);
        progressWheel = (ProgressWheel) findViewById(R.id.progress);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

    }

    public void setTitleBar() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Profile");
    }

    @Override
    public boolean onSupportNavigateUp() {
         onBackPressed();
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                if (data != null) {

                    selectedFilePath = data.getStringExtra("bitmapImagePath");
//                    Log.e(TAG, "IMAGE-" + imagePath);
                    Bitmap bitmap = createBitmap(selectedFilePath);
                    ivProfileImage.setImageBitmap(bitmap);
//                    base64Image = encodeImage(bitmap);
                    freeMemory();


//                    progressWheel.setVisibility(View.VISIBLE);
                    updateUserProfile();


                }
            }
        }
    }

    private void deleteUser() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");

        GetUserProfileRequestModel getUserProfileRequestModel = new GetUserProfileRequestModel();

        getUserProfileRequestModel.setAccessKey(Utils.getFromUserDefaults(ProfileActivity.this, Constants.ACCESS_KEY));
        getUserProfileRequestModel.setSecretKey(Utils.getFromUserDefaults(ProfileActivity.this, Constants.SECRET_KEY));
        getUserProfileRequestModel.setUserId(Utils.getFromUserDefaults(ProfileActivity.this, Constants.USER_ID));
        getUserProfileRequestModel.setIs_testdata("1");

        StringEntity entity = null;
        try {
            entity = new StringEntity(getMapper().writer().writeValueAsString(getUserProfileRequestModel));
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "GET USER PROFILE URL-" + Urls.DELETE_USER);
        client.post(ProfileActivity.this, Urls.DELETE_USER, entity, "application/json", new DeleteProfileHandler());
    }

    private class DeleteProfileHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            progressWheel.setVisibility(View.GONE);

            if (responseBody != null) {

                Log.e(TAG, "GET USER PROFILE RESPONSE-" + new String(responseBody));

                try {
                    JSONObject object = new JSONObject(new String(responseBody));

                    if (object.getString("status").equals("1")){

                        Toast.makeText(ProfileActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                        Utils.clearPreferences(ProfileActivity.this);
                        setResult(RESULT_OK);
                        ProfileActivity.this.finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }

    private void getUserProfile() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");


        GetUserProfileRequestModel getUserProfileRequestModel = new GetUserProfileRequestModel();


        getUserProfileRequestModel.setAccessKey(Utils.getFromUserDefaults(ProfileActivity.this, Constants.ACCESS_KEY));
        getUserProfileRequestModel.setSecretKey(Utils.getFromUserDefaults(ProfileActivity.this, Constants.SECRET_KEY));
        getUserProfileRequestModel.setUserId(Utils.getFromUserDefaults(ProfileActivity.this, Constants.USER_ID));
        getUserProfileRequestModel.setIs_testdata("1");


        StringEntity entity = null;
        try {
            entity = new StringEntity(getMapper().writer().writeValueAsString(getUserProfileRequestModel));
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "GET USER PROFILE URL-" + Urls.GET_USER_PROFILE);
        client.post(ProfileActivity.this, Urls.GET_USER_PROFILE, entity, "application/json", new GetUserProfileHandler());
    }

    private class GetUserProfileHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            progressWheel.setVisibility(View.GONE);

            if (responseBody != null) {

                Log.e(TAG, "GET USER PROFILE RESPONSE-" + new String(responseBody));

                GetUserProfileResponseModel getUserProfileResponseModel = null;
                try {
                    getUserProfileResponseModel = getMapper().readValue(responseBody, GetUserProfileResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (getUserProfileResponseModel != null) {
                    if (getUserProfileResponseModel.getStatus().equals("1")) {
                        Log.e(TAG, "PROFILE IMAGE-" + getUserProfileResponseModel.getUser().get(0).getProfile_picture());

                        if (!getUserProfileResponseModel.getUser().get(0).getProfile_picture().equals("")) {
                            Utils.saveToUserDefaults(ProfileActivity.this, Constants.USER_PROFILE_IMAGE, Urls.BASE_IMAGE_URL + getUserProfileResponseModel.getUser().get(0).getProfile_picture());
                            setProfile();
                        }

                        tvMobileNumber.setText("+" + getUserProfileResponseModel.getUser().get(0).getCountry_code() + " " +
                                getUserProfileResponseModel.getUser().get(0).getContact_number());


                        if (getUserProfileResponseModel.getUser().get(0).getIs_push_enabled() == 1) {
                            Utils.saveSettingsToUserDefaults(ProfileActivity.this, Constants.IS_NOTIFICATION_ENABLE, true);
                        } else if (getUserProfileResponseModel.getUser().get(0).getIs_push_enabled() == 0) {
                            Utils.saveSettingsToUserDefaults(ProfileActivity.this, Constants.IS_NOTIFICATION_ENABLE, false);
                        }


                    } else if (getUserProfileResponseModel.getStatus().equals("0")) {
                        Toast.makeText(ProfileActivity.this, getUserProfileResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }


    private void updateUserProfile() {


//        AsyncHttpClient client = new AsyncHttpClient();
//        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
//        client.addHeader("User-Agent", "Android");
//
//
//        UpdateUserProfileRequestModel updateUserProfileRequestModel = new UpdateUserProfileRequestModel();
//
//        updateUserProfileRequestModel.setSecretKey(Utils.getFromUserDefaults(ProfileActivity.this, Constants.SECRET_KEY));
//        updateUserProfileRequestModel.setAccessKey(Utils.getFromUserDefaults(ProfileActivity.this, Constants.ACCESS_KEY));
//        updateUserProfileRequestModel.setUserId(Utils.getFromUserDefaults(ProfileActivity.this, Constants.USER_ID));
//        updateUserProfileRequestModel.setContactNumber(Utils.getFromUserDefaults(ProfileActivity.this, Constants.CONTACT_NUMBER));
//        updateUserProfileRequestModel.setIs_testdata("1");
//        updateUserProfileRequestModel.setProfilePicture(base64Image);
//
//
//        StringEntity entity = null;
//        try {
//            entity = new StringEntity(getMapper().writer().writeValueAsString(updateUserProfileRequestModel));
//        } catch (UnsupportedEncodingException | JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        Log.e(TAG, "UPDATE USER PROFILE URL-" + Urls.UPDATE_USER_PROFILE);
//        client.post(ProfileActivity.this, Urls.UPDATE_USER_PROFILE, entity, "application/json", new updateUserProfileResponseHandler());


        FileUploadInput fileUploadInput = new FileUploadInput();
        fileUploadInput.paramList.add(fileUploadInput.new Item("user_id",Utils.getFromUserDefaults(ProfileActivity.this, Constants.USER_ID)));
        fileUploadInput.paramList.add(fileUploadInput.new Item("is_testdata", "1"));
        fileUploadInput.paramList.add(fileUploadInput.new Item("contact_number", Utils.getFromUserDefaults(ProfileActivity.this, Constants.CONTACT_NUMBER)));
        fileUploadInput.fileList.add(fileUploadInput.new Item("profile_picture", selectedFilePath));
        WebserviceWrapper webserviceWrapper =new WebserviceWrapper(this, null, new WsResponseListener() {
            @Override
            public void onDelieverResponse(String serviceType, Exception err, Object data) {
                progressBar.setVisibility(View.GONE);
                if (data!=null){
                    try {
                        JSONObject jsonObject =new JSONObject(data.toString());
                        if (jsonObject.getString("status").equals("1")){
                            Log.e(TAG, "UPDATE USER PROFILE RESPONSE-" + data.toString());
                            UpdateUserProfileResponseModel updateUserProfileResponseModel = null;
                            try {
                                updateUserProfileResponseModel = getMapper().readValue(data.toString(),  UpdateUserProfileResponseModel.class);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            if (updateUserProfileResponseModel != null) {
                                if (updateUserProfileResponseModel.getStatus().equals("1")) {
                                    Log.e(TAG, "UPDATE IMAGE-" + updateUserProfileResponseModel.getUser().get(0).getProfile_picture());


                                    if (!updateUserProfileResponseModel.getUser().get(0).getProfile_picture().equals("")) {
                                        Utils.saveToUserDefaults(ProfileActivity.this, Constants.USER_PROFILE_IMAGE, Urls.BASE_IMAGE_URL + updateUserProfileResponseModel.getUser().get(0).getProfile_picture());
                                        setProfile();
                                    }

                                    tvMobileNumber.setText("+" + updateUserProfileResponseModel.getUser().get(0).getCountry_code()+ " " +
                                            updateUserProfileResponseModel.getUser().get(0).getContact_number());
                                } else if (updateUserProfileResponseModel.getStatus().equals("0")) {
                                    Toast.makeText(ProfileActivity.this, updateUserProfileResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        });
        webserviceWrapper.executeUploadMediaFile(Urls.UPDATE_USER_PROFILE,fileUploadInput);
        progressBar.setVisibility(View.VISIBLE);
    }

    private class updateUserProfileResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            progressWheel.setVisibility(View.GONE);


            if (responseBody != null) {

                Log.e(TAG, "UPDATE USER PROFILE RESPONSE-" + new String(responseBody));
                UpdateUserProfileResponseModel updateUserProfileResponseModel = null;
                try {
                    updateUserProfileResponseModel = getMapper().readValue(responseBody,  UpdateUserProfileResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (updateUserProfileResponseModel != null) {
                    if (updateUserProfileResponseModel.getStatus().equals("1")) {
                        Log.e(TAG, "UPDATE IMAGE-" + updateUserProfileResponseModel.getUser().get(0).getProfile_picture());


                        if (!updateUserProfileResponseModel.getUser().get(0).getProfile_picture().equals("")) {

                            Picasso.with(ProfileActivity.this)
                                    .load(Urls.BASE_IMAGE_URL + updateUserProfileResponseModel.getUser().get(0).getProfile_picture())
                                    .into(ivProfileImage);

                            Utils.saveToUserDefaults(ProfileActivity.this, Constants.USER_PROFILE_IMAGE, Urls.BASE_IMAGE_URL + updateUserProfileResponseModel.getUser().get(0).getProfile_picture());
                        }

                        tvMobileNumber.setText("+" + updateUserProfileResponseModel.getUser().get(0).getCountry_code()+ " " +
                                updateUserProfileResponseModel.getUser().get(0).getContact_number());
                    } else if (updateUserProfileResponseModel.getStatus().equals("0")) {
                        Toast.makeText(ProfileActivity.this, updateUserProfileResponseModel.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

            }

        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            error.printStackTrace();
        }
    }

    Bitmap bitmap;

    private Bitmap createBitmap(String imagePath) {

//        Bitmap bitmap = null;

        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }


        bitmap = BitmapFactory.decodeFile(imagePath);
        return bitmap;
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }


    public static void freeMemory() {
        System.runFinalization();
        Runtime.getRuntime().gc();
        System.gc();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case MY_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(ProfileActivity.this, CameraActivity.class);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {


                            requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_REQUEST_CODE);

                        }

                    }

                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
