package com.followme.ui;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.R;
import com.followme.requestmodel.ChangePushStatusRequestModel;
import com.followme.responsemodel.ChangePushStatusResponseModel;
import com.followme.utils.Constants;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class SettingsActivity extends BaseActivity {

    private String TAG = SettingsActivity.class.getName();
    private Toolbar toolbar;
    private SwitchCompat aSwitch;
    private ProgressWheel progressWheel;
    private boolean isNotificationEnable = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        init();

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.e(TAG, "CHECKED");
//                    Utils.saveSettingsToUserDefaults(SettingsActivity.this, Constants.IS_NOTIFICATION_ENABLE, true);
                    progressWheel.setVisibility(View.VISIBLE);
                    isNotificationEnable = true;
                    changePushStatus(1);

                } else {
                    Log.e(TAG, "UNCHECKED");
//                    Utils.saveSettingsToUserDefaults(SettingsActivity.this, Constants.IS_NOTIFICATION_ENABLE, false);
                    progressWheel.setVisibility(View.VISIBLE);
                    isNotificationEnable = false;
                    changePushStatus(0);
                }
            }
        });

    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitleBar();

        aSwitch = (SwitchCompat) findViewById(R.id.switch_notification);
        progressWheel = (ProgressWheel) findViewById(R.id.progress);

//        if (Utils.getBooleanFromUserDefaults(SettingsActivity.this, Constants.IS_SETTINGS_FIRST_TIME)) {
//            Utils.saveBooleanToUserDefaults(SettingsActivity.this, Constants.IS_SETTINGS_FIRST_TIME, false);
//            aSwitch.setChecked(true);
//        } else {
        if (Utils.getSettingsFromUserDefaults(SettingsActivity.this, Constants.IS_NOTIFICATION_ENABLE)) {
            aSwitch.setChecked(true);
        } else {
            aSwitch.setChecked(false);
        }
//        }


    }


    public void setTitleBar() {


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Settings");
    }

    @Override
    public boolean onSupportNavigateUp() {
//        return super.onSupportNavigateUp();
        onBackPressed();
        return true;
    }


    private void changePushStatus(int isPushEnable) {


        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");


        ChangePushStatusRequestModel changePushStatusRequestModel = new ChangePushStatusRequestModel();


        changePushStatusRequestModel.setAccess_key(Utils.getFromUserDefaults(SettingsActivity.this, Constants.ACCESS_KEY));
        changePushStatusRequestModel.setSecret_key(Utils.getFromUserDefaults(SettingsActivity.this, Constants.SECRET_KEY));
        changePushStatusRequestModel.setUser_id(Utils.getFromUserDefaults(SettingsActivity.this, Constants.USER_ID));
        changePushStatusRequestModel.setIs_testdata("1");
        changePushStatusRequestModel.setIs_push_enabled(String.valueOf(isPushEnable));



        StringEntity entity = null;
        try {
            entity = new StringEntity(getMapper().writer().writeValueAsString(changePushStatusRequestModel));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "CHANGE PUSH STATUS URL-" + Urls.CHANGE_PUSH_STATUS);
        client.post(SettingsActivity.this, Urls.CHANGE_PUSH_STATUS, entity, "application/json", new ChangePushStatusResponseHandler());


    }


    private class ChangePushStatusResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            progressWheel.setVisibility(View.GONE);

            if (responseBody != null) {
                Log.e(TAG, "CHANGE PUSH STATUS RESPONSE-" + new String(responseBody));

                ChangePushStatusResponseModel changePushStatusResponseModel = null;
                try {
                    changePushStatusResponseModel = getMapper().readValue(responseBody, ChangePushStatusResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (changePushStatusResponseModel != null && changePushStatusResponseModel.getStatus().equals("1")) {
                    if (isNotificationEnable) {
                        Utils.saveSettingsToUserDefaults(SettingsActivity.this, Constants.IS_NOTIFICATION_ENABLE, true);
                    } else {
                        Utils.saveSettingsToUserDefaults(SettingsActivity.this, Constants.IS_NOTIFICATION_ENABLE, false);
                    }
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }
}
