package com.followme.utils;

/**
 * Created by pc on 04/05/17.
 */

public class Urls {

    //http://13.228.195.171/services/Webservice.php?Service=refreshToken
    public static final String Server_URL = "http://13.228.195.171/services/Webservice.php";
    public static final String BASE_URL = "http://13.228.195.171/services/Webservice.php?Service=";
    public static final String BASE_IMAGE_URL = "http://13.228.195.171/services/user_images/";
    public static final String BASE_TAG_IMAGE_URL = "http://13.228.195.171/services/";

    public static final String REFRESH_TOKEN = BASE_URL + "refreshToken";
    public static final String SEND_CONFIRMATION_CODE = BASE_URL + "sendConfirmationCode";
    public static final String VERIFY_MOBILE_NUMBER = BASE_URL + "verifyMobileNumber";
    public static final String GET_USER_PROFILE = BASE_URL + "getUserProfile";
    public static final String UPDATE_USER_PROFILE = BASE_URL + "updateUserProfile";
    public static final String GET_CONTACTS_DETAIL = BASE_URL + "getContactsDetail";
    public static final String ADD_TAG = BASE_URL + "addTags";
    public static final String CHANGE_PUSH_STATUS = BASE_URL + "changePushStatus";
    public static final String GET_MESSAGE_LIST = BASE_URL + "getMessageList";
    public static final String DELETE_TAG = BASE_URL + "deleteTag";
    public static final String GET_MY_TAGS = BASE_URL + "getMyTags";
    public static final String GET_FRIEND_TAG_LIST = BASE_URL + "getFriendTagList";
    public static final String UPDATE_TAG_STATUS = BASE_URL +  "updateTagStatus";
    public static final String DELETE_USER = BASE_URL + "deleteAccount";
}
