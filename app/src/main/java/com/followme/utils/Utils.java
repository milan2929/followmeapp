package com.followme.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.followme.R;
import com.followme.listeners.AlertButtonsClickListener;

/**
 * Created by pc on 04/05/17.
 */

public class Utils {

    public static void getConfirmDialog(Context mContext, String title, String msg, String positiveBtnCaption, String negativeBtnCaption, boolean isCancelable, final AlertButtonsClickListener target) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mContext);

        builder.setTitle(title)
                .setMessage(msg)
                .setCancelable(isCancelable)
                .setPositiveButton(positiveBtnCaption,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                target.PositiveButtonsClick(dialog, id);
                            }
                        })
                .setNegativeButton(negativeBtnCaption,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                target.NegativeButtonsClick(dialog, id);
                            }
                        });

        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
        TextView messageText = (TextView) alert.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }


    public static void getAlertmDialog(Context mContext, String title, String msg, String positiveBtnCaption, final AlertButtonsClickListener target) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mContext);
        builder.setTitle(title)
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(positiveBtnCaption,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                target.PositiveButtonsClick(dialog, id);
                            }
                        });
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
        TextView messageText = (TextView) alert.findViewById(android.R.id.message);
        messageText.setGravity(Gravity.CENTER);
    }


    public static void saveCameraFlashMode(@NonNull final Context context, @NonNull final String cameraFlashMode) {

        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);


        if (preferences != null) {
            final SharedPreferences.Editor editor = preferences.edit();
            editor.putString(Constants.FLASH_MODE, cameraFlashMode);
            editor.apply();
        }
    }

    public static String getCameraFlashMode(@NonNull final Context context) {

        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        if (preferences != null) {
            return preferences.getString(Constants.FLASH_MODE, Camera.Parameters.FLASH_MODE_AUTO);
        }

        return Camera.Parameters.FLASH_MODE_AUTO;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }


    public static void saveToUserDefaults(Context context, String key, String value) {

//        Log.d("Utils", "Saving:" + key + ":" + value);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();

    }

    public static String getFromUserDefaults(Context context, String key) {
        Log.d("Utils", "Get:" + key);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getString(key, "");
    }

    public static void saveIntegerToUserDefaults(Context context, String key, int value) {

//        Log.d("Utils", "Saving:" + key + ":" + value);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.apply();

    }

    public static int getIntegerFromUserDefaults(Context context, String key) {
//        Log.d("Utils", "Get:" + key);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getInt(key, 0);
    }

    public static String getDeviceToken(Context context) {
        String ts = Context.TELEPHONY_SERVICE;
        TelephonyManager mTelephonyMgr = (TelephonyManager) context.getSystemService(ts);
        return mTelephonyMgr.getDeviceId();
    }


    public static void showNoInternetAlertDialog(final Context context) {

        final Dialog alertDialogs = new Dialog(context);
        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogs.setContentView(R.layout.alert_no_connection);

        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView txtaltmsg = (TextView) alertDialogs.findViewById(R.id.txtaltmsg);
        TextView txtaltok = (TextView) alertDialogs.findViewById(R.id.tv_ok);


        txtaltmsg.setText(context.getResources().getString(R.string.no_connection));


        alertDialogs.setCancelable(false);

        txtaltok.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                alertDialogs.dismiss();
            }

        });

        alertDialogs.show();
    }


    public static void saveBooleanToUserDefaults(Context context, String key, boolean value) {

        Log.d("Utils", "Saving:" + key + ":" + value);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();

    }

    public static boolean getBooleanFromUserDefaults(Context context, String key) {
        Log.d("Utils", "Get:" + key);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    public static void clearPreferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences( Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    public static void saveSettingsToUserDefaults(Context context, String key, boolean value) {

        Log.d("Utils", "Saving:" + key + ":" + value);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.apply();

    }

    public static boolean getSettingsFromUserDefaults(Context context, String key) {
        Log.d("Utils", "Get:" + key);
        SharedPreferences preferences = context.getSharedPreferences(
                Constants.SHARED_PREFS, Context.MODE_PRIVATE);
        return preferences.getBoolean(key, true);
    }

    public static boolean isValidPhoneNumber(String number) {


        if (number.length() == 10) {
            return true;
        } else if (number.length() > 10) {
            return true;
        } else {
            // whatever is appropriate in this case
            return false;
        }
    }

    public static String getCountryCode(String number) {

        if (number != null) {
            if (!number.equals("")) {
                if (number.length() > 10) {
                    return number.substring(0, number.length() - 10);
                }
            }
        }

        return "";
    }

    public static String getPhoneNumber(String number) {

        if (number.length() == 10) {
            return number;
        } else if (number.length() > 10) {
            return number.substring(number.length() - 10);
        } else {
            // whatever is appropriate in this case
//            throw new IllegalArgumentException("word has less than 3 characters!");
            return "";

        }
    }

}
