package com.followme.utils;

import com.followme.realm.MyMigration;

import io.realm.RealmConfiguration;

/**
 * Created by pc on 04/05/17.
 */

public class Constants {

    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static RealmConfiguration config = new RealmConfiguration.Builder()
            .name("followme.realm")
            .schemaVersion(0)
            .migration(new MyMigration())
            .build();

    public final static String SHARED_PREFS = "FollowMePreferences";
//    public static String MM_DIR_NAME = "FollowMe";
    public static String FLASH_MODE = "flash_mode";

    public static int CONNECTION_TIME_OUT = 10000; // 10 seconds connection timeout

    public static final String ACCESS_KEY = "access_key";
    public static final String ACCESS_KEY_VALUE = "nousername";


    public static final String SECRET_KEY = "secret_key";
    public static final String SECRET_KEY_VALUE = "0EruaRM6NNDW+KI7YfifMAagLYVLwZH5jPBVdDDNcME=";

    public static final String USER_ID = "userId";
    public static final String GLOBAL_PASSWORD = "globalPassword";
    public static final String CONTACT_NUMBER = "contactNumber";

    public static final String IS_NUMBER_VERIFIED = "isVerified";
    public static final String isContactSynced = "isContactSynced";

    public static final String IS_NOTIFICATION_ENABLE = "isNotificationEnable";
//    public static final String IS_SETTINGS_FIRST_TIME = "isFirstTime";

    public static final String USER_PROFILE_IMAGE = "userProfileImage";
    public static final String MY_TAG="myTag";

    public static final int TAG_READ = 2;
    public static final int TAG_DELIVERED = 1;

//    public static ArrayList<Contacts> listContact = new ArrayList<>();
}
