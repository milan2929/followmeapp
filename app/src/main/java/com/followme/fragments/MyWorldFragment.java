package com.followme.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.util.LruCache;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.R;
import com.followme.adapter.SlidingImage_Adapter;
import com.followme.helper.MultiImageDialogue;
import com.followme.realm.Tags;
import com.followme.requestmodel.GetMyTagsRequestModel;
import com.followme.ui.HomeActivity;
import com.followme.ui.NewTagActivity;
import com.followme.utils.Constants;
import com.followme.utils.MultiDrawable;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.realm.Realm;

import static com.followme.ui.BaseActivity.getMapper;


/**
 * Created by pc on 05/05/17.
 */

public class MyWorldFragment extends Fragment implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<Tags>, ClusterManager.OnClusterInfoWindowClickListener<Tags>, ClusterManager.OnClusterItemClickListener<Tags>, ClusterManager.OnClusterItemInfoWindowClickListener<Tags> {

    private String TAG = MyWorldFragment.class.getName();

    private GoogleMap mMap;
    //    private ClusterManager<Person> mClusterManager;
//    private ClusterManager<GetMyTagsResponseModel.Tags> mClusterManager;
    private ClusterManager<Tags> mClusterManager;

//    private Random mRandom = new Random(1984);
    private ProgressWheel progressWheel;

    SupportMapFragment supportMapFragment;
    Menu menu;
    AdView adView;

//    private DBHalperClass dbHalperClass;

    //    ArrayList<Tags> tagsArrayList = new ArrayList<>();
    ArrayList<Tags> arrayList = new ArrayList<>();
    public Tags myTag = null;
    private Realm realm;
//    private int mDimension;
//    private int index;
    private LruCache<String, Bitmap> mMemoryCache;
//    private ViewPager tagImages;
//    private ImageView img_forward;
//    private ImageView img_backward;

//    private ArrayList<String> mUrls = new ArrayList<>();
//    private ArrayList<Integer> mPositions = new ArrayList<>();


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        realm = ((HomeActivity) getActivity()).realm;
//        mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
        final View view = inflater.inflate(R.layout.fragment_my_world, container, false);
        initView(view);
        getMyTags();
        return view;


    }


    private void initView(View view) {

//        dbHalperClass = new DBHalperClass(getActivity());


        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, @NonNull Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_my_world);
        progressWheel = (ProgressWheel) view.findViewById(R.id.progress);

        adView = (AdView) view.findViewById(R.id.adView);
        AdRequest.Builder build = new AdRequest.Builder();
         build.addTestDevice("45784DE82D84CC18577404F63C990355");
        adView.loadAd(build.build());

        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), NewTagActivity.class);
                startActivity(intent);
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        progressWheel.setVisibility(View.VISIBLE);
//        getMyTags();
//        getTags();

        if (mMap != null) {

//            if (Utils.isNetworkAvailable(getActivity())) {
//                getMyTags(); // API Call
//            } else {
//            getTags(); // Get data offline
            getTagFromDataBase();
//            }

        }
    }


    private class getImages extends AsyncTask<Void, Integer, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                for (Tags tags : arrayList) {
                    try {
                        URL url = new URL(Urls.BASE_TAG_IMAGE_URL + tags.getMedia().getVideo_thumb_image());
                        addBitmapToMemoryCache(Urls.BASE_TAG_IMAGE_URL + tags.getMedia().getVideo_thumb_image(), BitmapFactory.decodeStream(url.openConnection().getInputStream()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            if (arrayList.size() > 0 && getMap()!=null)
                startDemo();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mMap != null) {
            return;
        }

        mMap = googleMap;
        getTagFromDataBase();

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
//                Toast.makeText(getActivity(), "MapClickListener", Toast.LENGTH_SHORT).show();

                if (menu != null) {
//                    menu.findItem(R.id.action_view).setVisible(false);
//                    menu.findItem(R.id.action_share).setVisible(false);
//                    menu.findItem(R.id.action_delete).setVisible(false);
                    menu.findItem(R.id.action_refresh).setVisible(true);
                }
            }
        });


    }


    protected GoogleMap getMap() {
        return mMap;
    }
//
//    @Override
//    public boolean onClusterClick(Cluster<Person> cluster) {
//        return false;
//    }

    //    private final IconGenerator mClusterIconGenerator = new IconGenerator(getActivity().getApplicationContext());


    private class PersonRenderer extends DefaultClusterRenderer<Tags> {
        private final IconGenerator mIconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final IconGenerator mClusterIconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final ImageView mImageView;
        private final ImageView mClusterImageView;
        private final int mDimension;

        PersonRenderer() {
            super(getActivity().getApplicationContext(), getMap(), mClusterManager);

            View multiProfile = getActivity().getLayoutInflater().inflate(R.layout.multi_profile, null);
            mClusterIconGenerator.setContentView(multiProfile);
            mClusterImageView = (ImageView) multiProfile.findViewById(R.id.image);

            mImageView = new ImageView(getActivity().getApplicationContext());
            mDimension = (int) getResources().getDimension(R.dimen.custom_profile_image);
            mImageView.setLayoutParams(new ViewGroup.LayoutParams(mDimension, mDimension));
            int padding = (int) getResources().getDimension(R.dimen.custom_profile_padding);
            mImageView.setPadding(padding, padding, padding, padding);
            mIconGenerator.setContentView(mImageView);
        }

//        class BeforeClusterRendered {
//            Cluster<Tags> cluster;
//            MarkerOptions markerOptions;
//            List<Drawable> profilePhotos;
//            Tags tags;
//
//            BeforeClusterRendered(Cluster<Tags> cluster, MarkerOptions markerOptions) {
//                this.cluster = cluster;
//                this.markerOptions = markerOptions;
//                profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
//            }
//
//            void loadCluster() {
//                if (cluster.getSize() > 0) {
//                    for (Tags p : cluster.getItems()) {
//
//                        tags = p;
//                        Bitmap bitmap = getBitmapFromMemCache(p.getMedia().getVideo_thumb_image());
//                        if (bitmap == null) {
//                            Picasso.with(getActivity())
//                                    .load(Urls.BASE_TAG_IMAGE_URL + p.getMedia().getVideo_thumb_image())
//                                    .resize(mDimension, mDimension)
//                                    .centerCrop()
//                                    .into(new Target() {
//                                        @Override
//                                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//
//                                            addBitmapToMemoryCache(tags.getMedia().getVideo_thumb_image(), bitmap);
//                                            mClusterImageView.setImageBitmap(bitmap);
//                                            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
//                                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
//                                        }
//
//                                        @Override
//                                        public void onBitmapFailed(Drawable errorDrawable) {
//
//                                        }
//
//                                        @Override
//                                        public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                                        }
//                                    });
//                        } else {
//                            mClusterImageView.setImageBitmap(bitmap);
//                            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
//                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
//                        }
//                        break;
//                    }
//                } else {
//                    mClusterImageView.setImageDrawable(null);
//                    Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
//                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
//                }
//
//            }
//
//        }


        @Override
        protected void onBeforeClusterItemRendered(final Tags person, final MarkerOptions markerOptions) {
            // Draw a single person.
            // Set the info window to show their name.

//
//            Bitmap bitmap = getBitmapFromMemCache(person.getMedia().getMedia_url());
//            if (bitmap == null) {
//                Picasso.with(getActivity())
//                        .load(Urls.BASE_TAG_IMAGE_URL + person.getMedia().getMedia_url())
//                        .resize(mDimension, mDimension)
//                        .centerCrop()
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//
//                                addBitmapToMemoryCache(person.getMedia().getMedia_url(), bitmap);
//                                mImageView.setImageBitmap(bitmap);
//                                Bitmap icon = mIconGenerator.makeIcon();
//                                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Drawable errorDrawable) {
//
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                            }
//                        });
//            } else {
//                mImageView.setImageBitmap(bitmap);
//                Bitmap icon = mIconGenerator.makeIcon();
//                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");
//            }

            Bitmap bitmap = getBitmapFromMemCache(Urls.BASE_TAG_IMAGE_URL + person.getMedia().getVideo_thumb_image());
            if (bitmap != null) {
                mImageView.setImageBitmap(bitmap);
            }
            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");

//            MarkerTarget markerTarget =new MarkerTarget(markerOptions, person);
//            mImageView.setTag(markerTarget);
//
//            try {
//
//                Picasso.with(getActivity())
//                        .load(Urls.BASE_TAG_IMAGE_URL + person.getMedia().getVideo_thumb_image())
//                        .resize(mDimension, mDimension)
//                        .into(markerTarget);
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }

        }
//        class MarkerTarget implements Target {
//
//            MarkerOptions markerOptions;
//
//            MarkerTarget(MarkerOptions markerOptions, Tags person) {
//                this.markerOptions = markerOptions;
//            }
//
//            @Override
//            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
//                mImageView.setImageBitmap(bitmap);
//                Bitmap icon = mIconGenerator.makeIcon();
//                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");
//                getMap().addMarker(markerOptions);
//                Log.e("PersonRenderer", "onBitmapLoaded: " );
//            }
//
//            @Override
//            public void onBitmapFailed(Drawable errorDrawable) {
//            }
//
//            @Override
//            public void onPrepareLoad(Drawable placeHolderDrawable) {
//                Bitmap icon = mIconGenerator.makeIcon();
//                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title("");
//                Log.e("PersonRenderer", "onPrepareLoad: " );
//            }
//        }
        @Override
        protected void onBeforeClusterRendered(Cluster<Tags> cluster, MarkerOptions markerOptions) {
            // Draw multiple people.
            // Note: this method runs on the UI thread. Don't spend too much time in here (like in this example).
//            BeforeClusterRendered clusterRendered = new BeforeClusterRendered(cluster, markerOptions);
//            clusterRendered.loadCluster();

            List<Drawable> profilePhotos = new ArrayList<Drawable>(Math.min(4, cluster.getSize()));
            int width = mDimension;
            int height = mDimension;


            for (Tags p : cluster.getItems()) {
                // Draw 4 at most.
                if (profilePhotos.size() == 4) break;

                Bitmap bitmap = getBitmapFromMemCache(Urls.BASE_TAG_IMAGE_URL + p.getMedia().getVideo_thumb_image());
                if (bitmap != null) {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
                    bitmapDrawable.setBounds(0, 0, width, height);
                    profilePhotos.add(bitmapDrawable);
                }


            }
            if (profilePhotos.size()>0) {
                MultiDrawable multiDrawable = new MultiDrawable(profilePhotos);
                multiDrawable.setBounds(0, 0,  width, height);

                mClusterImageView.setImageDrawable(multiDrawable);
            }
            Bitmap icon = mClusterIconGenerator.makeIcon(String.valueOf(cluster.getSize()));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));

        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }


    @Override
    public boolean onClusterClick(Cluster<Tags> cluster) {

        onMarkerClick(cluster);

        return true;
    }

    private void onMarkerClick(Cluster<Tags> cluster) {
        if (menu != null) {
//            menu.findItem(R.id.action_view).setVisible(false);
//            menu.findItem(R.id.action_share).setVisible(false);
//            menu.findItem(R.id.action_delete).setVisible(false);
            menu.findItem(R.id.action_refresh).setVisible(true);
        }


        ArrayList<Tags> list = (ArrayList<Tags>) cluster.getItems();
        showMultipleImagePopup(getActivity(), list);

//        showMultipleImagePopup(getActivity(), images, tagMessage);
//        LatLngBounds.Builder builder = LatLngBounds.builder();
//        for (ClusterItem item : cluster.getItems()) {
//            builder.include(item.getPosition());
//        }
//        // Get the LatLngBounds
//        final LatLngBounds bounds = builder.build();
//
//        // Animate camera to the bounds
//        try {
//            getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<Tags> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(Tags item) {
        // Does nothing, but you could go into the user's profile page, for example.
//        Toast.makeText(getActivity(), "Cluster Item Clicked", Toast.LENGTH_SHORT).show();

        if (menu != null) {
//            menu.findItem(R.id.action_view).setVisible(true);
//            menu.findItem(R.id.action_share).setVisible(true);
//            menu.findItem(R.id.action_delete).setVisible(true);
            menu.findItem(R.id.action_refresh).setVisible(false);
        }

        myTag = item;

        ArrayList<Tags> tagses =new ArrayList<>();
        tagses.add(item);

        showMultipleImagePopup(getActivity(), tagses);

        return false;
    }

    private void addBitmapToMemoryCache(@NonNull String key, @NonNull Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    private void removeBitmapFromMemCache(@NonNull String key) {
        mMemoryCache.remove(key);
    }

    @Override
    public void onClusterItemInfoWindowClick(Tags item) {
        // Does nothing, but you could go into the user's profile page, for example.

         myTag = item;

        ArrayList<Tags> tagses =new ArrayList<>();
        tagses.add(item);

        showMultipleImagePopup(getActivity(), tagses);
    }

    //    @Override
    protected void startDemo() {
//        getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(arrayList.get(0).getLatitude(), arrayList.get(0).getLongitude()), 9.5f));

        try {
            if (mClusterManager!=null){
                mClusterManager.clearItems();
                mClusterManager.cluster();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (arrayList.size() > 0) {
            LatLngBounds.Builder builder = LatLngBounds.builder();
            for (int i = 0; i < arrayList.size(); i++) {
                builder.include(arrayList.get(i).getPosition());
            }
            // Get the LatLngBounds
            final LatLngBounds bounds = builder.build();

            // Move camera to the bounds
            try {
//            getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
                getMap().moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        mClusterManager = new ClusterManager<Tags>(getActivity(), getMap());

        mClusterManager.setOnClusterClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);
        mClusterManager.setOnClusterItemClickListener(this);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);

        mClusterManager.setRenderer(new PersonRenderer());
        getMap().setOnCameraIdleListener(mClusterManager);
        getMap().setOnMarkerClickListener(mClusterManager);
        getMap().setOnInfoWindowClickListener(mClusterManager);


        mClusterManager.addItems(arrayList);
        mClusterManager.cluster();
//        if (arrayList.size() > 0) {
//            index = 0;
//            loadRecursiveMarker();
//        }

    }

//    private void loadRecursiveMarker() {
//        try {
//            if (index < arrayList.size()) {
//                Picasso.with(getActivity())
//                        .load(Urls.BASE_TAG_IMAGE_URL + arrayList.get(index).getMedia().getVideo_thumb_image())
//                        .resize(mDimension, mDimension)
//                        .centerCrop()
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                                index++;
//                                loadRecursiveMarker();
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Drawable errorDrawable) {
//
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                            }
//                        });
//            } else {
//                mClusterManager.cluster();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    private double random(double min, double max) {
//        return mRandom.nextDouble() * (max - min) + min;
//    }


    @Override
    public void
    onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        menu.findItem(R.id.action_view).setVisible(false);
//        menu.findItem(R.id.action_share).setVisible(false);
//        menu.findItem(R.id.action_delete).setVisible(false);
        menu.findItem(R.id.action_refresh).setVisible(true);
        this.menu = menu;

        super.onCreateOptionsMenu(menu, inflater);
    }

//    private void showImagePopup(Context context, String imageResource, String tagText) {
//
//        final Dialog alertDialogs = new Dialog(context);
//        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        alertDialogs.setContentView(R.layout.layout_poopup);
//
//        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//        ImageView imageView = (ImageView) alertDialogs.findViewById(R.id.iv_img);
//        TextView tvTag = (TextView) alertDialogs.findViewById(R.id.tv_tag);
//
//        tvTag.setText(tagText);
////        imageView.setImageResource(imageResource);
//
////        Glide.with(getActivity()).load(Urls.BASE_TAG_IMAGE_URL + imageResource).into(imageView);
//
//        Picasso.with(getActivity())
//                .load(Urls.BASE_TAG_IMAGE_URL + imageResource)
//                .into(imageView);
//
////        Bitmap bitmap = createBitmap(imageResource);
////        imageView.setImageBitmap(bitmap);
//
//        alertDialogs.setCancelable(true);
//
//        alertDialogs.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                menu.findItem(R.id.action_view).setVisible(false);
//                menu.findItem(R.id.action_share).setVisible(false);
//                menu.findItem(R.id.action_delete).setVisible(false);
//                menu.findItem(R.id.action_refresh).setVisible(true);
//            }
//        });
//        alertDialogs.show();
//    }


    private void getMyTags() {

        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");


        GetMyTagsRequestModel getMyTagsRequestModel = new GetMyTagsRequestModel();
        getMyTagsRequestModel.setSecret_key(Utils.getFromUserDefaults(getActivity(), Constants.SECRET_KEY));
        getMyTagsRequestModel.setAccess_key(Utils.getFromUserDefaults(getActivity(), Constants.ACCESS_KEY));
        getMyTagsRequestModel.setUser_id(Utils.getFromUserDefaults(getActivity(), Constants.USER_ID));
        getMyTagsRequestModel.setIs_testdata("1");

        StringEntity entity = null;
        try {
            entity = new StringEntity(getMapper().writer().writeValueAsString(getMyTagsRequestModel));
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }


        Log.e(TAG, "GET MY TAG URL-" + Urls.GET_MY_TAGS);
        client.post(getActivity(), Urls.GET_MY_TAGS, entity, "application/json", new GetMyTagsResponseHandler());


    }

    private class GetMyTagsResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            progressWheel.setVisibility(View.GONE);
            if (responseBody != null) {

                try {

                    JSONObject object = new JSONObject(new String(responseBody));
                    if (object.getInt("status") == 1) {
                        final JSONArray array = object.getJSONArray("Tags");

                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.createOrUpdateAllFromJson(Tags.class, array);
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                if (getMap()!=null)getTagFromDataBase();
                            }
                        });

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }


//    Bitmap drawable_from_url(String url) throws java.net.MalformedURLException, java.io.IOException {
//        Bitmap x;
//
//        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
//        connection.setRequestProperty("User-agent", "Mozilla/4.0");
//
//        connection.connect();
//        InputStream input = connection.getInputStream();
//
//        x = BitmapFactory.decodeStream(input);
//        return x;
//    }

//    public static Bitmap getBitmapFromURL(String src) {
//        try {
//            URL url = new URL(src);
//            return BitmapFactory.decodeStream(url.openConnection().getInputStream());
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//
////        try {
////            URL url = new URL(src);
////            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
////            connection.setDoInput(true);
////            connection.connect();
////            connection.setConnectTimeout(50000);
////            InputStream input = connection.getInputStream();
////            Bitmap myBitmap = BitmapFactory.decodeStream(input);
////            return myBitmap;
////        } catch (IOException e) {
////            // Log exception
////            return null;
////        }
//    }


//
//    private void getTags() {
//        String myTags = Utils.getFromUserDefaults(getActivity(), Constants.MY_TAG);
//        if (myTags != null) {
//            if (!myTags.equals("")) {
//
//                Gson gson = new Gson();
//
//                Type type = new TypeToken<ArrayList<MyTag>>() {
//                }.getType();
//                arrayList = gson.fromJson(myTags, type);
//                Log.e(TAG, "TAG SIZE-" + arrayList.size());
//                Log.e(TAG, "IMAGE-" + arrayList.get(0).getMedia());
//                Log.e(TAG, "IMAGE URL-" + arrayList.get(0).getImageUrl());
//
////                mUrls.clear();
////                mPositions.clear();
////                for (int i = 0; i < arrayList.size(); i++) {
////                    File file = new File(arrayList.get(i).getMedia());
////                    if (file.exists()) {
////
////                    } else {
////                        // Execute DownloadImage AsyncTask
////                        mPositions.add(i);
////                        mUrls.add(Urls.BASE_TAG_IMAGE_URL + arrayList.get(i).getImageUrl());
////                        arrayList.get(i).setFileExist(false);
////
////                    }
////                }
////
////                new DownLoadImages().execute();
//
//                startDemo();
//            } else {
//                getMyTags();
//            }
//        } else {
//            getMyTags();
//        }
//    }

//    public static void freeMemory() {
//        System.runFinalization();
//        Runtime.getRuntime().gc();
//        System.gc();
//    }
//
//
//    private Bitmap createBitmap(String imagePath) {
//
//        Bitmap bitmap = null;
//
//        if (bitmap != null) {
//            bitmap.recycle();
//            bitmap = null;
//        }
//
//
//        bitmap = BitmapFactory.decodeFile(imagePath);
//        return bitmap;
//    }


//    private void saveTagOffline() {
//        MyTag myTag = new MyTag();
//        Gson gson = new Gson();
//
//        String jsonString = Utils.getFromUserDefaults(getActivity(), Constants.MY_TAG);
//
//        if (jsonString != null) {
//            if (!jsonString.equals("")) {
//
//
//                Log.e(TAG, "MY TAG FROM PREF-" + jsonString);
//                Type type = new TypeToken<ArrayList<MyTag>>() {
//                }.getType();
//                ArrayList<MyTag> arrayList = gson.fromJson(jsonString, type);
//
//                ArrayList<Integer> tagIdList = new ArrayList<>();
//
//                for (int i = 0; i < arrayList.size(); i++) {
//                    tagIdList.add(arrayList.get(i).getTagId());
//                }
//
////                for (int i = 0; i < tagsArrayList.size(); i++) {
////                    if (!tagIdList.contains(tagsArrayList.get(i).getTag_id())) {
////
////
////                        MyTag.UserDetails userDetails = new MyTag.UserDetails();
////                        userDetails.setUserId(Utils.getFromUserDefaults(getActivity(), Constants.USER_ID));
////                        userDetails.setUserContactNumber(Utils.getFromUserDefaults(getActivity(), Constants.CONTACT_NUMBER));
////
////                        ArrayList<MyTag.FriendDetails> friendDetailsArrayList = new ArrayList<>();
////                        myTag.setUserDetails(userDetails);
////
////                        for (int j = 0; j < tagsArrayList.get(i).getFriend_detail().size(); j++) {
////                            MyTag.FriendDetails friendDetails = new MyTag.FriendDetails();
////
////
////                            friendDetails.setFriendsID(String.valueOf(tagsArrayList.get(i).getFriend_detail().get(j).getUser_id()));
////                            friendDetails.setFriendContactNumber(tagsArrayList.get(i).getFriend_detail().get(j).getContact_number());
////                            friendDetails.setFrindName("");
////
////                            friendDetailsArrayList.add(friendDetails);
////
////
////                        }
////
////                        myTag.setFriendDetails(friendDetailsArrayList);
////
////                        myTag.setLatitude(tagsArrayList.get(i).getLatitude());
////                        myTag.setLongitude(tagsArrayList.get(i).getLongitude());
////                        myTag.setmPosition(new LatLng(tagsArrayList.get(i).getLatitude(), tagsArrayList.get(i).getLongitude()));
////                        myTag.setMessage(tagsArrayList.get(i).getMessage());
////                        myTag.setMedia("");
////                        myTag.setImageUrl(tagsArrayList.get(i).getMedia().getMedia_url());
////                        myTag.setTagId(tagsArrayList.get(i).getTag_id());
////                        myTag.setFileExist(false);
////
////
////                        String userDetail = gson.toJson(myTag.getUserDetails());
////                        Log.e(TAG, "USER_DETAILS-" + userDetail);
////                        String frindsDetails = gson.toJson(myTag.getFriendDetails());
////                        Log.e(TAG, "FRIENDS DETAILS-" + frindsDetails);
////                        Log.e(TAG, "LATITUDE-" + myTag.getLatitude());
////                        Log.e(TAG, "LONGITUDE-" + myTag.getLongitude());
////                        Log.e(TAG, "POSITION-" + myTag.getmPosition());
////                        Log.e(TAG, "LOCAL IMAGE-" + myTag.getMedia());
////                        Log.e(TAG, "SERVER IMAGE-" + Urls.BASE_TAG_IMAGE_URL + myTag.getImageUrl());
////                        Log.e(TAG, "TAG ID-" + myTag.getTagId());
////                        Log.e(TAG, "MESSAGE-" + myTag.getMessage());
////                        Log.e(TAG, "IS FILE EXIST-" + myTag.isFileExist());
////
////                        boolean isTagInserted = dbHalperClass.insertAddTag(userDetail, frindsDetails, String.valueOf(myTag.getLatitude()), String.valueOf(myTag.getLongitude()), myTag.getMessage(),
////                                myTag.getMedia(), String.valueOf(myTag.getmPosition()), Urls.BASE_TAG_IMAGE_URL + myTag.getImageUrl(), myTag.getTagId(), 0);
////
////                        if (isTagInserted) {
////                            Log.e(TAG, "TAG INSERTED SUCCESSFULLY.");
////                        } else {
////                            Log.e(TAG, "ERROR IN INSERTING TAG.");
////                        }
////
////
////                    }
////                }
//
//            }
//        }
//    }


//    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
//
//        @Override
//        protected Bitmap doInBackground(String... params) {
//            String imageURL = params[0];
//
//            Bitmap bitmap = null;
//            try {
//                // Download Image from URL
//                InputStream input = new java.net.URL(imageURL).openStream();
//                // Decode Bitmap
//                bitmap = BitmapFactory.decodeStream(input);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            return bitmap;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap bitmap) {
//            super.onPostExecute(bitmap);
////            image.setImageBitmap(bitmap);
//            // Close progressdialog
//            setImageUri(bitmap);
//
//        }
//    }

//    private String setImageUri(Bitmap bitmap) {
//
//
//        ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
//        File directory = cw.getDir("FollowMe", Context.MODE_PRIVATE);
//        if (!directory.exists()) {
//            directory.mkdir();
//        }
//        File mypath = new File(directory, System.currentTimeMillis() + ".png");
//
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(mypath);
//            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
//            fos.close();
//        } catch (Exception e) {
//            Log.e("SAVE_IMAGE", e.getMessage(), e);
//        }
//
//        Log.e(TAG, "DOWNLOADED IMAGE PATH-" + mypath.getAbsolutePath());
//
//        return directory.getAbsolutePath();
//    }


//    private class DownLoadImages extends AsyncTask {
//
//        @Override
//        protected Object doInBackground(Object[] params) {
//
//
//            ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
//            File directory = cw.getDir("FollowMe", Context.MODE_PRIVATE);
//            if (!directory.exists()) {
//                directory.mkdir();
//            }
//
//            for (int i = 0; i < mUrls.size(); i++) {
//
//                try {
//                    File firstFile = new File(directory, System.currentTimeMillis() + ".png");
//                    if (firstFile.exists() == false) {
////                        HttpClient httpClient = new DefaultHttpClient();
//
//                        HttpGet httpGet = new HttpGet(mUrls.get(i));
//                        HttpParams httpParameters = new BasicHttpParams();
//                        int timeoutConnection = 30000;
//                        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
//                        int timeoutSocket = 5000;
//                        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//
//                        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
//                        httpClient.setParams(httpParameters);
//
//                        Log.e(TAG, "IMAGE URL-" + mUrls.get(i));
//                        HttpResponse response = httpClient.execute(httpGet);
//                        Log.e(TAG, "STATUS CODE-" + response.getStatusLine().getStatusCode());
//                        if (response.getStatusLine().getStatusCode() == 200) {
//                            HttpEntity entity = response.getEntity();
//                            InputStream is = entity.getContent();
//                            Boolean status = firstFile.createNewFile();
//
//                            FileOutputStream fileOutputStream = new FileOutputStream(firstFile);
//                            byte[] buffer = new byte[1024];
//                            long total = 0;
//                            int count = 0;
//                            while ((count - is.read(buffer)) != -1) {
//                                total += count;
//                                fileOutputStream.write(buffer, 0, count);
//
//                            }
//                            fileOutputStream.close();
//                            is.close();
//
//                            for (int j = 0; j < arrayList.size(); j++) {
//                                if (arrayList.get(j).isFileExist()) {
//
//                                } else {
//                                    arrayList.get(i).setFileExist(true);
//                                    arrayList.get(i).setMedia(firstFile.getAbsolutePath());
//                                }
//                            }
//
//                            publishProgress(i);
//                        }
//                    }
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                } catch (ClientProtocolException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            return null;
//        }
//
//        @Override
//        protected void onProgressUpdate(Object[] values) {
//            super.onProgressUpdate(values);
//        }
//
//        @Override
//        protected void onPostExecute(Object o) {
//            super.onPostExecute(o);
//
//            startDemo();
//
//        }
//    }


    private void getTagFromDataBase() {
        arrayList.clear();
        arrayList.addAll(realm.copyFromRealm(realm.where(Tags.class)
                .equalTo("sender_id", Integer.parseInt(Utils.getFromUserDefaults(getActivity(), Constants.USER_ID))).isNotNull("media").findAll()));

        startDemo();
        if (arrayList.size() > 0) {
            new getImages().execute();
        }

//        Gson gson = new Gson();


//        Cursor cursor = dbHalperClass.getDataFromAddTag();
//        Log.e(TAG, "CURSOR SIZE-" + cursor.getCount());

//        if (cursor.getCount() > 0) {
//            arrayList.clear();
//            cursor.moveToFirst();
//            do {
//
//                Log.e(TAG, "ID-" + cursor.getString(0));
//                Log.e(TAG, "USER DETAILS-" + cursor.getString(1));
//                Log.e(TAG, "FRIENDS DETAILS-" + cursor.getString(2));
//                Log.e(TAG, "LATITUDE-" + cursor.getString(3));
//                Log.e(TAG, "LONGITUDE-" + cursor.getString(4));
//                Log.e(TAG, "MESSAGE-" + cursor.getString(5));
//                Log.e(TAG, "IMAGE LOCAL PATH-" + cursor.getString(6));
//                Log.e(TAG, "POSITION-" + cursor.getString(7));
//                Log.e(TAG, "SERVER IMAGE PATH-" + cursor.getString(8));
//                Log.e(TAG, "TAG ID-" + cursor.getString(9));
//                Log.e(TAG, "IS FILE EXIST-" + cursor.getString(10));
//
//
//                MyTag myTag = new MyTag();
//
//
//                Type type = new TypeToken<MyTag.UserDetails>() {
//                }.getType();
//                MyTag.UserDetails userDetails = gson.fromJson(cursor.getString(1), type);
//                Log.e(TAG, "USER CONTACT NUMBER-" + userDetails.getUserContactNumber());
//                Log.e(TAG, "USER ID-" + userDetails.getUserId());
//                myTag.setUserDetails(userDetails);
//
//                Type type1 = new TypeToken<ArrayList<MyTag.FriendDetails>>() {
//                }.getType();
//                ArrayList<MyTag.FriendDetails> friendDetailses = gson.fromJson(cursor.getString(2), type1);
//                Log.e(TAG, "FRIENDS LIST SIZE-" + friendDetailses.size());
//                for (int i = 0; i < friendDetailses.size(); i++) {
//                    Log.d(TAG, "FRIEND ID-" + friendDetailses.get(i).getFriendsID());
//                    Log.d(TAG, "FRIEND CONTACT NUMBER-" + friendDetailses.get(i).getFriendContactNumber());
//                }
//                myTag.setFriendDetails(friendDetailses);
//
//                myTag.setLatitude(Double.parseDouble(cursor.getString(3)));
//                myTag.setLongitude(Double.parseDouble(cursor.getString(4)));
//                myTag.setMessage(cursor.getString(5));
//                myTag.setMedia(cursor.getString(6));
//                myTag.setmPosition(new LatLng(Double.parseDouble(cursor.getString(3)), Double.parseDouble(cursor.getString(4))));
//                myTag.setImageUrl(cursor.getString(8));
//                myTag.setTagId(Integer.parseInt(cursor.getString(9)));
//                if (cursor.getString(10).equals("1")) {
//                    myTag.setFileExist(true);
//                } else {
//                    myTag.setFileExist(false);
//                }
//
//                arrayList.add(myTag);
//
//            } while (cursor.moveToNext());

//            Log.e(TAG, "TAG LIST SIZE-" + arrayList.size());

//        }

    }


    private void showMultipleImagePopup(final Context context, final ArrayList<Tags> tagses) {

        MultiImageDialogue multiImageDialogue =new MultiImageDialogue(context , tagses, realm);
        multiImageDialogue.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                menu.findItem(R.id.action_refresh).setVisible(true);
            }
        });
        multiImageDialogue.show();

//        final Dialog alertDialogs = new Dialog(context);
//        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        alertDialogs.setContentView(R.layout.layout_multiple_image_popup);
//
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        lp.copyFrom(alertDialogs.getWindow().getAttributes());
//        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
//        lp.width = displayMetrics.widthPixels;
//        lp.height = displayMetrics.heightPixels;
//        alertDialogs.getWindow().setAttributes(lp);
//
//
//        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//        tagImages = (ViewPager) alertDialogs.findViewById(R.id.pagerTagImages);
//        ImageView imgClose = (ImageView) alertDialogs.findViewById(R.id.img_close);
//        ImageView imgShare = (ImageView) alertDialogs.findViewById(R.id.img_share);
//        ImageView imgDelete = (ImageView) alertDialogs.findViewById(R.id.img_delete);
//        img_forward = (ImageView) alertDialogs.findViewById(R.id.img_forward);
//        img_backward = (ImageView) alertDialogs.findViewById(R.id.img_backward);
//
//        img_forward.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (tagImages.getCurrentItem() < tagses.size())
//                    tagImages.setCurrentItem(tagImages.getCurrentItem()+1);
//            }
//        });
//        img_backward.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (tagImages.getCurrentItem() >0)
//                    tagImages.setCurrentItem(tagImages.getCurrentItem()-1);
//            }
//        });
//
//        imgDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showAlertDialogs(getActivity(), "", "Are you sure you want to delete?");
//            }
//        });
//
//        imgShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Picasso.with(getActivity())
//                        .load(Urls.BASE_TAG_IMAGE_URL + tagses.get(tagImages.getCurrentItem()).getMedia().getMedia_url())
//                        .into(new Target() {
//                            @Override
//                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//
//                                File firstFile = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
//                                 FileOutputStream out = null;
//                                try {
//                                    out = new FileOutputStream(firstFile);
//                                    bitmap.compress(Bitmap.CompressFormat.JPEG, 95, out);
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                } finally {
//                                    try {
//                                        if (out != null) {
//                                            out.close();
//                                        }
//                                    } catch (IOException e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//
//                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                                sharingIntent.setType("image/*");
//                                Uri bmpUri = Uri.fromFile(firstFile);
//                                sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Follow Me Tag");
//                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, tagses.get(tagImages.getCurrentItem()).getMessage());
//                                sharingIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
//                                startActivity(Intent.createChooser(sharingIntent, "Share your tag"));
//
//
//                            }
//
//                            @Override
//                            public void onBitmapFailed(Drawable errorDrawable) {
//
//                            }
//
//                            @Override
//                            public void onPrepareLoad(Drawable placeHolderDrawable) {
//
//                            }
//                        });
//
//            }
//        });
//        tagImages.addOnPageChangeListener(onPageChangeListener);
//
//        tagImages.setAdapter(new SlidingImage_Adapter(getActivity(), tagses, realm));
//
//        imgClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialogs.dismiss();
//            }
//        });
//
//
//        alertDialogs.setCancelable(true);
//
//        alertDialogs.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
////                menu.findItem(R.id.action_view).setVisible(false);
////                menu.findItem(R.id.action_share).setVisible(false);
////                menu.findItem(R.id.action_delete).setVisible(false);
//                menu.findItem(R.id.action_refresh).setVisible(true);
//            }
//        });
//        alertDialogs.show();
    }










}
