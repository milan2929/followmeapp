package com.followme.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.followme.R;
import com.followme.adapter.HomePagerAdapter;


/**
 * Created by pc on 22/05/17.
 */

public class HomeFragment extends Fragment {

    private String TAG = HomeFragment.class.getName();

    private ViewPager viewPager;
    private HomePagerAdapter adapter;
//    public static Fragment fragment;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);

        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        initTab(view);
        return view;

    }

    private void initTab(View view) {


        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setIcon(getResources().getDrawable(R.drawable.ic_camera)));
        tabLayout.addTab(tabLayout.newTab().setText("Tags"));
        tabLayout.addTab(tabLayout.newTab().setText("MyWorld"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        viewPager = (ViewPager) view.findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(3);
        adapter = new HomePagerAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition()==0){
                    ((TagCameraFragment)adapter.getFragment(0)).startSenssor();
                }
             }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getPosition()==0){
                    ((TagCameraFragment)adapter.getFragment(0)).removeSenssor();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager.setCurrentItem(1);


    }

    @Override
    public void onResume() {
        super.onResume();
//        viewPager.setCurrentItem(1);
    }

    public void setTab(int position) {
        if (viewPager != null) {
            viewPager.setCurrentItem(position);
        }
    }

    //Return current fragment on basis of Position
//    public Fragment getFragment() {
////        return adapter.getItem(pos);
//        return this.fragment;
//    }


}
