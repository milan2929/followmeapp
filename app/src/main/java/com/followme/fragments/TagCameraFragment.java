package com.followme.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.followme.FollowMeApplicationClass;
import com.followme.R;
import com.followme.camerautils.AndroidUtils;
import com.followme.listeners.OnImageTakenListener;
import com.followme.ui.HomeActivity;
import com.followme.ui.NewTagActivity;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pc on 05/05/17.
 */

public class TagCameraFragment extends Fragment implements ImageChooserListener, SensorEventListener, OnImageTakenListener {


    private String TAG = TagCameraFragment.class.getName();
//    public static final String imagePath = Environment.getExternalStorageDirectory() + "/FollowMe/Photos/";

//    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
//    private static final String STATE_SINGLE_SHOT = "single_shot";
//    private static final String STATE_LOCK_TO_LANDSCAPE = "lock_to_landscape";
//    private static final int CONTENT_REQUEST = 1337;
//    private CameraFragment std = null;
//    private CameraFragment ffc = null;
    private CameraFragment current = null;
    private boolean hasTwoCameras = (Camera.getNumberOfCameras() > 1);
//    private boolean singleShot = false;
//    private boolean isLockedToLandscape = false;


    // ---
    ImageView ivShutterButton;
//    private AlertDialog SelectScoreboardDialog = null;

    public Boolean isFromGallery = false;


//    String categoryid;
    File file;
//    Uri pictureURI;
    //    ImageView ivDone;
//    ImageView ivMenu;
//    ImageView ivClose;
    ImageView ivPreview;
    //    ImageView ivPreviewSmall;
//    ImageView ivCyclone;
    FrameLayout frmlayGrid;
    //    ImageView ivGrid;
    ImageView ivGallary;
    LinearLayout ivSwitchCamera;
    ImageView ivFlashButton;
//    TextView tvNext;
//    TextView tvBack;
//    static int position = 1;
//    final int RESULT_CAMERA2 = 1;
//    final int RESULT_AVAIRY = 2;
//    String userid;
    private SensorManager senSensorManager;

//    ImageView ivSettings;
//    ImageView ivHeart;

    private int FLASH_MODE = 0;//0-Auto,1-On,2-Off

//    final static int MY_REQUEST_CODE = 1111;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.fragment_camera, container, false);
        initView(view);
        return view;

    }


    private void initView(View view) {

        ivPreview = (ImageView) view.findViewById(R.id.imagePreview);

        ivGallary = (ImageView) view.findViewById(R.id.ivGallary);
        ivSwitchCamera = (LinearLayout) view.findViewById(R.id.ivSwitchCamera);

        ivFlashButton = (ImageView) view.findViewById(R.id.flashButton);
        frmlayGrid = (FrameLayout) view.findViewById(R.id.frmlayGrid);
        ivShutterButton = (ImageView) view.findViewById(R.id.shutterButton);



        ivGallary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseImage();
            }
        });


        ivSwitchCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                current.switchCamera();
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, current).
//                        commit();
//                if (position == 0) {
//                    if (std == null) {
//                        std = TagCameraFragment.newInstance();
//                        position = 1;
//                    }
//
//                    current = std;
//                } else {
//                    if (ffc == null) {
//                        ffc = TagCameraFragment.newInstance(true);
//                        position = 0;
//                    }
//
//                    current = ffc;
//                }
//
//
//                getFragmentManager().beginTransaction().setCustomAnimations(R.animator.card_flip_right_in, R.animator.card_flip_right_out,
//                        R.animator.card_flip_left_in, R.animator.card_flip_left_out).replace(R.id.container, current).commit();
//                findViewById(android.R.id.content).post(new Runnable() {
//                    @Override
//                    public void run() {
//                        current.lockToLandscape(isLockedToLandscape);
//                    }
//                });
            }
        });

        try {
            if (hasTwoCameras) {
                current = (CameraFragment) CameraFragment.newInstance();
                ((HomeActivity) getActivity()).getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, current).commit();

    //
    //            findViewById(android.R.id.content).post(new Runnable() {
    //                @Override
    //                public void run() {
    //                    current.lockToLandscape(isLockedToLandscape);
    //                }
    //            });

            } else {

                current = (CameraFragment) CameraFragment.newInstance();

                ((HomeActivity) getActivity()).getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, current).commit();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        current.setListener(this);

        ivShutterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (current.isCameraAvailable()) {
                current.takePicture();
//                    pd = ProgressDialog.show(CameraActivity.this, "",
//                            "Capturing image...");
//                } else {
//                    Toast.makeText(CameraActivity.this,
//                            "Sorry, but you cannot use the camera now!",
//                            Toast.LENGTH_LONG).show();
//                }
            }
        });


        ivFlashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                current.changeFlashMode(ivFlashButton);
//                ivFlashButton.setText(current.getmFlashMode());


//                Log.e("Flash Mode is=", current.getFlashMode() + "-");
//
//                if (FLASH_MODE == 0)//Auto
//                {
//                    current.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
//                    ivFlashButton.setText(" On");
//
//                    FLASH_MODE = 1;
//
//                } else if (FLASH_MODE == 1)//off
//                {
//                    current.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
//                    ivFlashButton.setText(" Off");
//                    FLASH_MODE = 2;
//
//                } else {
//                    current.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
//                    ivFlashButton.setText(" Auto");
//                    FLASH_MODE = 0;
//
//                }
            }
        });

    }

    public void startSenssor(){
        senSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        Sensor senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void removeSenssor(){
        senSensorManager.unregisterListener(this);
    }


    private ImageChooserManager imageChooserManager;
    private String filePath;
    private int chooserType;


    public void chooseImage() {

        Log.e("Choose Image", "Camera");
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
//        imageChooserManager = new ImageChooserManager(this,
//                ChooserType.REQUEST_PICK_PICTURE, "myfolder", true);
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE);
        imageChooserManager.setImageChooserListener(this);
        try {
//            pd = ProgressDialog.show(CameraActivity.this, "",
//                    "Capturing image...");
            filePath = imageChooserManager.choose();

            Log.e("Choose Image", "Camera --" + filePath);

        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("CAMERA", "OnActivityResult");
        Log.i("CAMERA", "File Path : " + filePath);
        Log.i("CAMERA", "Chooser Type: " + chooserType);
        if (resultCode == getActivity().RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE);
            }
            imageChooserManager.submit(requestCode, data);
        } else {
         }
    }


    // Create a constant to convert nanoseconds to seconds.
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static final int SHAKE_THRESHOLD = 600;
    int mLastOrientation = 0;


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 1500) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000;
                int mOrientation = 0;
                // if (speed > SHAKE_THRESHOLD) {
                //getRandomNumber();

                if (x < 5 && x > -5 && y > 5)
                    mOrientation = 0;
                else if (x < -5 && y < 5 && y > -5)
                    mOrientation = 270;//90
                else if (x < 5 && x > -5 && y < -5)
                    mOrientation = 180;
                else if (x > 5 && y < 5 && y > -5)
                    mOrientation = 90;//270
                //  }
                if (mOrientation != mLastOrientation) {
                    //Toast.makeText(CameraActivity1.this, "Orientation = " + mOrientation, Toast.LENGTH_SHORT).show();

                    rotate(mOrientation, ivGallary);
                    rotate(mOrientation, ivFlashButton);
//                    rotate(mOrientation, ivGrid);
                    rotate(mOrientation, ivSwitchCamera);
//                    rotate(mOrientation, ivPreviewSmall);
//                    rotate(mOrientation, ivCyclone);
                    //   rotate(mOrientation,ivGallary);

                }
                mLastOrientation = mOrientation;
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }


    public void rotate(float rotation, View view) {

        float currentRotation = mLastOrientation;//ivFlashButton.getRotation();
        RotateAnimation anim = new RotateAnimation(currentRotation, rotation,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        //  currentRotation = (currentRotation + 30) % 360;


        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(500);
        anim.setFillEnabled(true);

        anim.setFillAfter(true);

        view.startAnimation(anim);
        //RotateAnimation rotate= (RotateAnimation) AnimationUtils.loadAnimation(this, R.anim.rotate);

        //  text.setAnimation(rotate);

        //ivSwitchCamera.setPivotX(ivSwitchCamera.getX()+(ivSwitchCamera.getWidth()/2));
        //ivSwitchCamera.setPivotY(ivSwitchCamera.getY()+(ivSwitchCamera.getHeight()/2));

    }

    public void SaveImage(final Bitmap image) {
        ((HomeActivity) getActivity()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                pd.dismiss();
                current.restartPreview();
                saveImageData(image);
                sendBitmapToSavePage(image, path);
//                sendBitmapToSavePage(image, imagePath);

            }
        });
        handler.sendEmptyMessage(0);
    }

    ProgressDialog pd;
    int height = 0, width = 0;

    android.os.Handler handler = new android.os.Handler(new android.os.Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            // TODO Auto-generated method stub
            //   ivPreview.setVisibility(View.VISIBLE);

            // Picasso.with(getApplication()).load(file).into(ivPreview);


//            Intent newIntent = new Intent(CameraActivity.this, FeatherActivity.class);
            //    newIntent.setData( Uri.parse("content://media/external/images/media/32705") );
//            newIntent.setData(Uri.fromFile(file));
//            newIntent.putExtra(Constants.EXTRA_IN_API_KEY_SECRET, "your api secret");
//            startActivityForResult(newIntent, RESULT_AVAIRY);

//            final Bitmap bm = getBitmapFromPath(file.getAbsolutePath());
//
//            Log.i("SIZE BITMAP ", bm.getHeight() + " " + bm.getWidth());
//
//            ivPreviewSmall.setImageBitmap(bm);
//
//            sendBitmapToSavePage(bm);


//            ImageLoader.getInstance().displayImage(
//                    "file:///" + file.getAbsolutePath(), ivPreviewSmall,
//                    new ImageLoadingListener() {
//
//                        @Override
//                        public void onLoadingStarted(String arg0, View arg1) {
//                            // TODO Auto-generated method stub
//
//                        }
//
//                        @Override
//                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//
//                        }
//
//                        @Override
//                        public void onLoadingComplete(String arg0, View arg1,
//                                                      Bitmap arg2) {
//                            // TODO Auto-generated method stub
//                            pd.dismiss();
//
//                            final Bitmap bm = getBitmapFromPath(file.getAbsolutePath());
//
//                            Log.i("SIZE BITMAP ", bm.getHeight() + " " + bm.getWidth());
//
//                            ivPreviewSmall.setImageBitmap(arg2);
//
//                            sendBitmapToSavePage(bm);
//
//
//                        }
//
//                        @Override
//                        public void onLoadingCancelled(String arg0, View arg1) {
//                            // TODO Auto-generated method stub
//
//                        }
//                    });

            return false;
        }
    });


    String path;
    String savedImageDirectory = Environment.getExternalStorageDirectory().toString();// + File.separator + "FollowMe";
    Format dateInFilename = new SimpleDateFormat("yyyyMMdd_HHmmss");

    //    Uri saveImageData(byte[] data, int pictureNum) {
    Uri saveImageData(Bitmap image) {
        try {
            File dir = new File(savedImageDirectory);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            if (!dir.isDirectory()) {
//                Toast.makeText(this, "Error saving picture: can't create directory " + dir.getPath(), Toast.LENGTH_LONG).show();
                return null;
            }
            String filename = String.format("IMG_"
                    + dateInFilename.format(new Date()));
//            if (pictureNum > 0)
//                filename += ("-" + pictureNum);
            filename += ".jpeg";

            path = savedImageDirectory + File.separator + filename;
            // FileOutputStream out = new FileOutputStream(path);
            // out.write(data);
            // out.close();
//
//            FileOutputStream fos = new FileOutputStream(path);
//            BufferedOutputStream bos = new BufferedOutputStream(fos);
//            image.compress(Bitmap.CompressFormat.JPEG, 100, bos);
////            bos.write(data);
//            bos.flush();
//            fos.getFD().sync();
//            bos.close();

            Log.e(TAG, "PATH-----" + path);

            AndroidUtils.scanSavedMediaFile(getActivity(), path);
            // Toast.makeText(this, "Saved Picture", Toast.LENGTH_SHORT).show();

            return Uri.fromFile(new File(path));
        } catch (Exception ex) {
//            Toast.makeText(this,
//                    "Error saving picture: " + ex.getClass().getName(),
//                    Toast.LENGTH_LONG).show();
            return null;
        }
    }

//    @Override
//    public boolean isSingleShotMode() {
//        return (singleShot);
//    }
//
//    @Override
//    public void setSingleShotMode(boolean mode) {
//        singleShot = mode;
//    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (chosenImage != null) {

                    Log.e("onImageChosen",
                            "Camera --" + chosenImage.getFilePathOriginal());

                    file = new File(chosenImage.getFilePathOriginal());

                    if (file.exists()) {
//
                        Bitmap myBitmap = FollowMeApplicationClass.getInstance().getBitmapFromPath(file.getAbsolutePath());
//
//                        ivPreviewSmall.setImageBitmap(myBitmap);
                        isFromGallery = true;
                        sendBitmapToSavePage(myBitmap, chosenImage.getFilePathOriginal());
                    }

                }
            }
        });
    }

    @Override
    public void onError(String s) {
        Log.e("Image choose error : ", s.toString());
    }


    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    public void sendBitmapToSavePage(final Bitmap bm, final String path) {

        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.setContentView(R.layout.activity_image_save);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        ImageButton tvCancel = (ImageButton) dialog.findViewById(R.id.tvCancel);
        ImageView ivCapturedImage = (ImageView) dialog.findViewById(R.id.ivCapturedImage);
//        ImageView ivEdit = (ImageView) dialog.findViewById(R.id.ivEdit);
//        ImageView ivUpload = (ImageView) dialog.findViewById(R.id.ivUpload);
//        ImageView ivMessage = (ImageView) dialog.findViewById(R.id.ivMessage);
//        FrameLayout flSaveImage = (FrameLayout) dialog.findViewById(R.id.fl_save);
        ImageView ivSave = (ImageView) dialog.findViewById(R.id.shutterButton);


        ivCapturedImage.setImageBitmap(bm);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(CameraActivity.this, "====>Save Clicked", Toast.LENGTH_LONG).show();
//                Log.e(TAG, "Save Image");
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(path);
                    BufferedOutputStream bos = new BufferedOutputStream(fos);
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    bos.flush();
                    fos.getFD().sync();
                    bos.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

//            bos.write(data);


//                ImageManager imageManager = new ImageManager(getActivity());
//                Toast.makeText(CameraActivity.this, "Upload clicked", Toast.LENGTH_SHORT).show();
                // Store Image


//                Log.e(TAG, "Path===>" + path);
//                Toast.makeText(CameraActivity.this, "==>" + path, Toast.LENGTH_LONG).show();


//                String filename = String.format("IMG_"
//                        + dateInFilename.format(new Date()));
//                filename += ".jpeg";


//                File originalFile = new File(path);
                File originalFile = new File(path);
                Log.e(TAG, "Original File Path===>" + originalFile.getPath());
                Log.e(TAG, "Original File Name===>" + originalFile.getName());


//                Intent intent = new Intent(getActivity(), NewTagActivity.class);
//                Bitmap resized = ThumbnailUtils.extractThumbnail(bm, 350, 350);
//                intent.putExtra("bitmapImage", resized);
//                getActivity().startActivity(intent);

//                dialog.dismiss();


                Intent intent = new Intent(getActivity(), NewTagActivity.class);
                intent.putExtra("bitmapImagePath", originalFile.getPath());
                getActivity().startActivity(intent);
                dialog.dismiss();


            }
        });


//

//        ivEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Toast.makeText(CameraActivity.this, "Edit clicked", Toast.LENGTH_SHORT).show();
//            }
//        });

//
//        ivMessage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Toast.makeText(CameraActivity.this, "Message clicked", Toast.LENGTH_SHORT).show();
//            }
//        });

        dialog.show();
    }
//
//    View.OnClickListener onClickListenerSettings= new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            Intent intent = new Intent(CameraActivity.this, SettingsMainActivity.class);
//            startActivity(intent);
//        }
//    };


//    View.OnClickListener onClickListenerCyclone = new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            showUploadAlert();
//        }
//    };


    @Override
    public void onImageTaken(Bitmap bitmap) {
        SaveImage(bitmap);
    }

    public void stopCamera() {
        current.stopCameraPreview();
    }

    public void startCamera() {
        current.restartPreview();
    }


//    View.OnClickListener onClickListenerMenu= new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            Intent i = new Intent(CameraActivity.this, MainActivity.class);
//            startActivity(i);
//            overridePendingTransition(R.anim.slide_up_dialog, R.anim.slide_out_down);
//        }
//    };
//
//    private void showUploadAlert() {
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        final View dialogView = inflater.inflate(R.layout.view_upload, null);
//        dialogBuilder.setView(dialogView);
//
//        TextView tvDone = (TextView) dialogView.findViewById(R.id.tvDone);
//
//
//        final AlertDialog alertDialog = dialogBuilder.create();
//        alertDialog.show();
//
//        tvDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });
//
//    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        FlurryAgent.logEvent("Camera Screen");
//    }


}
