package com.followme.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.followme.R;
import com.followme.utils.Constants;
import com.followme.utils.Utils;

/**
 * Created by pc on 22/05/17.
 */

public class SettingsFragment extends Fragment {

    private String TAG = SettingsFragment.class.getName();

    private Switch aSwitch;
    Menu menu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        return super.onCreateView(inflater, container, savedInstanceState);
        setHasOptionsMenu(true);

        final View view = inflater.inflate(R.layout.activity_settings, container, false);
        init(view);
//        getActivity().setTitle("Settings");
        return view;
    }

    private void init(View view) {

        aSwitch = (Switch) view.findViewById(R.id.switch_notification);


        if (Utils.getSettingsFromUserDefaults(getActivity(), Constants.IS_NOTIFICATION_ENABLE)) {
            aSwitch.setChecked(true);
        } else {
            aSwitch.setChecked(false);
        }


        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Log.e(TAG, "CHECKED");
                    Utils.saveSettingsToUserDefaults(getActivity(), Constants.IS_NOTIFICATION_ENABLE, true);

                } else {
                    Log.e(TAG, "UNCHECKED");
                    Utils.saveSettingsToUserDefaults(getActivity(), Constants.IS_NOTIFICATION_ENABLE, false);
                }
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        menu.findItem(R.id.action_view).setVisible(false);
//        menu.findItem(R.id.action_share).setVisible(false);
//        menu.findItem(R.id.action_delete).setVisible(false);
        menu.findItem(R.id.action_refresh).setVisible(false);
        this.menu = menu;

        super.onCreateOptionsMenu(menu, inflater);
    }

//    @Override
//    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        getActivity().setTitle("Settings");
//    }
}
