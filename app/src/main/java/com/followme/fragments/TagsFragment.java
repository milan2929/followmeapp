package com.followme.fragments;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.followme.R;
import com.followme.adapter.MessageListAdapter;
import com.followme.helper.Toolbar_ActionMode_Callback;
import com.followme.realm.Tags;
import com.followme.requestmodel.DeleteTagsRequestModel;
import com.followme.requestmodel.GetMessageListRequestModel;
import com.followme.responsemodel.DeleteTagResponseModel;
import com.followme.responsemodel.SampleTagModel;
import com.followme.ui.HomeActivity;
import com.followme.ui.NewTagActivity;
import com.followme.ui.TagDetailsActivity;
import com.followme.utils.Constants;
import com.followme.utils.ProgressWheel;
import com.followme.utils.Urls;
import com.followme.utils.Utils;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.followme.ui.BaseActivity.getMapper;

/**
 * Created by pc on 05/05/17.
 */

public class TagsFragment extends Fragment {

    private String TAG = TagsFragment.class.getName();

    private ListView listViewTags;
    private MessageListAdapter mAdapter;
    private ProgressWheel progressWheel;
    private LinearLayout linearLayoutNoMessage;

    private ArrayList<Tags> tagsArrayList = new ArrayList<>();

    Menu menu;
    private Realm realm;

    private ActionMode mActionMode;
    public boolean isNewTagIniatiate = false;
    AdView adView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        setHasOptionsMenu(true);
        realm = ((HomeActivity) getActivity()).realm;

        final View view = inflater.inflate(R.layout.fragment_tags, container, false);
        initView(view);
        implementListViewClickListeners();
        return view;


    }

    private void initView(View view) {


        adView = (AdView) view.findViewById(R.id.adView);
        AdRequest.Builder build = new AdRequest.Builder();
        build.addTestDevice("45784DE82D84CC18577404F63C990355");
        adView.loadAd(build.build());

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab_tags);
        listViewTags = (ListView) view.findViewById(R.id.lv_tags);
        progressWheel = (ProgressWheel) view.findViewById(R.id.progress);
        linearLayoutNoMessage = (LinearLayout) view.findViewById(R.id.linear_no_message);

        getMessageList();

        mAdapter = new MessageListAdapter(getActivity(), tagsArrayList, realm);
        listViewTags.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isNewTagIniatiate = true;
                Intent intent = new Intent(getActivity(), NewTagActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        updateMessageList();
        if (mAdapter != null) mAdapter.notifyDataSetChanged();


    }

    private void updateMessageList() {
        try {
            tagsArrayList.clear();
            tagsArrayList.addAll(realm.copyFromRealm(realm.where(Tags.class).notEqualTo("sender_id", Integer.parseInt(Utils.getFromUserDefaults(getActivity(), Constants.USER_ID)))
                    .isNotNull("sender_detail").equalTo("is_delete", 0).findAll().where().distinct("sender_id")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.action_refresh).setVisible(true);
        this.menu = menu;

        super.onCreateOptionsMenu(menu, inflater);
    }


    //Implement item click and long click over list view
    private void implementListViewClickListeners() {
        listViewTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //If ActionMode not null select item

                if (mActionMode != null) {
                    onListItemSelect(position);
                } else {

                    Tags tags = tagsArrayList.get(position);
                    Intent intent = new Intent(getActivity(), TagDetailsActivity.class);
                    intent.putExtra("username", tags.getSender_detail().getContact_number());
                    intent.putExtra("data", tagsArrayList.get(position).getTag_id());
                    getActivity().startActivity(intent);


                }


            }
        });
        listViewTags.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //Select item on long click
                onListItemSelect(position);
                return true;
            }
        });
    }


    //List item select method
    private void onListItemSelect(int position) {
        mAdapter.toggleSelection(position);//Toggle the selection
        boolean hasCheckedItems = mAdapter.getSelectedCount() > 0;//Check if any items are already selected or not
        if (hasCheckedItems && mActionMode == null)
            // there are some selected items, start the actionMode
//            mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(new Toolbar_ActionMode_Callback(getActivity(), mAdapter, sampleTagModelArrayList, true));
            mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(new Toolbar_ActionMode_Callback(getActivity(), mAdapter, tagsArrayList, true));
        else if (!hasCheckedItems && mActionMode != null)
            // there no selected items, finish the actionMode
            mActionMode.finish();
        if (mActionMode != null)
            //set action mode title on item selection
            mActionMode.setTitle(String.valueOf(mAdapter
                    .getSelectedCount()) + " selected");


    }

    //Set action mode null after use
    public void setNullToActionMode() {
        if (mActionMode != null)
            mActionMode = null;
    }

    //Delete selected rows
    public void deleteRows() {
        SparseBooleanArray selected = mAdapter.getSelectedIds();//Get selected ids
        //Loop all selected ids
        ArrayList<String> selectedTagsIdList = new ArrayList<>();
        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {
                //If current id is selected remove the item via key
//                sampleTagModelArrayList.remove(selected.keyAt(i));

                selectedTagsIdList.add(String.valueOf(tagsArrayList.get(selected.keyAt(i)).getTag_id()));
                tagsArrayList.remove(selected.keyAt(i));
                mAdapter.notifyDataSetChanged();//notify adapter

            }
        }
        deleteTag(convertListToCommaSepratedString(selectedTagsIdList));
        Toast.makeText(getActivity(), selected.size() + " item deleted.", Toast.LENGTH_SHORT).show();//Show Toast
        mActionMode.finish();//Finish action mode after use
    }

    public int getSelectedCount() {

        return mAdapter.getSelectedCount();
    }


    public String getSelectedContact() {

        String selectedContact = null;

        SparseBooleanArray selected = mAdapter.getSelectedIds();//Get selected ids


        //Loop all selected ids
        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {
                //If current id is selected get the item via key

//                selectedContact = sampleTagModelArrayList.get(selected.keyAt(i)).getUserName();
                if (tagsArrayList.get(selected.keyAt(i)).getSender_detail().getName() == null) {
                    selectedContact = tagsArrayList.get(selected.keyAt(i)).getSender_detail().getContact_number();

                } else {
                    if (tagsArrayList.get(selected.keyAt(i)).getSender_detail().getName().equals("")) {
                        selectedContact = tagsArrayList.get(selected.keyAt(i)).getSender_detail().getContact_number();

                    } else {
                        selectedContact = tagsArrayList.get(selected.keyAt(i)).getSender_detail().getName();

                    }

                }
//                selectedContact = tagsArrayList.get(selected.keyAt(i)).getSender_detail().getContact_number();


            }
        }


        return selectedContact;
    }


    private void getMessageList() {


        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");

        GetMessageListRequestModel getMessageListRequestModel = new GetMessageListRequestModel();
        getMessageListRequestModel.setSecret_key(Utils.getFromUserDefaults(getActivity(), Constants.SECRET_KEY));
        getMessageListRequestModel.setAccess_key(Utils.getFromUserDefaults(getActivity(), Constants.ACCESS_KEY));
        getMessageListRequestModel.setUser_id(Utils.getFromUserDefaults(getActivity(), Constants.USER_ID));
        getMessageListRequestModel.setIs_testdata("1");

        try {
            String input = getMapper().writer().writeValueAsString(getMessageListRequestModel);
            Log.e("TagsFragment", Urls.GET_MESSAGE_LIST + " input: " + input);
            StringEntity entity = new StringEntity(input);
            client.post(getActivity(), Urls.GET_MESSAGE_LIST, entity, "application/json", new GetMessageListResponseHandler());

        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }


    }


    private class GetMessageListResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            progressWheel.setVisibility(View.GONE);
            if (responseBody != null) {

                try {
                    JSONObject object = new JSONObject(new String(responseBody));
                    if (object.getInt("status") == 1) {
                        final JSONArray array = object.getJSONArray("Tags");

                        realm.executeTransactionAsync(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.createOrUpdateAllFromJson(Tags.class, array);

                                RealmResults<Tags> res = realm.where(Tags.class).findAll().where().distinct("sender_id");
                                for (Tags tags : res){
                                    long count = realm.where(Tags.class).equalTo("is_delete", 1).equalTo("sender_id", tags.getSender_id()).count();
                                    if (count > 0){
                                        realm.where(Tags.class).equalTo("sender_id", tags.getSender_id()).findAll().deleteAllFromRealm();
                                    }
                                }
                            }
                        }, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {

                                updateMessageList();

                                if (tagsArrayList.size() > 0) {
                                    if (mAdapter != null && tagsArrayList.size() > 0) {
                                        mAdapter.addAll(tagsArrayList);
                                        mAdapter.notifyDataSetChanged();
                                    } else {
                                        listViewTags.setVisibility(View.GONE);
                                        linearLayoutNoMessage.setVisibility(View.VISIBLE);
                                    }

                                } else {
                                    listViewTags.setVisibility(View.GONE);
                                    linearLayoutNoMessage.setVisibility(View.VISIBLE);
                                }

                            }
                        });


                    } else if (object.getInt("status") == 3) {
                        listViewTags.setVisibility(View.GONE);
                        linearLayoutNoMessage.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }


    private String convertListToCommaSepratedString(ArrayList<String> list) {
        String str = "";
        StringBuilder commaSepValueBuilder = new StringBuilder();

        //Looping through the list
        for (int i = 0; i < list.size(); i++) {
            //append the value into the builder
            commaSepValueBuilder.append(list.get(i));

            //if the value is not the last element of the list
            //then append the comma(,) as well
            if (i != list.size() - 1) {
                commaSepValueBuilder.append(",");
            }
        }
        Log.e(TAG, "COMMA SEPERATED STRING-" + commaSepValueBuilder.toString());
        str = commaSepValueBuilder.toString();
        return str;
    }


    private void deleteTag(String tagIds) {


        AsyncHttpClient client = new AsyncHttpClient();
        client.setConnectTimeout(Constants.CONNECTION_TIME_OUT);
        client.addHeader("User-Agent", "Android");


        DeleteTagsRequestModel deleteTagsRequestModel = new DeleteTagsRequestModel();
        deleteTagsRequestModel.setSecret_key(Utils.getFromUserDefaults(getActivity(), Constants.SECRET_KEY));
        deleteTagsRequestModel.setAccess_key(Utils.getFromUserDefaults(getActivity(), Constants.ACCESS_KEY));
        deleteTagsRequestModel.setTag_ids(tagIds);
        deleteTagsRequestModel.setIs_testdata("1");


        StringEntity entity = null;
        try {
            entity = new StringEntity(getMapper().writer().writeValueAsString(deleteTagsRequestModel));
        } catch (UnsupportedEncodingException | JsonProcessingException e) {
            e.printStackTrace();
        }

        Log.e(TAG, "DELETE TAG URL-" + Urls.DELETE_TAG);
        client.post(getActivity(), Urls.DELETE_TAG, entity, "application/json", new DeleteTagResponseHandler());


    }


    private class DeleteTagResponseHandler extends AsyncHttpResponseHandler {

        @Override
        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
            progressWheel.setVisibility(View.GONE);
            if (responseBody != null) {
                Log.e(TAG, "DELETE TAG RESPONSE-" + new String(responseBody));

                DeleteTagResponseModel deleteTagResponseModel = null;
                try {
                    deleteTagResponseModel = getMapper().readValue(responseBody, DeleteTagResponseModel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (deleteTagResponseModel != null && deleteTagResponseModel.getStatus().equals("1")) {
                    if (tagsArrayList.size() > 0) {
                        listViewTags.setVisibility(View.VISIBLE);
                        linearLayoutNoMessage.setVisibility(View.GONE);
                    } else {
                        listViewTags.setVisibility(View.GONE);
                        linearLayoutNoMessage.setVisibility(View.VISIBLE);
                    }
                }

            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
            progressWheel.setVisibility(View.GONE);
            Log.e(TAG, "STATUS CODE-" + statusCode);
            Log.e(TAG, "ERROR-" + error.getMessage());
            error.printStackTrace();
        }
    }
}
