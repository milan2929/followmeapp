package com.followme.listeners;

import android.graphics.Bitmap;

/**
 * Created by pc on 08/05/17.
 */

public interface OnImageTakenListener {

    void onImageTaken(Bitmap bitmap);
}
