package com.followme.helper;


import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.followme.R;
import com.followme.realm.Contacts;
import com.followme.ui.BaseActivity;
import com.followme.utils.Utils;

import org.json.JSONObject;

import io.realm.Realm;

public class ReadContactsApi {

    public static String TAG = "CONTACTS";
    private final String all_coutry_code;
    private Context context;
    private String coutry_code;
    private  CompleteSyncing completeSyncing;

    public class ContactDetails {

        private String phone_number;
        private String country_code;

        public String getPhone_number() {
            return phone_number;
        }

        public String getCountry_code() {
            return country_code;
        }

        public void setPhone_number(String phone_number) {
            this.phone_number = phone_number;
        }

        public void setCountry_code(String country_code) {
            this.country_code = country_code;
        }
    }

    private String GetCountryZipCode(Context context) {
        String CountryID;
        String CountryZipCode = "";
        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        CountryID = manager.getSimCountryIso().toUpperCase();
        String[] rl =context.getResources().getStringArray(R.array.CountryCodes);

        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }

    public interface CompleteSyncing{
        void onComplete();
    }

    public ReadContactsApi(Context context,CompleteSyncing completeSyncing) {
        this.context = context;
        this.completeSyncing = completeSyncing;
        all_coutry_code = context.getResources().getString(R.string.countrycode);
        coutry_code = GetCountryZipCode(context);

        new ReadContacts().execute();
    }


    private class ReadContacts extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] params) {

            Realm realm = ((BaseActivity) context).getRealm();

            try {
                Cursor pCur = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
                if (pCur.getCount() > 0) {
                    while (pCur.moveToNext()) {
                        String id = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        String version = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA_VERSION));

                        Contacts contacts = realm.where(Contacts.class).equalTo("record_id", Integer.parseInt(id)).findFirst();
                        if (contacts==null || contacts.getApp_version() > Integer.parseInt(version)){

                            if ( Integer.parseInt(pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)))> 0) {
                                String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                                if (Utils.isValidPhoneNumber(phoneNo.replaceAll("[^a-zA-Z0-9]", ""))) {

                                    ContactDetails contactDetails = modifyPhoneNumber(phoneNo);

//                                    contacts = new Contacts();
//                                    contacts.setRecord_id(Integer.parseInt(id));
//                                    contacts.setContact_number(contactDetails.getPhone_number());
//                                    contacts.setCountry_coder(contactDetails.getCountry_code());
//                                    contacts.setName(name);
//                                    contacts.setImage("");
//                                    contacts.setApp_version(Integer.parseInt(version));
//                                    contacts.setSelected(false);

                                    JSONObject jsonObject =new JSONObject();
                                    jsonObject.put("record_id", Integer.parseInt(id));
                                    jsonObject.put("contact_number", contactDetails.getPhone_number());
                                    jsonObject.put("country_coder", contactDetails.getCountry_code());
                                    jsonObject.put("name",name);
                                    jsonObject.put("app_version", Integer.parseInt(version));
                                    jsonObject.put("isSelected", false);

                                    try {
                                        realm.beginTransaction();
                                        realm.createOrUpdateObjectFromJson(Contacts.class, jsonObject);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    } finally {
                                        realm.commitTransaction();
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                realm.close();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            Log.d(TAG, "CONTACTS SYNCHED");
            completeSyncing.onComplete();
            super.onPostExecute(o);
        }
    }

    @android.support.annotation.NonNull
    private ContactDetails modifyPhoneNumber(String phone_number) {
        ContactDetails contactDetails = new ContactDetails();
        phone_number = phone_number.replaceAll("[^0-9+]", "").replaceAll("\\s+", "").trim();
        if (phone_number.substring(0, 1).equals("+")) {
            phone_number = phone_number.substring(1, phone_number.length());
            updateCountryCode(phone_number, contactDetails);
        } else if (phone_number.substring(0, 2).equals("00")) {
            phone_number = phone_number.substring(2, phone_number.length());
            updateCountryCode(phone_number, contactDetails);
        } else if (phone_number.substring(0, 1).equals("0")) {
            phone_number = phone_number.substring(1, phone_number.length());
            contactDetails.setCountry_code(coutry_code);
            contactDetails.setPhone_number(phone_number);
        } else {
            contactDetails.setCountry_code(coutry_code);
            contactDetails.setPhone_number(phone_number);
        }
        return contactDetails;
    }

    private boolean updateCountryCode(@android.support.annotation.NonNull String phone_number, @android.support.annotation.NonNull ContactDetails contactDetails) {
        for (int i = 1; i < 4; i++) {
            if (all_coutry_code.contains(String.format(",%s,", phone_number.substring(0, i)))) {
                contactDetails.setCountry_code(phone_number.substring(0, i));
                contactDetails.setPhone_number(phone_number.substring(i, phone_number.length()));
                return true;
            }
        }
        return false;
    }


}
