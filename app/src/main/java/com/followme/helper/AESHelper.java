//package com.followmeandroidapp.helper;
//
//import android.util.Base64;
//
//import javax.crypto.Cipher;
//import javax.crypto.spec.SecretKeySpec;
//
///**
// * Created by pc on 12/05/17.
// */
//
//public class AESHelper {
//
//    public static String encrypt(String input, String key){
//        byte[] crypted = null;
//        try{
//            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
//            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//            cipher.init(Cipher.ENCRYPT_MODE, skey);
//            crypted = cipher.doFinal(input.getBytes());
//        }catch(Exception e){
//            System.out.println(e.toString());
//        }
//        return   Base64.encodeToString(crypted,
//                Base64.NO_WRAP);
//    }
//
//    public static String decrypt(String input, String key){
//        byte[] output = null;
//        try{
//            SecretKeySpec skey = new SecretKeySpec(key.getBytes(), "AES");
//            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
//            cipher.init(Cipher.DECRYPT_MODE, skey);
//            output = cipher.doFinal(Base64.decode(input, Base64.NO_WRAP));
//        }catch(Exception e){
//            System.out.println(e.toString());
//        }
//        return new String(output);
//    }
//
//}
