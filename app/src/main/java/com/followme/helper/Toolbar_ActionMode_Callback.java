package com.followme.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.followme.R;
import com.followme.adapter.MessageListAdapter;
import com.followme.fragments.TagsFragment;
import com.followme.realm.Tags;
import com.followme.ui.HomeActivity;

import java.util.ArrayList;


/**
 * Created by pc on 13/05/17.
 */

public class Toolbar_ActionMode_Callback implements ActionMode.Callback {


    private Context context;
    //    private RecyclerView_Adapter recyclerView_adapter;
    private MessageListAdapter listView_adapter;
    private ArrayList<Tags> message_models;
    private boolean isListViewFragment;


    public Toolbar_ActionMode_Callback(Context context, MessageListAdapter listView_adapter, ArrayList<Tags> message_models, boolean isListViewFragment) {
        this.context = context;
//        this.recyclerView_adapter = recyclerView_adapter;
        this.listView_adapter = listView_adapter;
        this.message_models = message_models;
        this.isListViewFragment = isListViewFragment;
    }

    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.menu_delete, menu);//Inflate the menu over action mode
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

        //Sometimes the meu will not be visible so for that we need to set their visibility manually in this method
        //So here show action menu according to SDK Levels
        if (Build.VERSION.SDK_INT < 11) {
            MenuItemCompat.setShowAsAction(menu.findItem(R.id.action_delete), MenuItemCompat.SHOW_AS_ACTION_NEVER);
//            MenuItemCompat.setShowAsAction(menu.findItem(R.id.action_copy), MenuItemCompat.SHOW_AS_ACTION_NEVER);
//            MenuItemCompat.setShowAsAction(menu.findItem(R.id.action_forward), MenuItemCompat.SHOW_AS_ACTION_NEVER);
        } else {
            menu.findItem(R.id.action_delete).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//            menu.findItem(R.id.action_copy).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
//            menu.findItem(R.id.action_forward).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }

        return true;
    }

    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:

                //Check if current action mode is from ListView Fragment or RecyclerView Fragment
//                if (isListViewFragment) {


//                Fragment listFragment = HomeActivity.fragment;//Get list view Fragment
//                Fragment listFragment = HomeActivity.selectedFragment;//Get list view Fragment
//                if (listFragment != null) {
//                    //If list fragment is not null
//
//
//                    Fragment tagFragment=((HomeFragment)listFragment).getFragment();
//
//                    if (((TagsFragment) tagFragment).getSelectedCount() == 1) {
//                        if (((TagsFragment) tagFragment).getSelectedContact() != null) {
//                            showAlertDialogs(context, "", "Delete tags from " + ((TagsFragment) tagFragment).getSelectedContact() + "?");
//                        } else {
//                            showAlertDialogs(context, "", "Delete tags from " + ((TagsFragment) tagFragment).getSelectedCount() + " contact?");
//                        }
//                    } else if (((TagsFragment) tagFragment).getSelectedCount() > 1) {
//                        showAlertDialogs(context, "", "Delete tags from " + ((TagsFragment) tagFragment).getSelectedCount() + " contacts?");
//                    }
//
//
//                }


                Fragment listFragment = HomeActivity.fragment;//Get list view Fragment


                if (listFragment != null) {
                    //If list fragment is not null


                    if (((TagsFragment) listFragment).getSelectedCount() == 1) {
                        if (((TagsFragment) listFragment).getSelectedContact() != null) {
                            showAlertDialogs(context, "", "Delete tags from " + ((TagsFragment) listFragment).getSelectedContact() + "?");


                        } else {
                            showAlertDialogs(context, "", "Delete tags from " + ((TagsFragment) listFragment).getSelectedCount() + " contact?");

                        }
                    } else if (((TagsFragment) listFragment).getSelectedCount() > 1) {
                        showAlertDialogs(context, "", "Delete tags from " + ((TagsFragment) listFragment).getSelectedCount() + " contacts?");


                    }
                }

                break;
//            case R.id.action_copy:
//
//                //Get selected ids on basis of current fragment action mode
//                SparseBooleanArray selected;
//                if (isListViewFragment)
//                    selected = listView_adapter
//                            .getSelectedIds();
//                else
//                    selected = recyclerView_adapter
//                            .getSelectedIds();
//
//                int selectedMessageSize = selected.size();
//
//                //Loop to all selected items
//                for (int i = (selectedMessageSize - 1); i >= 0; i--) {
//                    if (selected.valueAt(i)) {
//                        //get selected data in Model
//                        Item_Model model = message_models.get(selected.keyAt(i));
//                        String title = model.getTitle();
//                        String subTitle = model.getSubTitle();
//                        //Print the data to show if its working properly or not
//                        Log.e("Selected Items", "Title - " + title + "n" + "Sub Title - " + subTitle);
//
//                    }
//                }
//                Toast.makeText(context, "You selected Copy menu.", Toast.LENGTH_SHORT).show();//Show toast
//                mode.finish();//Finish action mode
//                break;
//            case R.id.action_forward:
//                Toast.makeText(context, "You selected Forward menu.", Toast.LENGTH_SHORT).show();//Show toast
//                mode.finish();//Finish action mode
//                break;


        }
        return false;
    }


    @Override
    public void onDestroyActionMode(ActionMode mode) {

        //When action mode destroyed remove selected selections and set action mode to null
        //First check current fragment action mode
//        if (isListViewFragment) {
        listView_adapter.removeSelection();  // remove selection
        Fragment listFragment = new HomeActivity().getFragment();//Get list fragment
        if (listFragment != null)
//            ((TagsFragment) ((HomeFragment) listFragment).getFragment()).setNullToActionMode();//Set action mode null

            ((TagsFragment) listFragment).setNullToActionMode();//Set action mode null


//        }
//        else {
//            recyclerView_adapter.removeSelection();  // remove selection
//            Fragment recyclerFragment = new MainActivity().getFragment(1);//Get recycler fragment
//            if (recyclerFragment != null)
//                ((RecyclerView_Fragment) recyclerFragment).setNullToActionMode();//Set action mode null
//        }
    }


    public void showAlertDialogs(final Context context, String title, String msg) {

        final Dialog alertDialogs = new Dialog(context);
        alertDialogs.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialogs.setContentView(R.layout.alert_delete);


        alertDialogs.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


        TextView txtaltmsg = (TextView) alertDialogs.findViewById(R.id.txtaltmsg);
        TextView txtaltyes = (TextView) alertDialogs.findViewById(R.id.tv_yes);
        TextView txtaltno = (TextView) alertDialogs.findViewById(R.id.tv_no);


        txtaltmsg.setText(msg);
//        txtaltmsg.setTextSize(14);
//        txtaltyes.setTextSize(14);
//        txtaltno.setTextSize(14);

        txtaltyes.setText("Delete");
        txtaltno.setText("Cancel");

        alertDialogs.setCancelable(false);

        txtaltyes.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

////                Fragment listFragment = HomeActivity.fragment;//Get list view Fragment
//                Fragment listFragment = HomeActivity.selectedFragment;//Get list view Fragment
//                if (listFragment != null)
//                    //If list fragment is not null
//
//
////                Fragment tagFragment=((HomeFragment)listFragment).getFragment(1);
//
//                    ((TagsFragment) ((HomeFragment) listFragment).getFragment()).deleteRows();//delete selected rows


                Fragment listFragment = HomeActivity.fragment;//Get list view Fragment


                if (listFragment != null)
                    //If list fragment is not null
                    ((TagsFragment) listFragment).deleteRows();//delete selected rows


                alertDialogs.dismiss();


            }

        });

        txtaltno.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {


                alertDialogs.dismiss();
            }

        });

        alertDialogs.show();
    }


}
